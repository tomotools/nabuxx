#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>
#include"ccd_filter.h"
#include"log_filter.h"
#include"utilities.h"
using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(ccd,m)
{
  xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for ccd module (ccd filter and log)";

  py::class_<nabuxx::LogFilter>(m, "LogFilter")
    .def(py::init([]( xt::pyarray<int> unused_shape_vh,    float clip_min,  float clip_max, int num_of_threads=-1) {	  
	  return new nabuxx::LogFilter(
				       clip_min,
				       clip_max ,
				       num_of_threads ) ; 
		  }),  py::arg("unused_shape_vh")= xt::pyarray<int>({0,0}), py::arg("clip_min")=0,  py::arg("clip_max")=1.0,  py::arg("num_of_threads")=-1 
      )
    .def("take_logarithm",  [](
			       nabuxx::LogFilter &this_obj,
			       py::buffer  & stack
			       )  {
	   
	   py::buffer_info buffer_info  =   nabuxx::checked_buffer_request( stack,  'f' ,  "take_logarithm" , "stack",  nabuxx::memlayouts::c_contiguous | nabuxx::memlayouts::f_contiguous       );
	   py::gil_scoped_release release;
	   printf(" Taking log di %ld elements\n",  buffer_info.size ) ;
	   this_obj.apply_filter( (float*)buffer_info.ptr,  buffer_info.size ) ;
	 }
      )
    ;

  py::class_<nabuxx::CCDFilter>(m, "CCDFilter")
    .def(py::init([](xt::pyarray<int> shape_vh,  float threshold, int num_of_threads=-1) {	  
	  xt::xtensor_fixed<int, xt::xshape<2>> my_shape_vh = shape_vh;
	  return new nabuxx::CCDFilter(
				       my_shape_vh,
				       threshold ,
				       num_of_threads ) ; 
		  }),  py::arg("shape_vh"),  py::arg("median_clip_thresh"),  py::arg("num_of_threads")=-1 
      )
    .def("median_clip_correction_multiple_images",  []( nabuxx::CCDFilter &this_obj, py::buffer &stack )  {
	
	py::buffer_info buffer_info  =   nabuxx::checked_buffer_request( stack,  'f' ,  "median_clip_correction_multiple_images" , "stack",  nabuxx::memlayouts::c_contiguous         );
	   
	 py::gil_scoped_release release;
	if ( buffer_info.ndim==2 ) {

	  std::vector<long int> new_shape = {1,   buffer_info.shape[0]  ,  buffer_info.shape[1] };
	  this_obj.median_clip_correction_multiple_images( (float*)buffer_info.ptr,  new_shape ) ;
	  
	} else 	if (buffer_info.ndim ==3 ) {

	  // auto prova = xt::adapt( (float*)buffer_info.ptr, buffer_info.size , xt::no_ownership(),    buffer_info.shape     ) ; 
	  
	  // printf(" RITORNO\n");
	  this_obj.median_clip_correction_multiple_images( (float*)buffer_info.ptr,  buffer_info.shape ) ;
	  
	} else	{
	  
	  throw std::runtime_error("Input stack must have 2 or 3 dimensions");
	}
      }
      )
    ;  
}

