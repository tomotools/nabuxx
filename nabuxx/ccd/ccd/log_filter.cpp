#include<algorithm>
#include<exception>
#include<stdexcept>
#include<math.h>

#include"log_filter.h"
#include<string.h>
#include<omp.h>

using namespace nabuxx;


LogFilter::LogFilter(float clip_min, float clip_max , int num_of_threads )  {

  if ( num_of_threads==-1) {
   this-> number_of_threads = omp_get_max_threads();
  } else {
    this-> number_of_threads = num_of_threads;
  }

  this->clip_min = clip_min ; 
  this->clip_max = clip_max ; 

}

void LogFilter::apply_filter( float *stack , long int  size) {
  printf("numero di threads %d  \n", this->number_of_threads );
#pragma omp parallel for schedule(static) num_threads(this->number_of_threads)
  for(long int i=0; i< size ; i++) {
    float tmp = stack[i];
    if( tmp < clip_min) tmp = clip_min;
    if( tmp > clip_max) tmp = clip_max;
    stack[i] = -logf(tmp);
  }
}

