#ifndef CCD_FILTER_H
#define CCD_FILTER_H

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xfixed.hpp>



namespace nabuxx {

  class CCDFilter {
  public:


    CCDFilter( xt::xtensor_fixed<int, xt::xshape<2>> shape_vh, float threshold, int num_of_threads = -1) ;


    void median_clip_correction( float *xdata , float *xbuffer, std::vector<long int> & shape) ;

    void median_clip_correction_multiple_images( float *stack , std::vector<long int> & shape);

    xt::xtensor_fixed<int, xt::xshape<2>> shape_vh;
    int number_of_threads;

    xt::xtensor<float, 3> buffers_4t3l ; // buffers for thread, three lines


    
    float threshold;
  
  };

}

#endif
