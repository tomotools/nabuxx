#ifndef LOG_FILTER_H
#define LOG_FILTER_H



namespace nabuxx {

  class LogFilter {
  public:


    LogFilter(float clip_min, float clip_max , int num_of_threads = -1) ;

    void apply_filter( float *stack , long int  shape);

    int number_of_threads;
    
    float clip_min;
    float clip_max;
  
  };

};

#endif
