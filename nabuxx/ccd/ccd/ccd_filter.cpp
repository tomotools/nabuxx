#include<algorithm>
#include<exception>
#include<stdexcept>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>

#include"ccd_filter.h"
#include<string.h>
#include<omp.h>

using namespace nabuxx;


CCDFilter::CCDFilter( xt::xtensor_fixed<int, xt::xshape<2>> shape_vh, float threshold, int num_of_threads )  {

  if ( num_of_threads==-1) {
   this-> number_of_threads = omp_get_max_threads();
  } else {
    this-> number_of_threads = num_of_threads;
  }

  this->shape_vh = shape_vh;

  this->threshold = threshold ; 
  {
    auto &sh = this->shape_vh  ; 
    this->buffers_4t3l.resize( { (size_t) this-> number_of_threads, (size_t)  3,(size_t)  sh[1]+2  } ); // room for borders and on extra line for temporary results
  }  
}

void CCDFilter::median_clip_correction(  float *xdata , float *xbuffer, std::vector<long int> & shape ) {
  int s_v = shape[0];
  int s_h = shape[1];

  if( s_v != shape_vh(0) || s_h != shape_vh(1)) {
      std::stringstream ss;
      ss << "In method CCDFilter::median_clip_correction the argument shape does not match the setup argument\n";
      ss <<  "--------------------\n";
      ss <<  "   for vertical   ... " <<  s_v << " versus " <<  shape_vh(0)  << "\n";
      ss <<  "       horizontal ... " <<  s_h << " versus " <<  shape_vh(1)  << "\n";
      throw std::invalid_argument(ss.str()) ;
  }
  


  float * __restrict__  data = xdata;
  float * __restrict__  buffer_ptr = xbuffer;
  float temp[9];

  
  memcpy( buffer_ptr +0*(s_h+2)  +1, data + 0*s_h  , sizeof(float)*s_h) ;
  memcpy( buffer_ptr +1*(s_h+2)  +1, data + 0*s_h  , sizeof(float)*s_h) ;
  memcpy( buffer_ptr +2*(s_h+2)  +1, data + std::min(1,s_v-1)*s_h  , sizeof(float)*s_h) ;


#define  central_line 1
#define B(i,j)   (buffer_ptr[ ((central_line+i) )*(s_h+2)  + (j+1)  ] )

  B(-1, -1) = B(-1, 0);
  B( 0, -1) = B( 0, 0);
  B( 1, -1) = B( 1, 0);
  
  B(-1, s_h) = B(-1, s_h -1 );
  B( 0, s_h) = B( 0, s_h -1 );
  B( 1, s_h) = B( 1, s_h -1 );


  for(int i_line =0; i_line < s_v; i_line++) {

    for(int i_x = 0 ; i_x < s_h; i_x++) {
      
    
      temp[4] = B( 0 , i_x);
      
      temp[0] = B( -1, i_x - 1 );
      temp[1] = B( -1, i_x     );
      temp[2] = B( -1, i_x + 1 );
      temp[3] = B(  0 ,i_x - 1 );
	
      int count =0;
      
      count += ((  temp[4]    > temp[0] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[1] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[2] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[3] + threshold)?  1:0 );  
	
      temp[0] = B(  1, i_x - 1 );
      temp[1] = B(  1, i_x     );
      temp[2] = B(  1, i_x + 1 );
      temp[3] = B(  0 ,i_x + 1 );

	
      count += ((  temp[4]    > temp[5] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[6] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[7] + threshold)?  1:0 );  
      count += ((  temp[4]    > temp[8] + threshold)?  1:0 );  
	
      int boolV;
      float swap;
      
      if(count>4) {	  
	do
	  {
	    boolV = 0;
	    for (count = 0; count < 8; count++)
	      {
		if (temp[count] > temp[count+1])
		  {
		    swap = temp[count];
		    temp[count] = temp[count+1];
		    temp[count+1] = swap;
		    boolV = 1;
		  }
	      }
	  } while(boolV!=0);
	
	data[i_line * s_h  + i_x] = temp[4];
	
      }
    }

    memcpy( buffer_ptr +0*(s_h+2)  + 0 , buffer_ptr +1*(s_h+2)   , sizeof(float)*(s_h+2) ) ;
    memcpy( buffer_ptr +1*(s_h+2)  + 0 , buffer_ptr +2*(s_h+2)   , sizeof(float)*(s_h+2) ) ;
    memcpy( buffer_ptr +2*(s_h+2)  + 1, data + std::min( i_line+2 ,s_v-1)*s_h  , sizeof(float)*s_h) ;

    B( 1, -1) = B( 1, 0);
    B( 1, s_h) = B( 1, s_h -1 );
    
  }
}

void CCDFilter::median_clip_correction_multiple_images( float *stack , std::vector<long int> & shape) {
  std::vector<long int> image_shape = { shape[1], shape[2]  };
  printf("number of threads %d  \n", this->number_of_threads );

#pragma omp parallel for num_threads(this->number_of_threads)
  for(int i=0; i< shape[0] ; i++) {
    int thread_id = omp_get_thread_num();

    auto &vh = this->shape_vh;
    
    this->median_clip_correction(   stack + i * shape[1]*shape[2] ,
				    &buffers_4t3l( thread_id, 0,0),
				    image_shape
				    ); 
  }
}


// // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// #include <iostream>
// #include <pybind11/pybind11.h>
// #include <pybind11/numpy.h>


// namespace py = pybind11;


// py::array_t<double> add_arrays(py::array_t<double> input1, py::array_t<double> input2) {
//   py::buffer_info buf1 = input1.request();
//   py::buffer_info buf2 = input2.request();

//   if (buf1.size != buf2.size) {
//     throw std::runtime_error("Input shapes must match");
//   }

//   /*  allocate the buffer */
//   py::array_t<double> result = py::array_t<double>(buf1.size);

//   py::buffer_info buf3 = result.request();

//   double *ptr1 = (double *) buf1.ptr,
//          *ptr2 = (double *) buf2.ptr,
//          *ptr3 = (double *) buf3.ptr;
//   int X = buf1.shape[0];
//   int Y = buf1.shape[1];

//   for (size_t idx = 0; idx < X; idx++) {
//     for (size_t idy = 0; idy < Y; idy++) {
//       ptr3[idx*Y + idy] = ptr1[idx*Y+ idy] + ptr2[idx*Y+ idy];
//     }
//   }
 
//   // reshape array to match input shape
//   result.resize({X,Y});

//   return result;
// }


// PYBIND11_MODULE(example, m) {
//         m.doc() = "Add two vectors using pybind11"; // optional module docstring

//         m.def("add_arrays", &add_arrays, "Add two NumPy arrays");
// }


// // ----------------------------
// #include <cstddef>
// #include <xtensor/xadapt.hpp>

// void compute(double* data, std::size_t size)
// {
//     std::vector<std::size_t> shape = { size };
//     auto a = xt::adapt(data, size, xt::no_ownership(), shape);
//     a = a + a; // does not modify the size
// }
