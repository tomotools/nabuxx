#include<stdio.h>
#include<math.h>
#include<cmath>
#include<sstream>
#include<iostream>

#include<exception>
#include<vector>
#include<algorithm>
#include<functional>

#include<string>
#include<exception>
#include<stdexcept>

#include"span_info.h"


#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xview.hpp>


#include <xtensor/xnpy.hpp>

#include<omp.h>



using namespace nabuxx;

SpanInfo::SpanInfo(
		   float z_offset_mm,
		   std::vector<float> & z_pix_per_proj_arg,
		   std::vector<float> & x_pix_per_proj_arg,
		   std::vector<int>   & detector_shape_vh_arg,
		   int pag_margin_pix_arg,
		   std::vector<double> & projection_angles_deg_arg,
		   float pix_size_mm,
		   bool require_redundancy,
		   float angular_tolerance_steps
		   ) {
  this-> z_offset_mm = z_offset_mm; 
  this->num_images = z_pix_per_proj_arg.size();
  this->z_pix_per_proj = xt::adapt( z_pix_per_proj_arg ); 
  this->x_pix_per_proj = xt::adapt( x_pix_per_proj_arg ); 
  this->internally_projection_angles_deg = xt::adapt( projection_angles_deg_arg ); 
  this->pix_size_mm = pix_size_mm;
  this->angular_tolerance_steps = angular_tolerance_steps;
  
  if (this->x_pix_per_proj .size()!= (size_t) this->num_images or
      this->internally_projection_angles_deg.size()!=  (size_t)this->num_images) {
    std::string message=( std::string(" all the arguments z_pix_per_proj, x_pix_per_proj and projection_angles_deg\n"
				      " must have the same ht but their lenght were \n") +
			  std::to_string( this->z_pix_per_proj.size() )+ std::string(", ")  +
			  std::to_string(this->x_pix_per_proj.size()) + std::string(", ") +
			  std::to_string(this->internally_projection_angles_deg.size()) +  std::string("respectively \n")  )  ;
    throw std::invalid_argument(message) ;
  }

  if( require_redundancy) {
    this->redundancy_angle_deg = 360;
  } else {
    this->redundancy_angle_deg = 0;
  }

  this->pag_margin_pix = pag_margin_pix_arg;

  this->detector_shape_vh = xt::adapt(detector_shape_vh_arg);

  if( this->detector_shape_vh.size() != 2  )  {
    std::string message=( std::string(" The argument  detector_shape_vh must be of length 2 \n"
				      " but its length was \n") + std::to_string( detector_shape_vh_arg.size() ) );
	throw std::invalid_argument(message) ;
  }
  
  this->angular_period_deg = 360;
  // std::cout << " Setting minimal angular span to " << this->angular_period_deg << std::endl ; 


  if ( projection_angles_deg_arg.back() > projection_angles_deg_arg[0] ) {
    this->internally_projection_angles_deg = xt::adapt(projection_angles_deg_arg);
    this->angles_are_inverted = false ;
  } else {
    this->internally_projection_angles_deg = xt::adapt(projection_angles_deg_arg);
    this->internally_projection_angles_deg = this->internally_projection_angles_deg *(-1.0);

    
    // std::transform(this->internally_projection_angles_deg.begin(),
    // 		   this->internally_projection_angles_deg.end(),
    // 		   this->internally_projection_angles_deg.begin(),
    // 		   std::bind(std::multiplies<float>(), std::placeholders::_1, -1.0)
    // 		   );

    
    this->angles_are_inverted = true ;
  }
  this->projection_angles_deg = xt::adapt(projection_angles_deg_arg);
  

  this->_setup_ephemerides();
  this->_setup_sunshine();

}


void   SpanInfo::get_doable_span(
						int &view_height_min, int &view_height_max,
						float &z_over_stage_pix_min, float &z_over_stage_pix_max,
						float &z_over_stage_mm_min, float &z_over_stage_mm_max
				 ) {
  
  // xt::xtensor<float,1>  vertical_profile = xt::sum<float> (this->sunshine_image , {1});
  // xt::dump_npy("doable_profile.npy", vertical_profile);
  
  // xt::dump_npy("sunshine_image.npy", sunshine_image);
  
  
  int vert_dim = this->total_view_heights.shape(0)  ;
  
  int span_start = vert_dim;
  int span_end   = 0 ;
  
  for(int i=0; i < vert_dim ; i++) {
    if( this->sunshine_starts( i ) >=0 ) {
      if( (this->sunshine_ends(i)  - this->sunshine_starts( i )  ) > 0 )  {
	span_start = i;
	break;
      }
    }
  }
  for(int i=vert_dim-1; i >= 0 ; i--) {
    if( this->sunshine_starts( i ) >=0 ) {
      if(   (this->sunshine_ends(i)  - this->sunshine_starts( i )  ) > 0   )  {
	span_end = i;
	break;
      }
    }
  }


  int d_v = this->detector_shape_vh(0);
  
  view_height_min = this->total_view_heights(span_start); 
  view_height_max = this->total_view_heights(span_end); 
  z_over_stage_pix_min = -this->z_pix_per_proj[0]  +(d_v-1)/2 - view_height_max;
  z_over_stage_pix_max = -this->z_pix_per_proj[0]  +(d_v-1)/2 - view_height_min;
  z_over_stage_mm_min = z_over_stage_pix_min*pix_size_mm ;
  z_over_stage_mm_max = z_over_stage_pix_max*pix_size_mm ;
  
}		


std::string SpanInfo::get_informative_string() {
  int view_height_min,  view_height_max;
  float z_over_stage_pix_min, z_over_stage_pix_max ; 
  float z_over_stage_mm_min, z_over_stage_mm_max ; 
  
  this->get_doable_span(  view_height_min,  view_height_max, z_over_stage_pix_min, z_over_stage_pix_max, z_over_stage_mm_min, z_over_stage_mm_max) ; 

  std::string direction;

  if(  this->z_pix_per_proj[ this->num_images - 1 ] >= this->z_pix_per_proj[ 0 ] ) {
    direction = "ascending" ; 
  } else {
    direction = "descending" ; 
  }

  
  std::stringstream ss;
  ss << "Doable vertical span\n";
  ss << "--------------------\n";
  ss << "\n";
  ss << " The scan has been performed with a " <<  direction << "  vertical translation of the rotation axis.\n" ;
  ss << "\n";
  
  ss << "Detector reference system at iproj=0:\n" ; 
  ss << "   from vertical view height  ... " << view_height_min   << "\n";
  ss << "         up to  (excluded)     ... " << view_height_max << "\n";
  ss << "\n";
  ss << " The slice that projects to the first line of the first projection \n";
  ss << "  corresponds to vertical heigth = 0\n\n";
  ss << "\n";
  ss << "In voxel,the vertical doable span measures: " <<  view_height_max - view_height_min  << " \n";
  ss << "\n";
  
  ss << " In the  sample stage system:\n";

  ss << "       from vertical height above stage ( mm units)  ...  " <<  z_over_stage_mm_min - z_offset_mm << "\n";
  ss << "       up to  (included)    " << z_over_stage_mm_max - z_offset_mm << "\n" ; 
  
  return ss.str();
}




      
void SpanInfo::_setup_ephemerides() {
  /*
    A function which will set :

    * self.integer_shift_v
    * self.fract_complement_to_integer_shift_v
    * self.total_view_heights
    * self.on_detector_projection_intervals
    * self.within_margin_projection_intervals

    */
  
  this->integer_shift_v.resize({(size_t)this->num_images});
  this->fract_complement_to_integer_shift_v.resize({(size_t)this->num_images});

  for(int i_pro=0; i_pro < this->num_images ; i_pro++) {
    
    float trans_v = this->z_pix_per_proj(i_pro);
    int int_ceil = (int) ceil(trans_v); 
    this->integer_shift_v(i_pro) =  int_ceil;
    this->fract_complement_to_integer_shift_v(i_pro) = int_ceil - trans_v;
  }

  int total_view_top = this->detector_shape_vh(0);
  int total_view_bottom = 0;

  total_view_top = std::max(total_view_top, (int)  ceil(total_view_top + xt::amax( this->z_pix_per_proj)() ) ) ;
  
  total_view_bottom = std::min(total_view_bottom, (int) floor(total_view_bottom + xt::amin(this->z_pix_per_proj)() ))  ; 

  this->total_view_heights = xt::arange<int>(total_view_bottom, total_view_top + 1, 1) ; 

  this->on_detector_projection_intervals.resize({ this->total_view_heights.shape(0) ,2 });
  this->on_detector_projection_intervals.fill(0);  
    
  this->within_margin_projection_intervals.resize( { this->total_view_heights.shape(0) ,2   }   );
  this->within_margin_projection_intervals.fill(0);

#pragma omp parallel for  
  for(int  i_h=0 ; i_h < (int) this->total_view_heights.size(); i_h++) {
    int height = this->total_view_heights(i_h);
    bool previous_is_inside_detector = false ; 
    bool previous_is_inside_margin = false;  
  
    for(int i_pro=0; i_pro < this->num_images + 1; i_pro++)  {
      
      bool is_inside_detector = false ;  
      bool is_inside_margin = false; 

      if( i_pro < this->num_images) {
      
	int pos_inside_filtered_v = height - this->integer_shift_v(i_pro) ;
	
	if( pos_inside_filtered_v >= 0 &&
	    ( pos_inside_filtered_v < (this->detector_shape_vh(0) - (this->fract_complement_to_integer_shift_v(i_pro) != 0)))
	    ) {
	  is_inside_detector = true ;
	}

	if( pos_inside_filtered_v >= this->pag_margin_pix &&
	    ( pos_inside_filtered_v < (this->detector_shape_vh(0) - this->pag_margin_pix - (this->fract_complement_to_integer_shift_v(i_pro) != 0)))
	    ) {
	  is_inside_margin = true;
	}
      }

      if ((!previous_is_inside_detector) && is_inside_detector) {
	this->on_detector_projection_intervals(i_h, 0) = i_pro;
      }
      if ( previous_is_inside_detector && (! is_inside_detector) ) {
	this->on_detector_projection_intervals(i_h, 1) = i_pro - 1;
      }

      if ( ( ! previous_is_inside_margin) && is_inside_margin) {
	this->within_margin_projection_intervals(i_h, 0) = i_pro ;
      }
      if ( previous_is_inside_margin && (! is_inside_margin)) {
	this->within_margin_projection_intervals(i_h, 1) = i_pro - 1;
      }

      previous_is_inside_detector = is_inside_detector ;
      previous_is_inside_margin = is_inside_margin ; 
    }
  }
}


float SpanInfo::_active_angular_span_of_a_line(xt::xarray<float> &line_collector, double avg_angular_step_deg   )   {
  double first_angle = NAN;
  double last_angle = NAN;
    
  for(int i_proj=0; i_proj< this->num_images; i_proj++) {

    
    if( line_collector(i_proj) > 0.99) {
      if( std::isnan(first_angle ) ) {
	first_angle = this->internally_projection_angles_deg(i_proj);
      }
    } else {
      if( std::isnan(last_angle )  && (!std::isnan(first_angle))) {
	last_angle = this->internally_projection_angles_deg(i_proj);
      }
    }
  }
  if( std::isnan(first_angle)  ) {
    return 0;
  } else {

    if( std::isnan(last_angle)) last_angle = this->internally_projection_angles_deg.periodic(-1) ;
    return fabs(last_angle - first_angle + avg_angular_step_deg *( 1.01 + this->angular_tolerance_steps) );
  }
}

void SpanInfo::_setup_sunshine() {
  /* It prepares

   * self.sunshine_image

   an image which for every height, contained in self.total_view_heights, and in the same order
   contain the list of factors, one per every projection of the total list. Each  factor is a backprojection
   weight.
  */


  double avg_angular_step_deg = (this->internally_projection_angles_deg.periodic(-1) -  this->internally_projection_angles_deg(0)  )/( this->internally_projection_angles_deg.shape(0) -1) ;


  this->sunshine_starts.resize({ this->total_view_heights.shape(0)  } ) ;
  this->sunshine_ends  .resize({ this->total_view_heights.shape(0)  } ) ;

#pragma omp parallel for  
  for(int  i_h=0; i_h < (int) this->total_view_heights.size(); i_h++) {


    xt::xarray<float> line_collector( {(size_t)this->num_images}, 0.0);

    this->sunshine_starts(i_h )=0 ;
    this->sunshine_ends( i_h ) = 0 ;
    
    // int height = this->total_view_heights(i_h);

    int first_on_dect = this->on_detector_projection_intervals(i_h,0);
    int last_on_dect  = this->on_detector_projection_intervals(i_h,1);

    int first_within_margin = this->within_margin_projection_intervals(i_h,0) ;
    int last_within_margin  = this->within_margin_projection_intervals(i_h,1) ;
    
    if (last_on_dect == 0) {
      continue;
    }

    double angle_on_dect_first = this->internally_projection_angles_deg(first_on_dect);
    double angle_on_dect_last  = this->internally_projection_angles_deg(last_on_dect );
    double angle_within_margin_first = this->internally_projection_angles_deg(first_within_margin);
    double angle_within_margin_last  = this->internally_projection_angles_deg(last_within_margin );


    // TEST
    angle_within_margin_first = angle_on_dect_first ; 
    angle_within_margin_last = angle_on_dect_last ; 

    for( auto  dect_or_margin: std::vector<int>({0,1}) ) {
      
      line_collector.fill(0.0);

      double angle_min, angle_max;
      int i_proj_start, i_proj_end; 
      int has_enough_angles = 0 ;
      
      if(dect_or_margin == 0) {
	angle_min = std::min( angle_on_dect_first,  angle_on_dect_last ) ;
	angle_max = std::max( angle_on_dect_first,  angle_on_dect_last ) ;
	i_proj_start =  first_on_dect       ; 
	i_proj_end   =  last_on_dect  + 1   ;

	this->sunshine_starts(i_h) = i_proj_start;
	this->sunshine_ends(i_h) = i_proj_end;
	
      } else {
	angle_min = std::min( angle_within_margin_first,  angle_within_margin_last ) ;
	angle_max = std::max( angle_within_margin_first,  angle_within_margin_last ) ;
	i_proj_start =  first_within_margin      ; 
	i_proj_end   =  last_within_margin + 1    ; 
      }

      float angular_bonus  = fabs(avg_angular_step_deg * this->angular_tolerance_steps) ;
      float angular_delta = (angle_max - angle_min ) ; 
      has_enough_angles =  ( ( angular_delta +  angular_bonus )   >= this->angular_period_deg )   ;
      if( this->redundancy_angle_deg  )  {
	has_enough_angles = has_enough_angles && ( angular_delta  > 2*(this->redundancy_angle_deg + fabs(avg_angular_step_deg)  )  ) ; 
      }
      if(dect_or_margin == 0 &&  ! has_enough_angles) {
	this->sunshine_ends(i_h) = this->sunshine_starts(i_h) = -1 ;  // invalid tag	
      }
            


    }
    
  }
}

void  SpanInfo::set_chunk_info( int span_v_absolute_start, int span_v_absolute_end) {

  this->chunk_info.absolute_span_v = xt::xarray<int> {span_v_absolute_start ,
						      span_v_absolute_end  } ; 

  auto relative_span_v = xt::xarray<int> {span_v_absolute_start - this->total_view_heights(0),
					  span_v_absolute_end   - this->total_view_heights(0)} ; 
  


  int i_tmp_a = + ( 1 << 30 )  ;
  int i_tmp_b = - ( 1 << 30 ) ;
  for(int i=span_v_absolute_start ; i <  span_v_absolute_end +1 ; i++ ) {
    int k = i - this->total_view_heights(0);
    if (this->sunshine_starts(k) >=0) {
      i_tmp_a = std::min( i_tmp_a,    this->sunshine_starts(k)   );
      i_tmp_b = std::max( i_tmp_b,    this->sunshine_ends(k)   );
    }
  }
  // std::cout << "   BORDI " <<  i_tmp_a   << "       "  <<      i_tmp_b        << std::endl;

  this->chunk_info.angle_indexes =   xt::arange<int>(    std::min(i_tmp_a, i_tmp_b), std::max(i_tmp_a, i_tmp_b)   ) ; 


  auto &angle_indexes = this->chunk_info.angle_indexes ;
  this->chunk_info.angle_index_span = {xt::amin(angle_indexes)(), xt::amax(angle_indexes)() + 1};
  xt::xtensor<int,1> & angle_index_span =   this->chunk_info.angle_index_span; 

  xt::xtensor<double ,1> chunk_angles_deg =  xt::view( this->internally_projection_angles_deg, xt::keep( this->chunk_info.angle_indexes) );
  this->chunk_info.chunk_angles_rad = chunk_angles_deg *(2*M_PI/360.0)  *(1 - 2 *((int)  this->angles_are_inverted ) ) ;

  this->chunk_info.integer_shift_v = xt::view( this->integer_shift_v, xt::range(angle_index_span(0), angle_index_span(1)) );
  this->chunk_info.fract_complement_to_integer_shift_v = xt::view( this->fract_complement_to_integer_shift_v, xt::range(angle_index_span(0), angle_index_span(1)) );
  this->chunk_info.z_pix_per_proj = xt::view( this->z_pix_per_proj, xt::range(angle_index_span(0), angle_index_span(1)) );
  this->chunk_info.x_pix_per_proj = xt::view( this->x_pix_per_proj, xt::range(angle_index_span(0), angle_index_span(1)) );

  // the chunk info is passed to interpolation routines.
  // When angle-ruen interpolation is done on flats
  // the global angle array will be needed. Thank to the following line it will be
  // availabl from original_span_info as    chunk_info.original_span_info->internally_projection_angles_deg

  this->chunk_info.internally_projection_angles_deg   =  this->internally_projection_angles_deg ; 
}
      

