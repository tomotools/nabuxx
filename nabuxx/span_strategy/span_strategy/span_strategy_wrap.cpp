#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>
#include"span_info.h"

using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(span_strategy,m)
{
  xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for span_strategy";
  py::class_<nabuxx::SpanInfo>(m, "SpanStrategy")
    .def(pybind11::init<
	 float                , // z_offset_mm=self.z_offset_mm
	 std::vector<float>  &, // z_pix_per_proj,
	 std::vector<float> &,  //  x_pix_per_proj,
	 std::vector<int>   &,  //  detector_shape_vh,
	 int ,                  // phase_margin_pix,
	 std::vector<double>  &, // projection_angles_deg,
	 float , // pix_size_mm
	 bool                ,   // require_redundancy
	 float  // angular_tolerance_steps
	 >(),  py::arg("z_offset_mm"), py::arg("z_pix_per_proj"), py::arg("x_pix_per_proj"), py::arg("detector_shape_vh"), py::arg("phase_margin_pix"), py::arg("projection_angles_deg"), py::arg("pixel_size_mm")=float (0.1), py::arg("require_redundancy")=bool(0), py::arg("angular_tolerance_steps")=float(0.0),

	 R"V0G0N(
        This class does all the accounting for the doable slices, giving for each the list of the useful angles,
        of the vertical and horizontal shifts,and more ...

        Parameters
        ----------
        z_offset_mm :
            the offset in mm
        z_pix_per_proj : array of floats
            an array of floats with one entry per projection, in pixel units. The values are the vertical displacements of the detector 
            respect to the rotation axis.
            An increasing z means that the rotation axis is following the positive direction of the detector  vertical axis.
            In the experimental setup, the vertical detector axis is pointing toward the ground, and a  z_pix=0 value
            is given when the translation-rotation stage "ground" is exactly at the beam height. Therefore a positive z_pix means that the translation
            stage has been lowered in order to scan the sample that is above the translation-rotation stage.
        x_pix_per_proj : array of floats
            one entry per projection. The horizontal displacement of the detector respect to  the rotation axis.
            A positive x means that the sample shadow on the detector is moving toward the left of the detector.
             (the detector is going right)
        detector_shape_vh : a tuple of two ints
            the vertical and horizontal dimensions
        phase_margin_pix : int
            the  maximum margin needed for the different kernls (phase, unsharp..)
        projection_angles_deg :  array of doubles
            per each projection the rotation angle of the sample in degree.
        require_redundancy: bool, optional, defaults to False
            It can be set to true  to True, when dead zones in the detector exists.
            In this case the minimal required angular span is increased from 360 to  2*360
            in order to impose the required for the necessary redundancy, which allows the 
            correction of the dead zones. 
        angular_tolerance_steps: float defaults to 0
            the angular tolerance, an angular width expressed in units of an angular step, which is tolerated in 
            the criteria for deciding if a slice is reconstructable or not

        )V0G0N"
    	 )
    .def_property_readonly("pix_size_mm", [](nabuxx::SpanInfo &this_obj){
	return this_obj.pix_size_mm; 
      })
    .def_property_readonly("z_offset_mm", [](nabuxx::SpanInfo &this_obj){
	return this_obj.z_offset_mm; 
      })
    .def_property_readonly("sunshine_ends", [](nabuxx::SpanInfo &this_obj){
	return xt::pyarray<int>(this_obj.sunshine_ends); 
      })
    .def_property_readonly("z_pix_per_proj", [](nabuxx::SpanInfo &this_obj){
	return xt::pyarray<int>(this_obj.z_pix_per_proj); 
      })
    .def_property_readonly("sunshine_image", [](nabuxx::SpanInfo &this_obj){
	return xt::pyarray<float>(this_obj.sunshine_image); 
      })
    .def_property_readonly("sunshine_starts", [](nabuxx::SpanInfo &this_obj){
	return xt::pyarray<int>(this_obj.sunshine_starts); 
      })
    .def_property_readonly("projection_angles_deg", [](nabuxx::SpanInfo &this_obj){
       return xt::pyarray<double>(this_obj.projection_angles_deg); 
      })
    .def_property_readonly("projection_angles_deg_internally", [](nabuxx::SpanInfo &this_obj){
       return xt::pyarray<double>(this_obj.internally_projection_angles_deg); 
      })
    .def("get_doable_span", [](nabuxx::SpanInfo &this_obj){
       int view_height_min, view_height_max;			      
       float z_over_stage_pix_min=0, z_over_stage_pix_max=0 ; 
       float z_over_stage_mm_min=0 , z_over_stage_mm_max=0 ; 
       this_obj.get_doable_span(  view_height_min, view_height_max, z_over_stage_pix_min, z_over_stage_pix_max, z_over_stage_mm_min, z_over_stage_mm_max);

       py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");
       py::object ns = SimpleNamespace( "view_heights_minmax"_a = xt::pyarray<int>({view_height_min, view_height_max}),
					"z_pix_minmax"_a = xt::pyarray<float>({z_over_stage_pix_min, z_over_stage_pix_max}),
					"z_mm_minmax"_a = xt::pyarray<float>({z_over_stage_mm_min, z_over_stage_mm_max})
					);
       return ns;
       
      })
    .def("get_informative_string", [](nabuxx::SpanInfo &this_obj){
	return this_obj.get_informative_string(); 
      })
    .def("get_chunk_info",  [](nabuxx::SpanInfo &this_obj, py::array_t<int> span_v) {
	int span_v_absolute_start,  span_v_absolute_end;
	py::buffer_info buffer_info  = span_v.request();
	if( buffer_info.ndim!=1  || buffer_info.shape[0]!=2) {
	  throw std::invalid_argument("the span_v argument has not the right shape. It must be a 1D array with two int entries");
	}
	span_v_absolute_start = *((int*)buffer_info.ptr) ;
	span_v_absolute_end   = *((int*) ((char*)buffer_info.ptr + buffer_info.strides[0] ) ) ;
	// 
	// printf(" LIMITI %d %d %ld\n",  span_v_absolute_start,  span_v_absolute_end,  buffer_info.strides[0] ) ;

	  {
	    py::gil_scoped_release release;
	    this_obj.set_chunk_info( span_v_absolute_start, span_v_absolute_end);
	  }


	  py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");
	  py::object ns = SimpleNamespace(
					  "angle_index_span"_a= xt::pyarray<int>(this_obj.chunk_info.angle_index_span),					
					  "span_v"_a= xt::pyarray<int>(this_obj.chunk_info.absolute_span_v),
					  "integer_shift_v"_a= xt::pyarray<int>(this_obj.chunk_info.integer_shift_v),
					  "fract_complement_to_integer_shift_v"_a= xt::pyarray<float>(this_obj.chunk_info.fract_complement_to_integer_shift_v),
					  "z_pix_per_proj"_a= xt::pyarray<float>(this_obj.chunk_info.z_pix_per_proj),
					  "x_pix_per_proj"_a= xt::pyarray<float>(this_obj.chunk_info.x_pix_per_proj),
					  "angles_rad"_a= xt::pyarray<float>(this_obj.chunk_info.chunk_angles_rad),
					  "internally_projection_angles_deg"_a= xt::pyarray<double>(this_obj.internally_projection_angles_deg)
					  );
	  printf(" LIMITI  OK %d %d %ld\n",  span_v_absolute_start,  span_v_absolute_end,  buffer_info.strides[0] ) ;
	  
	  return ns;
      }, py::arg("span_v"))
    ;
 
}

