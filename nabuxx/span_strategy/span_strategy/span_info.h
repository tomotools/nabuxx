#ifndef SPAN_INFO_H
#define SPAN_INFO_H

#include <xtensor/xarray.hpp>

#include <xtensor/xtensor.hpp>
#include <pybind11/stl.h>
#include<vector>

namespace nabuxx {


  class SpanInfo ; 

  class ChunkInfo{
  public:

    xt::xtensor<int,1> absolute_span_v;
    xt::xtensor<float,2> sunshine_subset;
    xt::xtensor<float,1> angular_profile;
    xt::xtensor<int ,1> angle_indexes;
    xt::xtensor<int ,1> angle_index_span;
    xt::xtensor<double ,1> chunk_angles_rad;

    xt::xtensor<int ,1> integer_shift_v ; 
    xt::xtensor<float ,1> fract_complement_to_integer_shift_v;
    xt::xtensor<float ,1> z_pix_per_proj;
    xt::xtensor<float ,1> x_pix_per_proj;


    xt::xtensor<double ,1>  internally_projection_angles_deg; 
    
    /*
            "sunshine_subset",
            "angle_index_span",
            "span_v",
            "integer_shift_v",
            "fract_complement_to_integer_shift_v",
            "z_pix_per_proj",
            "x_pix_per_proj",
            "angles_rad",
            "angle_indexes",
            "angles_are_inverted",
    */

    
  };
  
  class SpanInfo  {
  public:
    SpanInfo(float z_offset_mm,
	     std::vector<float>  &z_pix_per_proj,
	     std::vector<float>  &x_pix_per_proj,
	     std::vector<int>    &detector_shape_vh,
	     int pag_margin_pix,
	     std::vector<double>  &projection_angles_deg,
	     float pix_size_mm,
	     bool require_redundancy,
	     float angular_tolerance_steps
	     );

  
    float _active_angular_span_of_a_line(xt::xarray<float> &line_collector, double avg_angular_step_deg   )  ;
    void _setup_ephemerides() ;
    void _setup_sunshine() ;
    void  set_chunk_info( int span_v_absolute_start, int span_v_absolute_end) ;

    const std::string  get_sunshine_ends_s() {return "ciao";};

    void get_doable_span( int &view_height_min, int &view_height_max,
			  float &z_over_stage_pix_min, float &z_over_stage_pix_max,
			  float &z_over_stage_mm_min, float &z_over_stage_mm_max
			  ) ;
    std::string get_informative_string() ;
    float  z_offset_mm; 
    xt::xtensor<float, 1>  z_pix_per_proj;
    xt::xtensor<float, 1>  x_pix_per_proj;
    xt::xtensor<int, 1>     detector_shape_vh;
    int pag_margin_pix;
    xt::xtensor<double, 1>   internally_projection_angles_deg;
    xt::xtensor<double, 1>   projection_angles_deg;
    float redundancy_angle_deg;
    
    int num_images ;
    float angular_period_deg;
    bool angles_are_inverted;

    xt::xtensor<float, 2> sunshine_image ;
    /* For introspection of what is going on, everything is based on the sunshine image,
        The sunshine image has two dimensions, the  second one is the projection number
        while the first is the height of a slice which could possibly be reconstructed.
        All the logic is based on this image. Where the normalisation factor is zero that
        correspond  to a pair   height,projection  for which there is no contribution of that 
        projection to that slice.
     */
    
    xt::xtensor<int, 1> total_view_heights ;
    /*
         This will be an array of heights. The heights integers are referred at the detector when
         the vertical  translation is given by the first element in self.z_pix_per_proj. A view height
         equal to zero corresponds to a slice which projects on row 0 when translation is given 
	 by the first value in self.z_pix_per_proj.
         Negative integers are possible too, accordin to the direction of translations.
     */

    xt::xtensor<int, 2> on_detector_projection_intervals ;
    /*  A list wich will contain for every reconstructable heigth, listed in 
        self.total_view_heights, and in the same order,  a pair of two integer, the first is the
        fist sequential number of the projection for which the height is projected 
        inside the detector, the second integer is the last projection number for which the 
        projection occurs inside the detector.
     */
    
    xt::xtensor<int, 2> within_margin_projection_intervals ;
    /*
    All like self.on_detector_projection_intervals, but considering also the Paganin margin.
    */

    xt::xtensor<int, 1> integer_shift_v  ;
    /* This will contain for every translation z_pix_per_proj value, so for every projection i_pro, 
       the integer value given    by ceil z_pix_per_proj[i_pro]
    */
    
    xt::xtensor<float, 1>  fract_complement_to_integer_shift_v ;
    /* The fractional vertical shifts are translation < 1.0 pixels. The integer part, is kept 
      apart and used later when it  intervenes for cropping  when the data are collected for a
       given to be reconstructed chunk
    */

    xt::xtensor<int, 1>  sunshine_starts ;
    xt::xtensor<int, 1>  sunshine_ends ;

    float pix_size_mm;

    ChunkInfo chunk_info;

    float angular_tolerance_steps;
  };

  
}
#endif
