% matlab -nodisplay -nojvm -nosplash -nodesktop -r "run('test.m')"
addpath("./") ; 
addpath("/home/esrf/tafforea/matlab") ; 
addpath("/home/esrf/tafforea/matlab/tomotools") ;

para.paganin_l_micron=247.8 ;
para.image_pixel_size=5.06
para.unsharp_sigma=0  
para.unsharp_coeff=0  
para.unsharp_LoG=0  

d=edfread(compose("5.06_crayon_W150_60_Al2_W0.25_xc1000_%04d.edf", 843)) ;
flat = edfread("refHST0000.edf") ;
dark = edfread("dark.edf") ;
flat = flat-dark;
dd=zeros(size(d),"single");

for i=0:1499
  d=edfread(compose("5.06_crayon_W150_60_Al2_W0.25_xc1000_%04d.edf",i)) ;

  dd(:) = (d-dark)./(flat) ;
  i
  res = paganin_pyhst2( dd, para)  ;
  edfwrite(compose("projpag_%04d.edf", i), res, "float32") ;
end

