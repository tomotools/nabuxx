struct PaganinParameters {
  int paganin_marge;
  float paganin_l_micron;
  float image_pixel_size_y;
  float image_pixel_size_x;
  float unsharp_sigma;
  float unsharp_coeff;
  int   unsharp_LoG;
  int pag_version ; 
};


void paganin(
	     float * RawptrA,
	     int n_images,
	     int  Size0,
	     int  Size1,
	     PaganinParameters P ,  
	     float *output,
	     int num_of_threads
	     ) ;



int       paganin_worker(
			  float * RawptrA,
			  int  Size0, int  Size1,
			  PaganinParameters P ,   
			  int pstart, int pend,
			  float *output
			  )  ;
