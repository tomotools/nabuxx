#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>
#include"paganin_filter.h"

using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(paganin,m)
{
  xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for Paganin phase retrieval";
  m.def("paganin_pyhst",  [](
			     py::buffer &data_raw,
			     py::buffer &output,
			     int num_of_threads,
			     int paganin_marge,
			     float paganin_l_micron,
			     float image_pixel_size_y,
			     float image_pixel_size_x,
			     float unsharp_sigma,
			     float unsharp_coeff,
			     int   unsharp_LoG,
			     int pag_version
			     )  {
	  

	   py::buffer_info raw_info  =  data_raw.request();
	   py::buffer_info output_info =  output.request() ;

	   if( raw_info.ndim != 2  && raw_info.ndim!=3) {
	     throw std::invalid_argument(" The paganin_pyst2 function works on an image or a stack of images. The number of dimensions is wrong" ) ;
	   }
	   if( raw_info.ndim != output_info.ndim) {
	     throw std::invalid_argument(" the input and the output must have the same number of dimensions" ) ;
	   }
	   if( raw_info.format[0] !=  'f' || output_info.format[0] != 'f' ) {
	     throw std::invalid_argument(" the input and the output must have float type" ) ;
	   }
	   int ndim= raw_info.ndim;
	   
	   int num_images, Size0, Size1;

	   Size1 =  raw_info.shape[ndim-1];
	   Size0 =  raw_info.shape[ndim-2];

	   if( ndim==2) {
	     num_images = 1;
	   } else {
	     num_images = raw_info.shape[ndim-3];
	   }


	   if(  raw_info.shape[ndim-1] != output_info.shape[ndim-1]  ) {
	     throw std::invalid_argument(" the input and the output must have the same x dimension" ) ;
	   }
	   if(  raw_info.shape[ndim-2] != (output_info.shape[ndim-2] + 2*paganin_marge)  ) {
	     throw std::invalid_argument(" the output y dimension must be equal to the raw y dimension minus twice the paganin marge" ) ;
	   }

	   {
	     py::gil_scoped_release release;
	     paganin(
		     (float * ) raw_info.ptr,
		     num_images,
		     Size0,
		     Size1,
		     {
		         .paganin_marge      = paganin_marge       ,
			 .paganin_l_micron   = paganin_l_micron   ,
			 .image_pixel_size_y = image_pixel_size_y ,
			 .image_pixel_size_x = image_pixel_size_x ,
			 .unsharp_sigma      = unsharp_sigma      ,
			 .unsharp_coeff      = unsharp_coeff      ,
			 .unsharp_LoG        = unsharp_LoG        ,
			 .pag_version        = pag_version        
		     } ,  
		     (float *) output_info.ptr,
		     num_of_threads
		     ) ;

	   }

	},
	py::arg("data_raw"),
	py::arg("output"),
	py::arg("num_of_threads")= -1 , 
	py::arg("paganin_marge")=10.0,
	py::arg("paganin_l_micron")= 10.0 , 
	py::arg("image_pixel_size_y")= 1.0 , 
	py::arg("image_pixel_size_x")= 1.0 , 
	py::arg("unsharp_sigma")= 0.0 , 
	py::arg("unsharp_coeff")= 0.0 , 
	py::arg("unsharp_LoG")=  0 , 
	py::arg("pag_version")=  1
	);

}

