#include<mex.h>
#include"paganin_filter.h"
#include<string>
#include<iostream>
#include<string.h>
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    float *input=NULL, *output=NULL;
    int i, j, n_rows, Size0,  Size1;
    double avg;
    const mxArray *fPtr;    /* field pointer */
    double *realPtr;        /* pointer to data */

    if ((nrhs != 2) || (nlhs != 1)) {
      mexErrMsgIdAndTxt("Paganin:NumberOfArguments",
			"Error! Expecting exactly 2 rhs and 1 lhs argument!");
    }

    if( !mxIsSingle (prhs[0])  ) {
      mexErrMsgIdAndTxt("Paganin:NonFloatKind",
			"single precision input");
    }

    // input = mxGetPr(prhs[0]); // ( double *)
    input = (float*) mxGetData(prhs[0]);
    Size1 = mxGetM(prhs[0]);
    Size0 = mxGetN(prhs[0]);

    if (nlhs == 1) {
        plhs[0] = mxCreateNumericMatrix(Size1, Size0, mxSINGLE_CLASS,mxREAL);
        output = (float *)  mxGetData(plhs[0]);
    }

    if( !mxIsStruct( prhs[1]  ) ) {
      mexErrMsgIdAndTxt("Paganin:NonStructArgument",
			"Second argument must be a struct with Paganin parameters"
			);
    }
    int nfields = mxGetNumberOfFields(prhs[1]);
    char * names[] = {"paganin_l_micron", "image_pixel_size", "unsharp_sigma", "unsharp_coeff", "unsharp_LoG"};
    if( nfields != 5 ) {
      char message[10000];
      sprintf(message,"Error in parameters number. You passed :\n");
      for (int i = 0; i < nfields; i++) {
	sprintf(message + strlen(message)," %d)    %s\n", i, mxGetFieldNameByNumber(prhs[1],i) );
      }
      sprintf(message + strlen(message),"While the following are required :\n");
      for (int i = 0; i < 5; i++) {
	sprintf(message + strlen(message),"    %d) %s\n", i, names[i]);
      }

      
      mexErrMsgIdAndTxt("Paganin:WrongNumberOfParameters",
			message
			);
    }

    double values[5];
    for (int i=0; i<5; i++) {
      fPtr =  mxGetField(prhs[1], 0, names[i]) ;
      if (fPtr == NULL)  {
	char message[1000];
	sprintf(message,"parameters are missing %s"  , names[i] );	
	mexErrMsgIdAndTxt("Paganin:MissingPar",
			  message
			  );
      }
      if ( !(mxGetClassID(fPtr) == mxDOUBLE_CLASS)  || (mxIsComplex(fPtr))) {
	char message[1000];
	sprintf(message," %s is not scalar double precision"  , names[i] );
	mexErrMsgIdAndTxt("Paganin:WrongParType",
			  message
			  );
      }
      double x =  (mxGetPr(fPtr))[0];
      values[i] = x;
      // std::cout <<  names[i] << "  " << x <<  std::endl ;

    }
    int num_of_threads = 1 ;
    int num_images = 1;
    paganin(
	    input,
	    num_images,
	    Size0,
	    Size1,
	    {
	     .paganin_marge      = (int)   0  ,
	     .paganin_l_micron   = (float) values[0] ,
	     .image_pixel_size_y = (float) values[1] ,
	     .image_pixel_size_x = (float) values[1] ,
	     .unsharp_sigma      = (float) values[2] ,
	     .unsharp_coeff      = (float) values[3] ,
	     .unsharp_LoG        = (int)   values[4] 
	    } ,  
	    (float *) output,
	    num_of_threads
	    ) ;
    
      

}


