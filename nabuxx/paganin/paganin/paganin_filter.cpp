#include<omp.h>
#include<future>
#include<mutex>
#include <fftw3.h>
#include<complex.h>
#include"paganin_filter.h"
#include<algorithm>

std::mutex fftw_mutex;
typedef  float __complex__   fcomplex ;
int roundfft(const long int N) ;

void paganin(
	     float * RawptrA,
	     int n_images,
	     int  Size0,
	     int  Size1,
	     PaganinParameters P ,  
	     float *output,
	     int num_of_threads
	     )  {


  if ( num_of_threads==-1) {
    num_of_threads = omp_get_max_threads();
    if( num_of_threads/2 > 4) num_of_threads /= 2;
  } else {
    num_of_threads = num_of_threads;
  }

  // printf(" Multithreaded Paganin from nabuxx num_of_threads = %d\n", num_of_threads);
  int np_per_thread = n_images / num_of_threads;
  if ( (np_per_thread *num_of_threads ) < n_images ) {
    np_per_thread++;
  }
  

  std::future<int> retvals[num_of_threads];
  for(int i=0; i< num_of_threads; i++) {


    int p_start = i* np_per_thread;
    int p_end   = (i + 1)* np_per_thread;
    if( p_end > n_images) p_end = n_images ; 

    retvals[i] = std::async( paganin_worker ,   RawptrA,  Size0, Size1, P, p_start, p_end, output          );
    
  }
  for(int i=0; i< num_of_threads; i++) {
    retvals[i].get();
  }
}

  
int       paganin_worker(
			  float * RawptrA,
			  int  Size0, int  Size1,
			  PaganinParameters P ,   
			  int pstart, int pend,
			  float *output
			  )  {

  int Pos0  = 0;
  int Pos1  = 0;
  int pos0  = P.paganin_marge;
  int pos1  = 0;
  int size0 = Size0 - 2*P.paganin_marge;
  int size1 = Size1;

  int pos_pa0,   pos_pa1,   size_pa0,   size_pa1;
  int posEnd_pa0,  posEnd_pa1;
  int siz, siz2n, diff ;
  int i,j, I0, I1;
  float *f0 , *f1;
  fcomplex *gn0, *gn1;
  float sum_gn0, sum_gn1;
  float  * auxbuffer ;
  float  * kernelbuffer ;
  float *fgn0, *fgn1;
  float dum;
  fftwf_plan plan0, plan1;
  fftwf_plan plan2D_forward, plan2D_backward ;
  long int npj;
  float * Rawptr;

  // printf("  IN PAGANIN ROUTINE  pstart pend %d %d    shape %d %d\n", pstart, pend,  size0, size1);


  auxbuffer = NULL ;
  kernelbuffer = NULL ;

  size_pa0 =   std::max(size0 +2*P.paganin_marge,Size0);
  size_pa1 =   std::max(size1 +2*P.paganin_marge,Size1);

  pos_pa0    =  pos_pa1    = 0 ;
  posEnd_pa0 =  posEnd_pa1 = 0 ;

  pos_pa0 = pos0-(std::max( pos0 -Pos0, P.paganin_marge) );
  posEnd_pa0 = (pos0+size0) + ( std::max((Pos0+Size0) -(pos0+size0 ),     P.paganin_marge) );
  siz = posEnd_pa0 - pos_pa0 ;
  // siz2n = (int ) ( exp(log(2.0)* ( (int) ( log(siz*1.0)/log(2.0)+0.9999999) ) )  +0.999999 );
  siz2n = roundfft(siz);
  diff=siz2n-siz;
  pos_pa0    =  pos_pa0 - diff/2 ;
  posEnd_pa0 =  posEnd_pa0 + ( diff-diff/2) ;
  size_pa0 =  siz2n ;

  pos_pa1 = pos1-(std::max( pos1 -Pos1,P.paganin_marge) );
  posEnd_pa1 = (pos1+size1) + ( std::max ((Pos1+Size1) -(pos1+size1 ),     P.paganin_marge) );
  siz = posEnd_pa1 - pos_pa1 ;
  // siz2n = (int ) ( exp(log(2.0)* ( (int) ( log(siz*1.0)/log(2.0)+0.9999999) ) )  +0.999999 );
  siz2n =  roundfft(siz); 
  diff=siz2n-siz;
  pos_pa1    =  pos_pa1 - diff/2 ;
  posEnd_pa1 =  posEnd_pa1 + ( diff-diff/2) ;
  size_pa1 =  siz2n ;

  f0  =(float*) malloc(sizeof(float)*size_pa0);
  f1  =(float*) malloc(sizeof(float)*size_pa1);


  {
    std::lock_guard<std::mutex> guard(fftw_mutex);    
    gn0 = (fcomplex*) fftwf_malloc(sizeof(fftwf_complex) *size_pa0 );
    gn1 = (fcomplex*) fftwf_malloc(sizeof(fftwf_complex) *size_pa1 );
  }

  
  fgn0 =(float*) malloc(sizeof(float)*size_pa0);
  fgn1 =(float*) malloc(sizeof(float)*size_pa1);


  for( i=0; i<=(size_pa0)/2; i++) {
    f0[i] = ((float)i)/size_pa0/P.image_pixel_size_y *P.paganin_l_micron   ;
    fgn0[i] = ((float)i)/size_pa0 *2*M_PI;
  }

  for( i=0; i<=(size_pa1)/2; i++) {
    f1[i] = ((float)i)/size_pa1/P.image_pixel_size_x *P.paganin_l_micron   ;
    fgn1[i] = ((float)i)/size_pa1 *2*M_PI;
  }

  for(i= (size_pa0)/2+1 ; i< size_pa0 ; i++) {
    f0[i] = ((float)( i - size_pa0) )/size_pa0/P.image_pixel_size_y *P.paganin_l_micron ;
    fgn0[i] = ((float)( i - size_pa0) )/size_pa0*2*M_PI  ;
  }

  for(i= (size_pa1)/2+1 ; i< size_pa1 ; i++) {
    f1[i] = ((float)( i - size_pa1) )/size_pa1/P.image_pixel_size_x *P.paganin_l_micron ;
    fgn1[i] = ((float)( i - size_pa1) )/size_pa1*2*M_PI  ;
  }

  if ( P.unsharp_sigma >0.0)   {
    for(i=0; i<size_pa0; i++) {
      dum = i*i/(2*P.unsharp_sigma*P.unsharp_sigma) ;
      ((float*)gn0)[2*i] = exp(  - std::min(  dum,(float) 100.0  ) ) ;
      ((float*)gn0)[2*i+1] =  0    ;
    }    
    sum_gn0=((float*)gn0)[0];
    for(i=1; i<size_pa0; i++) {
      ((float*)gn0)[2*i] = ((float*)gn0)[2*i] + ((float*)gn0)[2*(size_pa0- i)]  ;
      sum_gn0+= ((float*)gn0)[2*i] ;
    }
    for(i=0; i<size_pa1; i++) {
      dum = i*i/(2*P.unsharp_sigma*P.unsharp_sigma) ;
      ((float*)gn1)[2*i] = exp(  - std::min(  dum, (float)100  ) ) ;
      ((float*)gn1)[2*i+1] =  0    ;
    }
    sum_gn1=((float*)gn1)[0];
    for(i=1; i<size_pa1; i++) {
      ((float*)gn1)[2*i] = ((float*)gn1)[2*i] + ((float*)gn1)[2*(size_pa1- i)]  ;
      sum_gn1+= ((float*)gn1)[2*i] ;
    }

    for(i=0; i<size_pa0; i++) {
      gn0[i]/= sum_gn0;
    }
    for(i=0; i<size_pa1; i++) {
      gn1[i]/= sum_gn1;
    }

    // .......  loccare tutto fuorche execute .....
    {
      std::lock_guard<std::mutex> guard(fftw_mutex);    
      // fftwf_plan_with_nthreads(1 ); // controllare che sia 1 per default dove deve esserlo
      // nor is there an FFTW_IN_PLACE flag. You simply pass the same pointer for both the input and output arguments.
      plan0 = fftwf_plan_dft_1d(size_pa0,(fftwf_complex* ) gn0 ,(fftwf_complex* )gn0 , FFTW_FORWARD, FFTW_ESTIMATE);
      plan1 = fftwf_plan_dft_1d(size_pa1,(fftwf_complex* ) gn1 ,(fftwf_complex* )gn1 , FFTW_FORWARD, FFTW_ESTIMATE);
    }

    fftwf_execute(plan0);
    fftwf_execute(plan1);


    {
      std::lock_guard<std::mutex> guard(fftw_mutex);    
      fftwf_destroy_plan(plan0);
      fftwf_destroy_plan(plan1);
    }
  }

  {

    std::lock_guard<std::mutex> guard(fftw_mutex);    

    auxbuffer     = (float *) fftwf_malloc(sizeof(fftwf_complex) *size_pa0*size_pa1 );
    kernelbuffer  = (float *) fftwf_malloc(sizeof(fftwf_complex) *size_pa0*size_pa1 );
  
    // fftwf_plan_with_nthreads(1 ); // controllare che sia 1 per default dove deve esserlo

    plan2D_forward = fftwf_plan_dft_2d(size_pa0,size_pa1 ,
				       (fftwf_complex*)auxbuffer,(fftwf_complex*)auxbuffer,
				       FFTW_FORWARD         ,FFTW_MEASURE );
    plan2D_backward = fftwf_plan_dft_2d(size_pa0,size_pa1 ,
					(fftwf_complex*)auxbuffer,(fftwf_complex*)auxbuffer,
					FFTW_BACKWARD         ,FFTW_MEASURE );
  }

  {
// #pragma	omp parallel for private(i, j) num_threads (ncpus)


    // f0[i] = ((float)i)/size_pa0/P.image_pixel_size_y *P.paganin_l_micron   ;
    // fgn0[i] = ((float)i)/size_pa0 *2*M_PI;

    float pfact = P.paganin_l_micron /P.image_pixel_size_x/2.0/M_PI ;
    pfact = 2*pfact*pfact ; 

    if (P.pag_version == 1 ) {
      // printf("PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 PAG VERSION 1 \n" );
      for(i=0; i<size_pa0; i++) {
	for(j=0; j< size_pa1 ; j++) {
	  ( auxbuffer) [ (i*size_pa1+j)*2  ] =  1.0/((1.0  + f0[i]*f0[i] +  f1[j]*f1[j]         )* size_pa0* size_pa1* size_pa0* size_pa1);
	  ( auxbuffer) [ (i*size_pa1+j)*2 +1 ] =0 ;
	}
      }
    } else {
      // printf("PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 PAG VERSION 2 \n" );
      for(i=0; i<size_pa0; i++) {
	for(j=0; j< size_pa1 ; j++) {
	  ( auxbuffer) [ (i*size_pa1+j)*2  ] =  1.0/((1.0  + (2-cos(fgn0[i])-cos(fgn1[j]))*pfact)* size_pa0* size_pa1* size_pa0* size_pa1);
	  ( auxbuffer) [ (i*size_pa1+j)*2 +1 ] =0 ;
	}
      }
    }
    
    fftwf_execute(plan2D_backward );
    
    // #pragma	omp parallel for private(i, j) num_threads (ncpus)
    if(P.paganin_marge) {
      for(i= P.paganin_marge; i<size_pa0- P.paganin_marge; i++) {
	for(j=0; j< size_pa1 ; j++) {
	  ( auxbuffer) [ (i*size_pa1+j)*2  ]   =  0;
	  ( auxbuffer) [ (i*size_pa1+j)*2 +1 ] =  0 ;
	}
      }
    }
    
    /* for(j=0; j< size_pa0 ; j++) { */
    /*   for(i= P.paganin_marge; i<size_pa1- P.paganin_marge; i++) { */
    /* ( auxbuffer) [ (j*size_pa1+i)*2  ]   =  0; */
    /* ( auxbuffer) [ (j*size_pa1+i)*2 +1 ] =  0 ; */
    /*   } */
    /* } */
    
    fftwf_execute(plan2D_forward );
    
    
    if ( P.unsharp_sigma >0.0)   {
      // if( omp_get_thread_num() == 0 ) {
      // 	printf( " applying unsharp to the  kernel buffer. Number of thread is %d \n", omp_get_num_threads());
      // }
      if(P.unsharp_LoG){
	for(i=0; i<size_pa0; i++) {
	  for(j=0; j< size_pa1 ; j++) {
	    ((fcomplex *)auxbuffer)[ (i*size_pa1+j)  ]*=(1.0 +P.unsharp_coeff*((2-2*cos(fgn0[i]))+(2-2*cos(fgn1[j])))*gn0[i]*gn1[j]);
	  }
	}
      } else {
	for(i=0; i<size_pa0; i++) {
	  for(j=0; j< size_pa1 ; j++) {
	    ((fcomplex *)auxbuffer)[ (i*size_pa1+j)  ] *= ((1.0+ P.unsharp_coeff) -P.unsharp_coeff *((fcomplex *) gn0 )[i]  * ((fcomplex *) gn1 )[j] )    ;
	  }
	}
      }
    }
    // #pragma	omp parallel for private(i, j) num_threads (ncpus)
    for(i=0; i<size_pa0; i++) {
      for(j=0; j< size_pa1 ; j++) {
	(kernelbuffer ) [ (i*size_pa1+j)*2  ]   = ( auxbuffer) [ (i*size_pa1+j)*2  ] ;
	(kernelbuffer ) [ (i*size_pa1+j)*2 +1 ] = ( auxbuffer) [ (i*size_pa1+j)*2 +1 ]  ;
      }
    }
  }
  
  
  for(npj=pstart; npj<pend  ; npj+=2) {
    
    Rawptr = RawptrA + npj*Size0*Size1 ;

    long int proj_step = Size0*Size1;
    if(npj+1>pend-1 ) {
      proj_step = 0; 
    }
    
    for(i=0; i< size_pa0 ; i++) {
      I0 = i -(Pos0 -pos_pa0) ;
      I0=I0%(2*Size0) ;
      if(I0<0) {
	I0 = I0 +  2*Size0 ;
      }
      if(I0>=Size0)  {
	I0=2*(Size0)-1 - I0;
      }
      for(j=0; j< size_pa1 ; j++) {
	I1 = j -(Pos1-pos_pa1) ;
	I1=I1%(2*Size1) ;
	if(I1<0) {
	  I1 = I1 +  2*Size1 ;
	}
	if(I1>=Size1)  {
	  I1=2*(Size1)-1 - I1;
	}
	( auxbuffer) [ (i*size_pa1+j)*2  ] =  Rawptr[I0*Size1+I1]  ;
	( auxbuffer) [ (i*size_pa1+j)*2+1] =  Rawptr[I0*Size1+I1 + proj_step] ;
      }
    }

    
    int dowithcpu=1 ;


    if(dowithcpu) {
      fftwf_execute(plan2D_forward );
      
      for(i=0; i<size_pa0; i++) {
	for(j=0; j< size_pa1 ; j++) {
	  ((fcomplex *)auxbuffer)[ (i*size_pa1+j)]=((fcomplex *)auxbuffer)[(i*size_pa1+j)]*((fcomplex *)kernelbuffer)[(i*size_pa1+j)];
	}
      }

      
      fftwf_execute(plan2D_backward );

      if(npj+1>pend-1 ) {
	for(i=   ( Pos0 - pos_pa0  ) + P.paganin_marge  ; i<( Pos0 - pos_pa0  )  + Size0  - P.paganin_marge ; i++) {
	  for(j=( Pos1 - pos_pa1  ); j< ( Pos1 - pos_pa1) + Size1 ; j++) {

	    // Rawptr [ (i-(Pos0-pos_pa0))*Size1 +j-(Pos1-pos_pa1)  ]=  ;

	    output[ npj*size0*size1  +  (i-(Pos0-pos_pa0) - P.paganin_marge )*size1 +j-(Pos1-pos_pa1)      ] = ((float *)  auxbuffer)[ (i*size_pa1+j)*2  ] ; 
	    
	  }
	}
      } else {
	for(i=   ( Pos0 - pos_pa0  ) + P.paganin_marge ; i<( Pos0 - pos_pa0  ) + Size0 - P.paganin_marge ; i++) {
	  for(j=( Pos1 - pos_pa1  ); j< ( Pos1 - pos_pa1) + Size1 ; j++) {
	    //  Rawptr [ (i-(Pos0-pos_pa0))*Size1 +j-(Pos1-pos_pa1)  ]=
	    output[ npj*size0*size1  +  (i-(Pos0-pos_pa0) - P.paganin_marge )*size1 +j-(Pos1-pos_pa1)      ] = ((float *)  auxbuffer)[ (i*size_pa1+j)*2  ] ;
	    
	    // Rawptr [ (i-(Pos0-pos_pa0))*Size1 +j-(Pos1-pos_pa1) + proj_step ]
	    output[ (npj+1)*size0*size1  +  (i-(Pos0-pos_pa0) - P.paganin_marge )*size1 +j-(Pos1-pos_pa1)  ] = ((float *)  auxbuffer)[ (i*size_pa1+j)*2  +1] ;
	    
	  }
	}
      }
    } 
  }

  {
    free(f0);
    free(f1);
    free(fgn0);
    free(fgn1);
  }
  

  {
    std::lock_guard<std::mutex> guard(fftw_mutex);    

    fftwf_free(gn0);
    fftwf_free(gn1);
    if(auxbuffer) fftwf_free(auxbuffer);
    if(kernelbuffer) fftwf_free(kernelbuffer);
    fftwf_destroy_plan(plan2D_forward);
    fftwf_destroy_plan(plan2D_backward);
    
  }

  return 1;
}
int roundfft(const long int N) {
  // long int MA=0,MB=0,MC=0,MD=0,ME=0,MF=0 ;
  const long int FA=2,FB=3,FC=5,FD=7,FE=11,FFF=13;
  long int DIFF=9999999999;
  long int   RES=1;

  long int   AA=1;
  long int BB,CC,DD,EE,FF;
  int A,B,C,D,E,F;
  for ( A=0; A <   (int) (log(1.0*N)/log(1.0*FA)+2); A++ ) {
    BB=AA ;
    for ( B=0; B <   (int) (log(1.0*N)/log(1.0*FB)+2); B++ ) {
      CC=BB ;
      for ( C=0; C <   (int) (log(1.0*N)/log(1.0*FC)+2); C++ ) {
	DD=CC ;
	for ( D=0; D <   (int) (log(1.0*N)/log(1.0*FD)+2); D++ ) {
	  EE=DD ;
	  for( E = 0; E<2; E++)  {
	    FF = EE; 
	    for( F = 0; F<2-E; F++) {
	      if(FF>N && DIFF>abs(N-FF)) {
		/* MA = A; */
		/* MB = B; */
		/* MC = C; */
		/* MD = D; */
		/* ME = E; */
		/* MF = F; */
		
		DIFF = abs(N-FF) ; 
		RES = FF;
		//printf("%ld %ld %ld %ld %ld %ld   %ld %ld %ld\n", A,B,C,D,E,F , DIFF, FF,N) ;
	      }
	      if(FF>N) break;
	      FF *= FFF;
	    }
	    if(EE>N) break;
	    EE *= FE;
	  }
	  if(DD>N) break;
	  DD *= FD;
	}
	if(CC>N) break;
	CC *= FC;
      }
      if(BB>N) break;
      BB *= FB;
    }
    if(AA>N) break;
    AA *= FA;
  }
  return RES;
}
