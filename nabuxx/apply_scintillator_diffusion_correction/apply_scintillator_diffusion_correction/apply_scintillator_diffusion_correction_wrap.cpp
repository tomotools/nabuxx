#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>
#include<algorithm>

#define FORCE_IMPORT_ARRAY
  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include<omp.h>

using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword



// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(apply_scintillator_diffusion_correction,m)
{
  m.doc() = "nabu c/c++ acceleration for apply_scintillator_diffusion_correction";
  m.def("apply_scintillator_diffusion_correction",
	[](
	   py::buffer &data,
	   py::buffer &rescaling_factors,
	   py::buffer &correction,
	   int bin_proj,
	   int bin_pix,
	   py::buffer & processed_projs_nums,
	   int start_y, 
	   int corr_start_proj_num, // coordinates relative to the global correction stack which is binned by bin_proj and bin_pix
	   int corr_start_y,
	   int num_of_threads
	   )
	{
	  
	  if ( num_of_threads==-1) {
	    num_of_threads = omp_get_max_threads();
	    if( num_of_threads/2 > 4) num_of_threads /= 2;
	  } else {
	    num_of_threads = num_of_threads;
	  }
	  py::buffer_info data_info   =  data.request();
	  py::buffer_info correction_info =  correction.request() ;
	  py::buffer_info processed_projs_nums_info =  processed_projs_nums.request() ;
	  py::buffer_info rescaling_factors_info =  rescaling_factors.request() ;
	  
	  if( data_info.ndim!=3) {
	    throw std::invalid_argument(" The apply_scintillator_diffusion_correction function works on stack of images. The number of dimensions is wrong" ) ;
	  }
	  if( correction_info.ndim != 3) {
	    throw std::invalid_argument(" correction stack must have three dimensions in apply_scintillator_diffusion_correction" ) ;
	  }
	  
	  if( processed_projs_nums_info.ndim != 1) {
	    throw std::invalid_argument(" correction stack must be an array with the original projection number for each radio of the data stack in function apply_scintillator_diffusion_correction" ) ;
	  }
	  
	  if( rescaling_factors_info .ndim != 1) {
	    throw std::invalid_argument(" rescaling_factors stack must be an array of dimension 1") ;
	  }
	  
	  if( rescaling_factors_info.format[0] !=  'f' ) {
	    throw std::invalid_argument(" the rescaling factors must have float type" ) ;
	  }
	  
	  if( data_info.format[0] !=  'f' || correction_info.format[0] != 'f' ) {
	    throw std::invalid_argument(" the data and the correction stack arguments must have float type" ) ;
	  }
	  
	  if( processed_projs_nums_info.format[0] !=  'i'  ) {
	    throw std::invalid_argument(" the data and the correction stack arguments must have integer type" ) ;
	  }
	  
	  int nradios =  data_info.shape[0];
	  int n_y =  data_info.shape[1];
	  int n_x =  data_info.shape[2];


	  if( rescaling_factors_info.shape[0] !=  nradios ) {
	    throw std::invalid_argument(" the rescaling_factors array must have the same size as the radios" ) ;
	  }
	  
	  int c_nradios =  correction_info.shape[0];
	  int c_n_y     =  correction_info.shape[1];
	  int c_n_x     =  correction_info.shape[2];
	  
	  int *p_nums = (int*) processed_projs_nums_info.ptr;
	  int c_start_p = corr_start_proj_num;
	  int c_start_y  = corr_start_y;

	  float * data_ptr = (float*) data_info.ptr ; 
	  float * correction_ptr = (float*) correction_info.ptr ;
	  
	  float * rescaling_factors_ptr = (float*) rescaling_factors_info.ptr ; 
	  
	  int bottom_throw=0;
	  int top_throw=0;
	  int y_bottom_throw=0;
	  int y_top_throw=0;

	  
#pragma omp parallel for  reduction(+:bottom_throw, top_throw, y_bottom_throw, y_top_throw )  num_threads(num_of_threads)
	  for(int ipro = 0; ipro < nradios; ipro++) {
	    // the global index of the radio
	    int gid_pro = p_nums[ipro];
	    
	    // where we need to interpolate in the correction stack
	    float fintp = ( gid_pro - c_start_p * bin_proj ) * 1.0 / bin_proj  ;
	    if ( fintp < 0 ) {
	      bottom_throw=1;
	      fintp = 0;
	    }
	    
	    // allow for some missing margin at the end of the binned correction due to the possibly last missing step but not too much
	    if ( fintp > c_nradios + 1 ) {
	      top_throw=1;
	      fintp = c_nradios + 1 ;
	    }
	    
	    int c_pro0 = (int) fintp;
	    float c_fp   = fintp - c_pro0;
	    
	    c_pro0 = std::min(c_pro0, c_nradios-1);
	    int c_pro1 = std::min(c_pro0 +1 , c_nradios-1);
	    
	    if( c_pro0 == c_pro1) {
	      c_fp=0;
	    }
	    
	    // two lines for further interpolation first on the projection, then in y, and finally along x.
	    // 
	    float correction_line[c_n_x +1] ;
	    
	    float finty = ( ( start_y + 0 ) - c_start_y * bin_pix ) * 1.0 / bin_pix  ;
	    if ( finty < 0 ) {
	      y_bottom_throw=1;
	      finty=0 ; 
	    }
	    
	    finty = ( ( start_y + n_y - 1 ) - c_start_y * bin_pix ) * 1.0 / bin_pix  ;
	    if ( finty > c_n_y + 1 ) {
	      y_top_throw=1;
	      finty = c_n_y + 1 ; 
	    }
	    
	    for(int iy = 0; iy < n_y; iy++) {
	      
	      finty = ( ( start_y + iy ) - c_start_y * bin_pix ) * 1.0 / bin_pix  ;
	      
	      int c_y0 = (int) finty;
	      float c_fy   = finty - c_y0;
	      
	      c_y0 = std::min(c_y0 , c_n_y - 1 );
	      int c_y1 = std::min(c_y0 + 1 , c_n_y - 1 );
	      
	      if( c_y0 == c_y1) {
		c_fy=0;
	      }
	      
	      assert(     c_y0   < c_n_y ) ; 
	      assert(     c_y1   < c_n_y ) ; 
	      assert(     c_pro0 < c_nradios ) ; 
	      assert(     c_pro1 < c_nradios ) ; 
	      assert(     c_pro0 >= 0 ) ; 
	      assert(     c_pro1 >= 0 ) ; 

	      for( int icx = 0; icx < c_n_x ;  icx++ ) {
		// interpolation over proj for c_y0
		float tmp0 = (
			      correction_ptr[(  c_pro0  * c_n_y  + c_y0)*c_n_x + icx    ] *(1 - c_fp) +
			      correction_ptr[(  c_pro1  * c_n_y  + c_y0)*c_n_x + icx    ] * c_fp
			      );
		// interpolation over proj for c_y1
		float tmp1 = (
			      correction_ptr[(  c_pro0  * c_n_y  + c_y1)*c_n_x + icx    ] *(1 - c_fp) +
			      correction_ptr[(  c_pro1  * c_n_y  + c_y1)*c_n_x + icx    ] * c_fp
			      );
		
		correction_line[icx] = tmp0 *( 1 - c_fy)  + tmp1* c_fy ;
	      }
	      
	      // add one more to remove the need of the "if" condition over the border
	      correction_line[c_n_x] = correction_line[c_n_x - 1];
	      
	      // now correct the whole data line at [ ipro, iy]
	      for( int ix = 0; ix < n_x ;  ix++ ) {
		
		float fx = ix * 1.0 / bin_pix ;
		int icx0 = (int) fx;
		
		fx = fx - icx0 ;
		int icx1 = icx0 + 1;

		assert(     icx1 <= c_n_x ) ; 
		
		float tmp =  correction_line[icx0] * (1-fx) + correction_line[icx1] * fx ;
		
		data_ptr[(  ipro  * n_y  + iy )*n_x + ix    ] += tmp * rescaling_factors_ptr[ipro];
	      }	      
	    }
	  }

	  if ( bottom_throw || top_throw || y_bottom_throw || y_top_throw    )  {
	     throw std::domain_error(" Framing problem with the correction array in apply_scintillator_diffusion_correction" ) ;
	  }
	},
	py::arg("data"),
        py::arg("rescaling_factors"),
	py::arg("correction"),
	py::arg("bin_proj"), 
	py::arg("bin_pix"),
	py::arg("processed_projs_num"), 
	py::arg("start_y"), 
	py::arg("corr_start_proj_num") ,
	py::arg("corr_start_y") , 
	py::arg("num_of_threads")=  -1
	);
  
}

