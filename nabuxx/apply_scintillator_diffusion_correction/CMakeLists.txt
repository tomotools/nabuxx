cmake_minimum_required(VERSION 3.16)

project (apply_scintillator_diffusion_correction/ LANGUAGES CXX  C)
cmake_minimum_required(VERSION 3.8)
message(STATUS "Building for system processor ${CMAKE_SYSTEM_PROCESSOR}")
include(CheckCXXCompilerFlag)

option(FOR_PYTHON "for_python" ON )

find_package(PkgConfig REQUIRED)


if ( ${FOR_PYTHON} ) 

  if(DEFINED ENV{CXX_AUXILIARY_PACKAGES_DIR})
    set( CXX_DIR  "$ENV{CXX_AUXILIARY_PACKAGES_DIR}"   ) 
  else()
    set( CXX_DIR  "/scisoft/tomotools_env"   ) 
  endif()
  
  find_package(pybind11  CONFIG)
  message(STATUS  "pybind11   "  "${pybind11_VERSION}" )
  
  if (  ${pybind11_VERSION} VERSION_LESS 2.9   )
    message(STATUS  "setting manually pybind11_INCLUDE_DIRS")
    if(EXISTS "${CXX_DIR}/cxx_packages/src/pybind11/include")
      set( pybind11_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/pybind11/include"  "${pybind11_INCLUDE_DIRS}")
    else()
      message(FATAL_ERROR "the package pybind11 found, but it is too old, and the auxiliary sources have not been found in ${CXX_DIR}/cxx_packages/src/pybind11/include")
    endif()
    
  endif()
  

  find_package(xtensor QUIET)
  find_package(xtensor-python QUIET)

  message(STATUS  "searching xtensor has given xtensor_FOUND=${xtensor_FOUND}" )

  if(NOT ${xtensor_FOUND})
    message(STATUS  "setting manually xtensor_INCLUDE_DIRS" )
    if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor/include")
      set( xtensor_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/xtensor/include;${CXX_DIR}/cxx_packages/src/xtl/include")
    else()
      message(FATAL_ERROR "the package xtensor was not found, neither was the auxiliary directory with headers.")
    endif()
  endif(NOT ${xtensor_FOUND})
  
  message(STATUS  "searching xtensor-python has given xtensor_FOUND=${xtensor-python_FOUND}" )
  if(NOT ${xtensor-python_FOUND})
    message(STATUS  "adding manually to xtensor_INCLUDE_DIRS" )
    if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor-python/include")
      set( xtensor_INCLUDE_DIRS   ${xtensor_INCLUDE_DIRS} ${CXX_DIR}/cxx_packages/src/xtensor-python/include)
    else()
      message(FATAL_ERROR "the package xtensor-python was not found, neither was the auxiliary directory with headers.")  
    endif()
  endif(NOT ${xtensor-python_FOUND})




set(Python3_FIND_VIRTUALENV FIRST)
find_package(Python3 COMPONENTS Interpreter Development REQUIRED)

# Ottieni la versione di Python
execute_process(
  COMMAND "${Python3_EXECUTABLE}" -c "import sys; print(f'{sys.version_info.major}.{sys.version_info.minor}')"
  OUTPUT_VARIABLE PYTHON_VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Estrai il percorso base dell'ambiente virtuale
get_filename_component(VENV_PATH "${Python3_EXECUTABLE}" DIRECTORY)
get_filename_component(VENV_PATH "${VENV_PATH}" DIRECTORY)

# Componi il percorso degli include di numpy
set(NUMPY_INCLUDE_DIR "${VENV_PATH}/lib/python${PYTHON_VERSION}/site-packages/numpy/core/include")

if (NOT EXISTS ${NUMPY_INCLUDE_DIR})
  message(FATAL_ERROR "Could not find numpy include directory at ${NUMPY_INCLUDE_DIR}. Ensure numpy is installed in the current Python environment.")
endif()

message(STATUS "Python3_EXECUTABLE: ${Python3_EXECUTABLE}")
message(STATUS "Python Version: ${PYTHON_VERSION}")
message(STATUS "Python3_INCLUDE_DIRS: ${Python3_INCLUDE_DIRS}")
message(STATUS "Python3_LIBRARIES: ${Python3_LIBRARIES}")
message(STATUS "NUMPY_INCLUDE_DIR: ${NUMPY_INCLUDE_DIR}")


include_directories(
  BEFORE
  ${NUMPY_INCLUDE_DIR}
  )


else()
  find_package(Matlab)
  # FindMatlab()
  ## find_package(Matlab COMPONENTS MAIN_PROGRAM ENG_LIBRARY MAT_LIBRARY MX_LIBRARY MEX_COMPILER REQUIRED)

  
endif()
  
find_package(OpenMP  REQUIRED)

# pkg_search_module(FFTW REQUIRED fftw3 IMPORTED_TARGET)
# include_directories(PkgConfig::FFTW)
# link_libraries     (PkgConfig::FFTW -lfftw3f   )

set(CMAKE_C_FLAGS " -Wall -O3 ${CMAKE_C_FLAGS} -g -Wuninitialized")
set(CMAKE_CXX_FLAGS " -Wall -O3  ${CMAKE_CXX_FLAGS} -g -Wuninitialized")

add_subdirectory(apply_scintillator_diffusion_correction/)
