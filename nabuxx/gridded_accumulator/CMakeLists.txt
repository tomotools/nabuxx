cmake_minimum_required(VERSION 3.16)
project(gridded_accumulator LANGUAGES CXX C)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


message(STATUS "Building for system processor ${CMAKE_SYSTEM_PROCESSOR}")
include(CheckCXXCompilerFlag)

set(CMAKE_C_FLAGS " -Wall -O3 ${CMAKE_C_FLAGS} -g -Wuninitialized")
set(CMAKE_CXX_FLAGS " -Wall -O3 ${CMAKE_CXX_FLAGS} -g -Wuninitialized")

find_package(PkgConfig REQUIRED)

if(DEFINED ENV{CXX_AUXILIARY_PACKAGES_DIR})
  set( CXX_DIR  "$ENV{CXX_AUXILIARY_PACKAGES_DIR}"   ) 
else()
  set( CXX_DIR  "/scisoft/tomotools_env"   ) 
endif()

find_package(pybind11 CONFIG)
message(STATUS "pybind11 ${pybind11_VERSION}")

if (${pybind11_VERSION} VERSION_LESS 2.9)
  message(STATUS "setting manually pybind11_INCLUDE_DIRS ${CXX_DIR}")
  if(EXISTS "${CXX_DIR}/cxx_packages/src/pybind11/include")
    set(pybind11_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/pybind11/include" "${pybind11_INCLUDE_DIRS}")
  else()
    message(FATAL_ERROR "the package pybind11 found, but it is too old, and the auxiliary sources have not been found in ${CXX_DIR}/cxx_packages/src/pybind11/include. Consider defining the CXX_AUXILIARY_PACKAGES_DIR environment variable and make it point to a directory containing cxx_packages/src/pybind11, fmt, HighFive, xtensor, xtensor-python,  xtl")
  endif()
endif()

set(Python3_FIND_VIRTUALENV FIRST)
find_package(Python3 COMPONENTS Interpreter Development REQUIRED)

# Ottieni la versione di Python
execute_process(
  COMMAND "${Python3_EXECUTABLE}" -c "import sys; print(f'{sys.version_info.major}.{sys.version_info.minor}')"
  OUTPUT_VARIABLE PYTHON_VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Estrai il percorso base dell'ambiente virtuale
get_filename_component(VENV_PATH "${Python3_EXECUTABLE}" DIRECTORY)
get_filename_component(VENV_PATH "${VENV_PATH}" DIRECTORY)

# Componi il percorso degli include di numpy
set(NUMPY_INCLUDE_DIR "${VENV_PATH}/lib/python${PYTHON_VERSION}/site-packages/numpy/core/include")

if (NOT EXISTS ${NUMPY_INCLUDE_DIR})
  message(FATAL_ERROR "Could not find numpy include directory at ${NUMPY_INCLUDE_DIR}. Ensure numpy is installed in the current Python environment.")
endif()

message(STATUS "Python3_EXECUTABLE: ${Python3_EXECUTABLE}")
message(STATUS "Python Version: ${PYTHON_VERSION}")
message(STATUS "Python3_INCLUDE_DIRS: ${Python3_INCLUDE_DIRS}")
message(STATUS "Python3_LIBRARIES: ${Python3_LIBRARIES}")
message(STATUS "NUMPY_INCLUDE_DIR: ${NUMPY_INCLUDE_DIR}")

# Project scope; consider using target_include_directories instead

include_directories(
  BEFORE
  ${NUMPY_INCLUDE_DIR}
  )

find_package(xtensor QUIET)
find_package(xtensor-python QUIET)
message(STATUS  "searching xtensor has given xtensor_FOUND=${xtensor_FOUND}" )

if(NOT ${xtensor_FOUND})
  message(STATUS  "setting manually xtensor_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor/include")
    set( xtensor_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/xtensor/include;${CXX_DIR}/cxx_packages/src/xtl/include")
  else()
    message(FATAL_ERROR "the package xtensor was not found, neither was the auxiliary directory with headers.")
  endif()
endif(NOT ${xtensor_FOUND})

message(STATUS  "searching xtensor-python has given xtensor_FOUND=${xtensor-python_FOUND}" )
if(NOT ${xtensor-python_FOUND})
  message(STATUS  "adding manually to xtensor_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor-python/include")
    set( xtensor_INCLUDE_DIRS   ${xtensor_INCLUDE_DIRS} ${CXX_DIR}/cxx_packages/src/xtensor-python/include)
  else()
    message(FATAL_ERROR "the package xtensor-python has not been found,  and the auxiliary sources have not been found in ${CXX_DIR}/cxx_packages/src/pybind11/include. Consider defining the CXX_AUXILIARY_PACKAGES_DIR environment variable and make it point to a directory containing cxx_packages/src/xtensor-python, fmt, HighFive, xtensor, xtensor-python,  xtl")

    message(FATAL_ERROR "the package xtensor-python was not found, neither was the auxiliary directory with headers.")  
  endif()
endif(NOT ${xtensor-python_FOUND})

# Find HDF5 package
find_package(HDF5 REQUIRED COMPONENTS C)

set(HIGHFIVE_USE_BOOST OFF)

find_package(HighFive QUIET)
if( ${HighFive_FOUND} )
   if(HighFive_VERSION VERSION_LESS "3.0.0")
     message(STATUS  "searching HighFive has given xtensor_FOUND=${HighFive_FOUND}  and version ${HighFive_VERSION}   but version is too old. Discarding it" )
     set( HighFive_FOUND 0  ) 
   else()
     message(STATUS  "searching HighFive has given xtensor_FOUND=${HighFive_FOUND}" )
   endif()
 endif()

if(NOT ${HighFive_FOUND})
  message(STATUS  "setting manually HighFive_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/HighFive/include")
    set( HighFive_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/HighFive/include")
  else()
    message(FATAL_ERROR "the package HighFive was not found, neither was the auxiliary directory with headers.")
  endif()
else()
   message( STATUS " VERSION HIGHFIVE ${HighFive_VERSION}")
endif(NOT ${HighFive_FOUND})


find_package(fmt QUIET)
message(STATUS  "searching fmt has given xtensor_FOUND=${fmt_FOUND}" )
if(NOT ${fmt_FOUND})
  message(STATUS  "setting manually fmt_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/fmt/include")
    set( fmt_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/fmt/include")
  else()
    message(FATAL_ERROR "the package fmt was not found, neither was the auxiliary directory with headers.")
  endif()
endif(NOT ${fmt_FOUND})



find_package(OpenMP REQUIRED)

add_subdirectory(gridded_accumulator)
add_subdirectory(etfscan)
