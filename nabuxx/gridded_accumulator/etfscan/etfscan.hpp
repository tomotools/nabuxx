#ifndef ETFSCAN_HPP
#define ETFSCAN_HPP

#include <string>
#include <vector>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include <highfive/H5File.hpp>
#include<iostream>
#include<mutex>
#include <filesystem>
#include<memory>
#include <unistd.h>

#include<sys/types.h>
#include<thread>

#include <pybind11/pybind11.h>
#include<span_info.h>

namespace py = pybind11;


class EtfScanReading;


// this is the structure used to send and receive reading commands
#define MAX_CMD_LEN 64000
struct Command {
  char filename[MAX_CMD_LEN]; // no filename will ever be that long
  char dataset_url[MAX_CMD_LEN];
  hsize_t start1;
  hsize_t start2;
  hsize_t span1;
  hsize_t span2;
};



class EtfScan {
public:
  // there is the possibility to enforce an external file fo the distortion which has priority over
  // the one that is searched in the etf directory if any.
  EtfScan(const std::string &etf_path,   int num_reading_sub_processes=1,   std::string external_distortion_file="");
  void default_setup() {
    span_info = NULL;
  }
  ~EtfScan(){
    if (span_info) delete span_info ;
    this->stop_readers();

    
  };

  void tune_span_info(  float phase_margin_pix=0, bool require_redundancy=false, float angular_tolerance_steps=5);

  void setup_path(const std::string &etf_path, std::string external_distortion_file="");
  void setup_readers( std::vector<int>  read_cmd_pipes, std::vector<int> read_res_pipes);
  void check_dark();
  void check_flats();
  void check_projections();
  void check_double_flats();
  void check_diffusion_corrections();
  void check_distortion_corrections(std::string external_distortion_file="");
  void check_weight();

  
  void check_dims(std::vector<size_t> dims, std::string dataset_name, std::string file_path);
  void reinterpret_distortion_corrections();
  void _MendBlissGaps() ; 


  
  
  void init_readers(int num_reading_sub_processes) ;
  void stop_readers() ;

  xt::xarray<float> get_frame(int i) ;
  xt::xarray<float> get_frames_stack(std::vector<int> i_list) ;


  
  // these three variables are used to spawn hdf5 reading processs,( processes which will be used to improve performances)
  // Vectors are used because there will be several reading/preprocessing threads running.
  // They are shared between sequential cretion of GriddedAccumulator Objects to diminuish memory pages traffic

  std::vector<pid_t> reading_children;
  std::vector<int> read_cmd_pipes;
  std::vector<int> read_res_pipes;
  int num_reading_sub_processes;
  std::vector<int> reader_is_adopted ;

  std::mutex reader_initialisation_mtx;  // this will be used in the associating phase, to access reader_is_adopted
  
  std::unique_ptr<EtfScanReading> createForSlice(int z_start, int z_end, int proj_start, int proj_end);

  float nominal_current = 0.2;

  xt::xtensor<size_t, 1> radios_shape;

  bool has_distortion_correction;
  std::vector<int> dc_sparse_starts; // where the section corresponding to pixels i starts ( in the flattened representation)
                                     // It must have one more element than the pixels so the the n+1 th can always be used as the end
  std::vector<int> dc_sparse_j;      // the indexes ( flattened)  in the source image
  std::vector<float> dc_sparse_coeffs;
  std::vector<int> dc_start_line; // the lowest line in the source whic contributes to ith pixel
  std::vector<int> dc_end_line;   // the end line in the source above those contributings to ith pixel
  xt::xarray<float> coords_source_x;
  xt::xarray<float> coords_source_z;
 
  bool has_dark;
  xt::xarray<float> dark;

  bool has_weight;
  xt::xarray<float> weight;

  bool has_flats;
  xt::xarray<float> flats;
  xt::xarray<float> flats_angles_unwrapped;
  xt::xarray<float> flats_currents;

  bool is_flats_by_angular_sectors = false;
 
  bool has_double_flats;
  xt::xarray<size_t> double_flats_shape;
  int nsectors_per_turn;
  int period_nturns;
  bool doubleflat_is_additive ; 

  
  bool has_diffusion_correction;
  int diffusion_bin_proj, diffusion_bin_x;
  xt::xarray<float> diffusion_currents;
  xt::xarray<size_t> diffusion_shape;

  std::string mypath;

  // tensors below are in double precision. For large scans it may matter.
  // For the flats having the angles as float is less important, small errors are more toleated
  xt::xarray<double> angles_deg;
  xt::xarray<double> angles_deg_unwrapped;
  xt::xarray<double> angles_rad;
  xt::xarray<double> angles_rad_unwrapped;
  xt::xarray<double> machine_current;
  xt::xarray<double> x_translation;
  xt::xarray<double> y_translation;
  xt::xarray<double> z_translation;

  double beam_energy;
  double distance;
  double estimated_cor;
  double x_pixel_size;
  double y_pixel_size;
  std::vector<std::string> bliss_original_files;

  py::dict extra_attributes;  // Dictionary to hold additional attributes


  nabuxx::SpanInfo *span_info ; 
  
};


class EtfScanReading {
public:
  EtfScanReading(){};
  ~EtfScanReading(){
  };
  
  void allocate_output_buffers();
  void read_chunk();
  void read_chunk_weights();
  void read_chunk_projections();
  
  void interpolate_sparse(xt::xarray<float> &target, xt::xarray<float> &source);

  void set_interpolation_for_subregion(int z_start, int z_end,
                                       std::vector<int> &original_dc_start_line,
                                       std::vector<int> &original_dc_end_line,
                                       xt::xarray<float> &original_coords_source_x,
                                       xt::xarray<float> &original_coords_source_z);

  void apply_diffusion_corrections(xt::xtensor<float,3> &raw_data, int output_proj_start, int output_proj_end, int read_start_z, int read_end_z) ;


  void setup_interp_and_data_for_double_flats(
							      xt::xtensor<float, 3> & local_double_flats,
							      xt::xtensor<int,1> &ldf_last_reads,
							      int i_proj_global, 
							      xt::xtensor<float, 1> ff,
							      int read_start_z, int read_end_z 
							      );
  
  template <typename E1, typename E2>
  void interpolate_sparse(E1 &target, E2 &source)
  {
    // this version has bonds checking ( E1, and E2 are meant to match xtensors and views)
    assert(dc_sparse_starts.size() > 1);

    target.fill(0.0f);
    auto flat_target =  xt::flatten(target);
    auto flat_source =  xt::flatten(source);

    int *sparse_starts = &dc_sparse_starts[0];
    float *sparse_coeffs = &dc_sparse_coeffs[0];
    int *sparse_j = &dc_sparse_j[0];

    for (size_t i = 0; i < dc_sparse_starts.size() - 1; ++i)
      {
	int start = sparse_starts[i];
	int end = sparse_starts[i + 1];

	for (int k = start; k < end; ++k)
	  {
	    int j = sparse_j[k];
	    float coeff = sparse_coeffs[k];

	    flat_target(i) += flat_source(j) * coeff;
	  }
      }
  }
  
  void interpolate_sparse_raw(float *flat_target, float *flat_source)
  {
    // this version does no bonds checking, it is faster but probably in a not relevant way
    // in respect of all the rest
    int *sparse_starts = &dc_sparse_starts[0];
    float *sparse_coeffs = &dc_sparse_coeffs[0];
    int *sparse_j = &dc_sparse_j[0];

    for (size_t i = 0; i < dc_sparse_starts.size() - 1; ++i)
      {
	flat_target[i] = 0 ; 

	int start = sparse_starts[i];
	int end = sparse_starts[i + 1];

	for (int k = start; k < end; ++k)
	  {
	    int j = sparse_j[k];
	    float coeff = sparse_coeffs[k];
	    flat_target[i] += flat_source[j] * coeff;
	  }
      }
  }

  // this commented line can be used for debugging
  void save_readed_signal_to_hdf5(int n);

  
  xt::xarray<float> readed_signal;
  xt::xarray<float> readed_weight;
  xt::xarray<float> readed_weight_Ifactor;

  int used_reader_idx ;
  int cmd_channel;
  int res_channel;


  
  int output_start_z;
  int output_end_z;
  int output_proj_start;
  int output_proj_end;

  EtfScan *parent = NULL;

  int read_start_z;
  int read_end_z;
  /// @brief
  xt::xtensor<size_t, 1> reading_shape;

  xt::xtensor<size_t, 1> radios_shape;
  std::vector<int> dc_sparse_starts; // where the section corresponding to pixels i starts ( in the flattened representation)
                                     // It must have one more element than the pixels so the the n+1 th can always be used as the end
  std::vector<int> dc_sparse_j;      // the indexes ( flattened)  in the source image
  std::vector<float> dc_sparse_coeffs;

  xt::xarray<float> dark;

  xt::xarray<float> weight;

  xt::xarray<float> flats;

};

void interpolate(float x, float y, std::vector<int>& indices, std::vector<float>& coeffs, int m, int n) ;


void apply_interpolated_scintillator_diffusion_correction(
							  xt::xtensor<float, 3> &raw_data,
							  xt::xtensor<float,1> &currents_segment,
							  float nominal_current,
							  xt::xtensor<float, 3> &diffusion_correction_data,
							  int diffusion_bin_proj,
							  int diffusion_bin_x,
							  int output_proj_start,
							  int read_start_z,
							  int corr_read_proj_start,
							  int corr_read_start_z
							  ) ;



// Helper function to format the shape
template <typename ShapeType>
std::string shape_to_string(const ShapeType& shape) {
  std::ostringstream oss;
  oss << "(";
  for (size_t i = 0; i < shape.size(); ++i) {
    oss << shape[i];
    if (i < shape.size() - 1) {
      oss << ", ";
    }
  }
  oss << ")";
  return oss.str();
}

// Generic read function for xt::xarray and xt::xtensor
template <typename T>
void generic_read_float_data(
			     const std::string& base_name,
			     const std::string& data_where,
			     const std::string& parent_path,
			     int start1,
			     int start2,
			     int span1,
			     int span2,
			     T& data,
			     int cmd_channel, int res_channel, int line, const char* file) {
    // Forming the command
    Command cmd;
    std::filesystem::path file_path = std::filesystem::path(parent_path) /  base_name;
    strncpy(cmd.filename, file_path.c_str(), MAX_CMD_LEN - 1);
    strncpy(cmd.dataset_url, data_where.c_str(), MAX_CMD_LEN - 1);
    cmd.start1 = start1;
    cmd.start2 = start2;
    cmd.span1 = span1;
    cmd.span2 = span2;

    size_t count; // for byte count check

    // Sending the command through the command channel
    count = write(cmd_channel, &cmd, sizeof(cmd));
    if (count != sizeof(cmd)) {
        std::stringstream ss;
        ss << "Could not write the command to the command channel. FILE: " << file << " LINE: " << line;
        throw std::runtime_error(ss.str());
    }

    // Retrieve the result of the reading through the result channel
    ssize_t totalRead = 0;
    ssize_t dataSize = data.size() * sizeof(typename T::value_type);
    
    while (totalRead < dataSize) {
      ssize_t bytesRead = read(res_channel, ((char*) data.data()) + totalRead, dataSize - totalRead);


      
      if (bytesRead == -1) {
        // Gestire errore (ad esempio EINTR, EAGAIN, ecc.)
        std::stringstream ss;
        ss << "Error during read operation. FILE: " << file << " LINE: " << line
           << ". Error: " << strerror(errno);  // Messaggio di errore basato su errno
        throw std::runtime_error(ss.str());
      }
      
      if (bytesRead == 0) {
        // Il canale è stato chiuso prematuramente (end-of-file)
        std::stringstream ss;
        ss << "End of file reached before reading all data. FILE: " << file << " LINE: " << line;
        throw std::runtime_error(ss.str());
      }
      
      totalRead += bytesRead;
    }

    if (totalRead != dataSize) {
      std::stringstream ss;
      ss << "Could not read the data in full from the result channel. FILE: " << file << " LINE: " << line;
      throw std::runtime_error(ss.str());
    }
}



// Generic read function
template <typename T>
void read_data(const std::string& base_name, const std::string& data_where, const std::filesystem::path& parent_path,
               size_t start1, size_t start2, size_t span1, size_t span2, xt::xarray<T>& data,
               int cmd_channel, int res_channel) {
    // Forming the command
    Command cmd;
    std::filesystem::path file_path = parent_path / base_name;
    strncpy(cmd.filename, file_path.c_str(), MAX_CMD_LEN - 1);
    strncpy(cmd.dataset_url, data_where.c_str(), MAX_CMD_LEN - 1);
    cmd.start1 = start1;
    cmd.start2 = start2;
    cmd.span1 = span1;
    cmd.span2 = span2;

    size_t count; // for byte count check

    // Sending the command through the command channel
    count = write(cmd_channel, &cmd, sizeof(cmd));
    if (count != sizeof(cmd)) {
        std::stringstream ss;
        ss << "Could not write the command to the command channel. FILE: " << __FILE__ << " LINE: " << __LINE__;
        throw std::runtime_error(ss.str());
    }

    // Retrieve the result of the reading through the result channel
    count = read(res_channel, data.data(), data.size() * sizeof(T));
    if (count != data.size() * sizeof(T)) {
        std::stringstream ss;
        ss << "Could not read the data in full from the result channel. FILE: " << __FILE__ << " LINE: " << __LINE__;
        throw std::runtime_error(ss.str());
    }
}

inline float modfi(float value, int *intpart)
{
  float temp, result;
  result = std::modf(value, &temp);
  if ( intpart) {
    *intpart = static_cast<int>( temp );
  }
  return result;
}

// Function to read a 3D slice from an HDF5 dataset using HighFive
void read_hdf5_slice(const char* filename, const char* dataset_url, hsize_t start1, hsize_t start2, hsize_t span1, hsize_t span2, std::vector<float>& buffer) ;

// this function is the main loop of the reader
void ReadingChildProcess(int id, int cmd_read_fd, int res_write_fd) ;


#endif 
