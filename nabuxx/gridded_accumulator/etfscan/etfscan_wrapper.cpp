#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include "etfscan.hpp"

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

namespace py = pybind11;

using namespace py::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword



template<typename T>
auto make_getter(xt::xarray<T>& member) {
    return [&member](EtfScan& reader) {
        xt::xarray<T>& data = member;
        py::array_t<T> result(data.shape());
        auto result_buffer = result.request();
        T* result_ptr = static_cast<T*>(result_buffer.ptr);
        std::copy(data.data(), data.data() + data.size(), result_ptr);
        return result;
    };
}


PYBIND11_MODULE(etfscan, m) {
  xt::import_numpy();
    py::class_<EtfScan>(m, "EtfScan")
      .def(py::init<const std::string &, int, std::string>(), py::arg("etf_path"), py::arg("num_reading_sub_processes") = 1, py::arg("external_distortion_file") = "")      
      .def_readonly("dark", &EtfScan::dark)
      .def_readonly("has_dark", &EtfScan::has_dark)
      .def_readonly("has_flats", &EtfScan::has_flats)
      
      .def("get_dark", [](EtfScan& reader) {

            xt::xarray<float>& data = reader.dark;
            py::array_t<float> result(data.shape());
            auto result_buffer = result.request();
            float* result_ptr = static_cast<float*>(result_buffer.ptr);
            std::copy(data.data(), data.data() + data.size(), result_ptr);
            return result;
        })
      .def_property_readonly("radio_dims", [](EtfScan& reader) {
	  return std::make_tuple(reader.radios_shape[2], reader.radios_shape[1]);
	})
      .def_property_readonly("radio_shape", [](EtfScan& reader) {
	  return std::make_tuple(reader.radios_shape[1], reader.radios_shape[2]);
	})
      .def_property_readonly("pixel_size_meters", [](EtfScan& reader) {
	  return reader.x_pixel_size;
	})
      .def_property_readonly("distance_meters", [](EtfScan& reader) {
	  return reader.distance;
	})
      
      .def_property_readonly("beam_energy_kev", [](EtfScan& reader) {
	  return reader.beam_energy;
	})

      .def_property_readonly("z_translation", [](EtfScan& reader) { return make_getter(reader.z_translation)(reader); })
      .def_property_readonly("x_translation", [](EtfScan& reader) { return make_getter(reader.x_translation)(reader); })
      .def_property_readonly("angles_deg", [](EtfScan& reader) { return make_getter(reader.angles_deg)(reader); })
      .def_property_readonly("angles_deg_unwrapped", [](EtfScan& reader) { return make_getter(reader.angles_deg_unwrapped)(reader); })
      .def_property_readonly("angles_rad", [](EtfScan& reader) { return make_getter(reader.angles_rad)(reader); })
      .def_property_readonly("angles_rad_unwrapped", [](EtfScan& reader) { return make_getter(reader.angles_rad_unwrapped )(reader); })


      .def("get_frame", [](EtfScan& self, int i) {
			  xt::xarray<float> frame = self.get_frame(i);
			  return xt::pyarray<float>(frame);  
			}, pybind11::arg("i"))
      
      .def("get_frames_stack", [](EtfScan& self, const std::vector<int>& i_list) {
				 // Wrapping: convertiamo xt::xtensor<float, 3> in xt::pyarray<float>
				 xt::xarray<float> stack = self.get_frames_stack(i_list);
				 return xt::pyarray<float>(stack);  
			       }, pybind11::arg("i_list"))
    
      .def("tune_span_info",  [](  EtfScan& reader   ,    float phase_margin_pix, bool require_redundancy, float angular_tolerance_steps  ) {
				reader.tune_span_info(   phase_margin_pix,  require_redundancy,  angular_tolerance_steps    );
			      }, py::arg("phase_margin_pix"), py::arg("require_redundancy"), py::arg("angular_tolerance_steps"))
      .def("get_dataset_info",  [](EtfScan& reader) {

	  py::list py_string_list;
	  for (const auto& str : reader.bliss_original_files) {
	    py_string_list.append(str);
	  }

	  
	  py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");	  
	  py::object ns = SimpleNamespace(
					  "beam_energy"_a=  py::float_(reader.beam_energy),
					  "distance"_a= py::float_(reader.distance),
					  "estimated_cor"_a= py::float_(reader.estimated_cor),
					  "x_pixel_size"_a= py::float_(reader.x_pixel_size),
					  "y_pixel_size"_a= py::float_(reader.y_pixel_size),
					  "original_bliss_files"_a = py_string_list,

					  "radios_shape"_a = xt::pyarray<int>(reader.radios_shape ),

					  "angles_deg"_a =  xt::pyarray<double>( reader.angles_deg ), 
					  "angles_deg_unwrapped"_a =  xt::pyarray<double>( reader.angles_deg_unwrapped  ), 
					  "angles_rad"_a =  xt::pyarray<double>( reader.angles_rad  ), 
					  "angles_rad_unwrapped"_a =  xt::pyarray<double>( reader.angles_rad_unwrapped  ), 
					  "machine_current"_a =  xt::pyarray<double>( reader.machine_current   ), 
					  "x_translation"_a =  xt::pyarray<double>( reader.x_translation	), 
					  "y_translation"_a =  xt::pyarray<double>( reader.y_translation   ), 
					  "z_translation"_a =  xt::pyarray<double>( reader.z_translation   )
					  );

	  return ns;
	} )

      .def("__getattr__", [](EtfScan &self, const std::string &name) -> py::object {
			    if (self.extra_attributes.contains(name.c_str())) {
			      return self.extra_attributes[name.c_str()];
			    }
			    throw py::attribute_error("Attribute " + name + " not found");
			  })
      .def("__setattr__", [](EtfScan &self, const std::string &name, py::object value) {
			    if (py::hasattr(py::cast(self), name.c_str())) {
			      py::setattr(py::cast(self), name.c_str(), value);
			    } else {
			      self.extra_attributes[name.c_str()] = value;
			    }
			  })


    .def_property_readonly("pix_size_mm", [](EtfScan &this_obj){
	return this_obj.span_info->pix_size_mm; 
      })
    .def_property_readonly("z_offset_mm", [](EtfScan &this_obj){
	return this_obj.span_info->z_offset_mm; 
      })
    .def_property_readonly("sunshine_ends", [](EtfScan &this_obj){
	return xt::pyarray<int>(this_obj.span_info->sunshine_ends); 
      })
    .def_property_readonly("z_pix_per_proj", [](EtfScan &this_obj){
	return xt::pyarray<int>(this_obj.span_info->z_pix_per_proj); 
      })
    .def_property_readonly("sunshine_image", [](EtfScan &this_obj){
	return xt::pyarray<float>(this_obj.span_info->sunshine_image); 
      })
    .def_property_readonly("sunshine_starts", [](EtfScan &this_obj){
	return xt::pyarray<int>(this_obj.span_info->sunshine_starts); 
      })
    .def_property_readonly("projection_angles_deg", [](EtfScan &this_obj){
       return xt::pyarray<double>(this_obj.span_info->projection_angles_deg); 
      })
    .def_property_readonly("projection_angles_deg_internally", [](EtfScan &this_obj){
       return xt::pyarray<double>(this_obj.span_info->internally_projection_angles_deg); 
      })
    .def("get_doable_span", [](EtfScan &this_obj){
       int view_height_min, view_height_max;			      
       float z_over_stage_pix_min=0, z_over_stage_pix_max=0 ; 
       float z_over_stage_mm_min=0 , z_over_stage_mm_max=0 ; 
       this_obj.span_info->get_doable_span(  view_height_min, view_height_max, z_over_stage_pix_min, z_over_stage_pix_max, z_over_stage_mm_min, z_over_stage_mm_max);

       py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");
       py::object ns = SimpleNamespace( "view_heights_minmax"_a = xt::pyarray<int>({view_height_min, view_height_max}),
					"z_pix_minmax"_a = xt::pyarray<float>({z_over_stage_pix_min, z_over_stage_pix_max}),
					"z_mm_minmax"_a = xt::pyarray<float>({z_over_stage_mm_min, z_over_stage_mm_max})
					);
       return ns;
       
      })
    .def("get_informative_string", [](EtfScan &this_obj){
	return this_obj.span_info->get_informative_string(); 
      })
    .def("get_chunk_info",  [](EtfScan &this_obj, py::array_t<int> span_v) {
	int span_v_absolute_start,  span_v_absolute_end;
	py::buffer_info buffer_info  = span_v.request();
	if( buffer_info.ndim!=1  || buffer_info.shape[0]!=2) {
	  throw std::invalid_argument("the span_v argument has not the right shape. It must be a 1D array with two int entries");
	}
	span_v_absolute_start = *((int*)buffer_info.ptr) ;
	span_v_absolute_end   = *((int*) ((char*)buffer_info.ptr + buffer_info.strides[0] ) ) ;
	// 
	// printf(" LIMITI %d %d %ld\n",  span_v_absolute_start,  span_v_absolute_end,  buffer_info.strides[0] ) ;

	  {
	    py::gil_scoped_release release;
	    this_obj.span_info->set_chunk_info( span_v_absolute_start, span_v_absolute_end);
	  }


	  py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");
	  py::object ns = SimpleNamespace(
					  "angle_index_span"_a= xt::pyarray<int>(this_obj.span_info->chunk_info.angle_index_span),					
					  "span_v"_a= xt::pyarray<int>(this_obj.span_info->chunk_info.absolute_span_v),
					  "integer_shift_v"_a= xt::pyarray<int>(this_obj.span_info->chunk_info.integer_shift_v),
					  "fract_complement_to_integer_shift_v"_a= xt::pyarray<float>(this_obj.span_info->chunk_info.fract_complement_to_integer_shift_v),
					  "z_pix_per_proj"_a= xt::pyarray<float>(this_obj.span_info->chunk_info.z_pix_per_proj),
					  "x_pix_per_proj"_a= xt::pyarray<float>(this_obj.span_info->chunk_info.x_pix_per_proj),
					  "angles_rad"_a= xt::pyarray<float>(this_obj.span_info->chunk_info.chunk_angles_rad),
					  "internally_projection_angles_deg"_a= xt::pyarray<double>(this_obj.span_info->internally_projection_angles_deg)
					  );
	  printf(" LIMITI  OK %d %d %ld\n",  span_v_absolute_start,  span_v_absolute_end,  buffer_info.strides[0] ) ;
	  
	  return ns;
			    }, py::arg("span_v"));
	
}
