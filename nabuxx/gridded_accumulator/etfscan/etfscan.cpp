// this declaration can be removed to get a faster compiled code
// out of debug phases
// but without a whole range of automatic checks performed by xtensor.
//   #define XTENSOR_ENABLE_ASSERT 

#include "etfscan.hpp"
#include <iostream>
#include <filesystem>
#include <vector>
#include "highfive/H5File.hpp"
#include "highfive/H5DataSet.hpp"
#include <exception>
#include <xtensor/xadapt.hpp>
#include <xtensor/xview.hpp>
#include <cmath>
#include <memory>
#include <algorithm>
#include <xtensor/xmath.hpp>
#include <thread>
#include <sstream>
#include<iomanip>
#include <chrono>  
#include <unistd.h>
#include <sys/wait.h>

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>

namespace fs = std::filesystem;
namespace h5 = HighFive;

 
EtfScan::EtfScan(const std::string &etf_path, int num_reading_sub_processes, std::string external_distortion_file)
{
  this->default_setup();
  // external_distortion_file, if given, will have priority over  the one that could be present in the etf directory
  this->setup_path(etf_path, external_distortion_file);

  this->init_readers(num_reading_sub_processes) ;
  
  this->tune_span_info();

}

void EtfScan::tune_span_info(  float phase_margin_pix, bool require_redundancy, float angular_tolerance_steps) {
  if( span_info) {
    delete span_info;
    span_info = NULL ; 
  }
  
  auto  z_pix_per_proj  =   ( z_translation   - z_translation(0) ) ; 
  auto  x_pix_per_proj  =   ( x_translation   - x_translation(0)  ) ;
  
  std::vector<int>    detector_shape_vh({(int) radios_shape[1], (int) radios_shape[2]  });

  float pixel_size_meters  = x_pixel_size; 
  float pix_size_mm =  pixel_size_meters * 1.0e+3   ; 
  float z_offset_mm = z_translation(0) * pix_size_mm ; 
  
  span_info = new nabuxx::SpanInfo(z_offset_mm,
				   z_pix_per_proj,
				   x_pix_per_proj,
				   detector_shape_vh,
				   phase_margin_pix,
				   angles_deg_unwrapped,
				   pix_size_mm,
				   require_redundancy,
				   angular_tolerance_steps
				   );  
}

void EtfScan::setup_path(const std::string &etf_path,  std::string external_distortion_file)
{
  // external_distortion_file, if non null , will have priority over  the one that could be present in the etf directory
  this->mypath = etf_path;

  this->check_projections();
  this->check_distortion_corrections(external_distortion_file);
  this->reinterpret_distortion_corrections();
  this->check_dark();
  this->check_weight();
  this->check_flats();
  this->check_double_flats();
  this->check_diffusion_corrections();

  std::stringstream ss;
  ss << " has_dark " << has_dark
     << " ; has_flats " << has_flats
     << " has_double_flats " << has_double_flats
     << " diffusion_bin_x " << diffusion_bin_x
     << " has_distortion_corrections " << has_distortion_correction;
  std::cout << " SETTED UP ETFSCAN: " << ss.str() << std::endl;
}

void EtfScan::setup_readers(std::vector<int> read_cmd_pipes, std::vector<int> read_res_pipes)
{
  this->read_cmd_pipes = read_cmd_pipes;
  this->read_res_pipes = read_res_pipes;
  this->reader_is_adopted.clear();
  this->reader_is_adopted = std::vector<int>(read_cmd_pipes.size(), 0);
}

void EtfScan::check_dims(std::vector<size_t> dims, std::string dataset_name, std::string file_path)
{
  if (xt::adapt(dims) != xt::view(radios_shape, xt::range(1, xt::xnone)))
  {
    std::stringstream ss;
    ss << " dataset " << dataset_name << " in file " << file_path
       << " has dimensions " << dims[0] << "," << dims[1]
       << " which differs from radio_shape " << radios_shape(0) << "," << radios_shape(1);
    throw std::runtime_error(ss.str());
  }
}

void EtfScan::check_dark()
{
  std::string base_name = "dark.h5";
  fs::path file_path = fs::path(mypath) / base_name;

  has_dark = false;

  if (fs::exists(file_path))
  {
    h5::File file(file_path.string(), h5::File::ReadOnly);
    if (file.exist("data"))
    {
      h5::DataSet dataset = file.getDataSet("data");
      std::vector<size_t> dims = dataset.getSpace().getDimensions();
      this->check_dims(dims, "data", file_path);
      dark.resize(dims);
      dataset.read_raw<float>(((float *)dark.data()));
      has_dark = true;
    }
  }
}

void EtfScan::check_weight()
{
  std::string base_name = "weight_map.h5";
  fs::path file_path = fs::path(mypath) / base_name;

  has_weight = false;

  if (fs::exists(file_path))
  {
    h5::File file(file_path.string(), h5::File::ReadOnly);
    if (file.exist("data"))
    {
      h5::DataSet dataset = file.getDataSet("data");
      std::vector<size_t> dims = dataset.getSpace().getDimensions();
      this->check_dims(dims, "data", file_path);
      weight.resize(dims);
      dataset.read_raw<float>(((float *)weight.data()));
      has_weight = true;
    }
  }
}

void EtfScan::check_double_flats()
{
  std::string base_name = "double_flats.h5";
  fs::path file_path = fs::path(mypath) / base_name;

  has_double_flats = false;

  try
  {
    if (fs::exists(file_path))
    {
      h5::File file(file_path.string(), h5::File::ReadOnly);

      std::vector<std::pair<std::string, int &>> datasets = {
          {"nsectors_per_turn", nsectors_per_turn},
          {"period_nturns", period_nturns}};
      for (auto &ds : datasets)
      {
        const std::string &dataset_name = ds.first;
        int &scalar = ds.second;
        h5::DataSet dataset = file.getDataSet(dataset_name);
        dataset.read(scalar);
      }

      {
        h5::DataSet dataset = file.getDataSet("double_flats");
        double_flats_shape = xt::adapt(dataset.getDimensions());
        // there are two way to consider double flats, by thinking to teir application.
        // On is muiltiplicative and the double flat will divide the flatfileded signal.
        // The other is additive and the double flat will subtract the flatfileded signal.
        // To check in which situation we are we look at the double flats values.
        // If they are rather close to 1 it means that we are in the presence of a multiplicative double flat
        // Otherwise it is an additive double flat.
        // Below we calculate an average for the first double flat and compare it to 0.5, that is the way
        //
        doubleflat_is_additive = false;
        {
          if (double_flats_shape.size() != 3)
          {
            throw std::runtime_error("Dataset for double flats does not have exactly 3 dimensions");
          }
          std::vector<hsize_t> slice_dims = {double_flats_shape[1], double_flats_shape[2]};
          xt::xtensor<float, 2> first_slice({slice_dims[0], slice_dims[1]}) ;
          if (slice_dims[0] == 0)
          {
            throw std::runtime_error("Dataset for double flats mast have at least on frame");
          }
          if (slice_dims[0] < static_cast<hsize_t>(nsectors_per_turn) )
          {
            throw std::runtime_error("Dataset for double flats mast have at least nsectors_per_turn double flats or more");
          }          
          if (slice_dims[0] % nsectors_per_turn)
          {
            throw std::runtime_error("Dataset for double flats mast have an exact  multiple of nsectors_per_turn double double flats");
          }          

          dataset.select({0, 0, 0}, {1, slice_dims[0], slice_dims[1]}).read_raw<float>(first_slice.data() );

          double sum = std::accumulate(first_slice.begin(), first_slice.end(), 0.0, [](double acc, float val)
                                       { return acc + val; }) /
                       (first_slice.size());
          doubleflat_is_additive = (sum < 0.5);
        }
      }
      has_double_flats = true;
    }
  }
  catch (const h5::Exception &e)
  {
    std::cerr << "Error in method check_double_flats :  " << e.what() << " reading file " << file_path.string() << " . Message from source file: " << __FILE__ << " line : " << __LINE__ << std::endl;
    throw;
  }
}

void EtfScan::check_flats()
{
  std::string base_name = "flats.h5";
  fs::path file_path = fs::path(mypath) / base_name;

  has_flats = false;

  try
  {
    if (fs::exists(file_path))
    {
      h5::File file(file_path.string(), h5::File::ReadOnly);

      std::vector<std::pair<std::string, xt::xarray<float> &>> datasets = {
          {"data", flats},
          {"framewise/flats_angles_unwrapped", flats_angles_unwrapped},
          {"framewise/flats_currents", flats_currents}};

      size_t previous_n = 0;
      std::string previous_name = "";

      for (auto &ds : datasets)
      {
        const std::string &dataset_name = ds.first;
        xt::xarray<float> &tensor = ds.second;
        if (file.exist(dataset_name))
        {
          h5::DataSet dataset = file.getDataSet(dataset_name);
          auto dims = dataset.getDimensions();

          if (previous_name.size() && previous_n != dims[0])
          {
            std::stringstream ss;
            ss << " The dataset " << dataset_name << " in file " << file_path.string()
               << " has length " << dims[0] << " but the previous " << previous_name
               << " has a different length of " << previous_n;
            throw std::runtime_error(ss.str());
          }
          previous_name = dataset_name;
          previous_n = dims[0];

          if (dataset_name == "data")
          {
            this->check_dims({dims[1], dims[2]}, dataset_name, file_path);
          }

          tensor.resize(dims);
          dataset.read_raw<float>(tensor.data());
        }
        else
        {
          std::stringstream ss;
          ss << " Could not find dataset " << dataset_name << " in file " << file_path.string();
          throw std::runtime_error(ss.str());
        }
      }
      { // check if flats come from by sector averaging
        is_flats_by_angular_sectors = false;
        if (flats_angles_unwrapped.shape()[0] >= 4)
        {
          // search for a gap in the sequence
          int nf = flats_angles_unwrapped.shape()[0];
          for (int i = 1; i < nf - 1; i++)
          {
            auto &ang = flats_angles_unwrapped;
            if (fabs(ang(i + 1) - ang(i)) >= fabs(1.8 * (ang(i) - ang(0)) / (i)))
            {
              is_flats_by_angular_sectors = true;
              break;
            }
          }
        }
      }
      if (has_dark)
      {
        flats -= dark;
      }
      { // flats to nominal current
        for (int i = 0; i < (int)flats.shape()[0]; i++)
        {
          flats[i] /= flats_currents[i] / nominal_current;
        }
      }
      has_flats = true;
    }
  }
  catch (const h5::Exception &e)
  {
    std::cerr << "Error in method check_flats :  " << e.what() << " reading file " << file_path.string() << " . Message from source file: " << __FILE__ << " line : " << __LINE__ << std::endl;
    throw;
  }
}

xt::xarray<float> EtfScan::get_frame(int i) {
    std::unique_ptr<EtfScanReading> etfscan_reader_ptr = this->createForSlice( 0 , radios_shape[1] ,  i,  i+1 );
    etfscan_reader_ptr->read_chunk();
    
    return xt::view(etfscan_reader_ptr->readed_signal,0,xt::all(),xt::all())     ;
}


xt::xarray<float> EtfScan::get_frames_stack(std::vector<int> i_list) {

  
  xt::xarray<float> result = xt::xarray<float>::from_shape({i_list.size(), radios_shape[1], radios_shape[2]});

#pragma omp parallel for num_threads(num_reading_sub_processes)
  for( int i=0 ; i <(int) i_list.size() ; i++ ) {
    std::unique_ptr<EtfScanReading> etfscan_reader_ptr = this->createForSlice( 0 , radios_shape[1] ,  i_list[i],  i_list[i]+1 );
    etfscan_reader_ptr->read_chunk();
    xt::view( result , i, xt::all(), xt::all()   ) =  xt::view(etfscan_reader_ptr->readed_signal, 0, xt::all(), xt::all());
  }
  return result;
}




void EtfScanReading::allocate_output_buffers()
{
  readed_signal.resize({(size_t)(output_proj_end - output_proj_start), (size_t)(output_end_z - output_start_z), radios_shape[2]});
  readed_weight.resize({(size_t)(output_end_z - output_start_z), radios_shape[2]});
  readed_weight_Ifactor.resize({(size_t)(output_proj_end - output_proj_start)});
}

void EtfScanReading::read_chunk()
{
  // For the moment the preprocessing, beside the distortion correction, is done outside.
  this->read_chunk_weights();
  this->read_chunk_projections();


  {
     std::lock_guard<std::mutex> lock(parent->reader_initialisation_mtx);

     parent->reader_is_adopted[used_reader_idx] = 0 ;
   }

}

void write_raw_data_to_file(const xt::xtensor<float, 3> &raw_data, const std::string &file_path)
{
  FILE *file = fopen(file_path.c_str(), "wb");
  if (!file)
  {
    std::stringstream ss;
    ss << " Error opening file " << file_path << " at file " << __FILE__ << " line " << __LINE__;
    throw std::runtime_error(ss.str());
  }
  size_t elements_written = fwrite(raw_data.data(), sizeof(float), raw_data.size(), file);
  if (elements_written != raw_data.size())
  {
    std::stringstream ss;
    ss << " Error: written " << elements_written << " elements instead of the " << raw_data.size()
       << " size of raw_data file " << file_path << " at file " << __FILE__ << " line " << __LINE__;
    throw std::runtime_error(ss.str());
  }

  fclose(file);
}

void read_raw_data_from_file(xt::xtensor<float, 3> &raw_data, const std::string &file_path)
{
  FILE *file = fopen(file_path.c_str(), "rb");
  if (!file)
  {
    std::stringstream ss;
    ss << " Error opening file " << file_path << " at file " << __FILE__ << " line " << __LINE__;
    throw std::runtime_error(ss.str());
  }
  fseek(file, 0, SEEK_END);
  size_t file_size = ftell(file);
  fseek(file, 0, SEEK_SET);

  size_t num_elements = file_size / sizeof(float);

  if (num_elements != raw_data.size())
  {
    std::stringstream ss;
    ss << "Found " << num_elements << " elements in file " << file_path
       << " it does not match the " << raw_data.size() << " elements of raw_data. File "
       << __FILE__ << " line " << __LINE__;
    throw std::runtime_error(ss.str());
  }
  size_t elements_read = fread(raw_data.data(), sizeof(float), num_elements, file);
  if (elements_read != num_elements)
  {
    std::stringstream ss;
    ss << "Expected to read " << num_elements << " elements but found only " << elements_read
       << " at file " << __FILE__ << " line " << __LINE__;
    throw std::runtime_error(ss.str());
  }
  fclose(file);
}

static void find_bounds_and_fraction_for_flat_interpolation(const xt::xtensor<float, 1> &a, float angle_a, float &fa, int &iflat_a0, int &iflat_a1)
{
  // Trova l'indice iflat_a1
  auto ita = std::lower_bound(a.begin(), a.end(), angle_a);
  iflat_a1 = std::distance(a.begin(), ita);
  iflat_a0 = 0;
  fa = 0.0f;

  if (iflat_a1 == 0)
  {
    iflat_a0 = iflat_a1;
  }
  else if (iflat_a1 == static_cast<int>(a.size()))
  {
    iflat_a1 -= 1;
    iflat_a0 = iflat_a1;
  }
  else
  {
    iflat_a0 = iflat_a1 - 1;
    fa = (angle_a - a[iflat_a0]) / (a[iflat_a1] - a[iflat_a0]);
  }
}

void EtfScanReading::read_chunk_projections()
{
  size_t nx = radios_shape[2];

  // the number of projections in the output is the same as the projections that one has to read.
  // This is the number of projections that we need to read.
  // This is at variance with the vertical span which is affected by the distortion corrections.
  // I this latter case the number of lines to read is different from the number of lines in the output
  hsize_t num_projections_to_read = output_proj_end - output_proj_start;

  // Resize the readed_signal tensor to hold the signal
  readed_signal.resize({num_projections_to_read, (size_t)(output_end_z - output_start_z), nx});

  // Create and resize a tensor for currents
  xt::xtensor<float, 1> currents;
  currents.resize({num_projections_to_read});

  hsize_t num_rows_to_read = read_end_z - read_start_z;

  // Create a tensor to hold raw data
  xt::xtensor<float, 3> raw_data({num_projections_to_read, num_rows_to_read, nx});

  
  // for double flats interpolation, the concerned subregion of concerned double flats
  xt::xtensor<float, 3> local_double_flats;

  // For double flats, if they are active
  // the reading will be done only if the previous read ones are not equal to the actual one
  // We need a maximum of four double flats because when there are more than one angular sectore per turn
  // then we need to do a bilinear interpolation: on interpolation over angles and one over time period/
  // Whe we have read we keep track of in this array.
  // ldf_last_reads as "loaded double flat last read"
  xt::xtensor<int, 1> ldf_last_reads = {-1, -1, -1, -1}; 
  if (parent->has_double_flats)
  {
    local_double_flats.resize({4, num_rows_to_read,  nx}); // 4 at maximum for bilinear interpolation. These frame subregions will be read on demand
  };

  // Scope for raw projections reading
  {
    
    generic_read_float_data( 
      "projections.h5",
      "data",
      parent->mypath,
      output_proj_start,
      read_start_z,
      num_projections_to_read,
      num_rows_to_read,
      raw_data,
      cmd_channel, res_channel, __LINE__, __FILE__
    ) ;


    if (parent->has_diffusion_correction)
    {
      apply_diffusion_corrections(raw_data, output_proj_start, output_proj_end, read_start_z, read_end_z);
    }

    if (parent->has_dark)
    {
      // Apply dark correction to raw_data if needed
      raw_data -= dark;
    }

    // Process each projection
    for (int iradio = 0; iradio < (int)num_projections_to_read; iradio++)
    {
      int global_iradio = iradio + output_proj_start;

      float radio_angle = parent->angles_deg_unwrapped(global_iradio);
      float radio_current = parent->machine_current(global_iradio);

      // Adjust raw_data based on the machine current
      raw_data[iradio] /= (radio_current / parent->nominal_current);

      if (parent->has_flats)
      {
        if (!parent->is_flats_by_angular_sectors)
        {
          // Simple interpolation

          xt::xarray<float> flats_angles_abs = xt::abs(parent->flats_angles_unwrapped);
          float radio_angle_abs = fabs(radio_angle);
          int iflat1, iflat0;
          float f;
          find_bounds_and_fraction_for_flat_interpolation(flats_angles_abs, radio_angle_abs, f, iflat0, iflat1);
          // as an idea to reduce the memory burden, to be implemented in the future :
          //  flats number iflat0 and iflat1 could be read on demand, if needed, and then discarded
          // instead of, like now, having all the stack loaded in memory
          for (size_t i = 0; i < raw_data.shape(1); ++i)
          {
            for (size_t j = 0; j < raw_data.shape(2); ++j)
            {
              float interpolated_flat = (1 - f) * flats(iflat0, i, j) + f * flats(iflat1, i, j);
              raw_data(iradio, i, j) /= interpolated_flat;
            }
          }
        }
        else
        {
          // Correction by angles and time along the scan
          xt::xarray<float> flats_angles_abs = xt::abs(parent->flats_angles_unwrapped);
          float radio_angle_abs = fabs(radio_angle);

          float angle_a = radio_angle_abs;
          while (angle_a > 360)
          {
            // search for an angle_a in the first turn
            angle_a -= 360;
          }

          float angle_b = radio_angle_abs;
          while (angle_b < flats_angles_abs[flats_angles_abs.shape()[0] - 1] - 360)
          {
            // search for ang angle_b in the last turn
            angle_b += 360;
          }

          float FF = (radio_angle_abs - angle_a) / (angle_b - angle_a);

          int iflat_a1, iflat_a0;
          float fa;
          find_bounds_and_fraction_for_flat_interpolation(flats_angles_abs, angle_a, fa, iflat_a0, iflat_a1);

          int iflat_b1, iflat_b0;
          float fb;
          find_bounds_and_fraction_for_flat_interpolation(flats_angles_abs, angle_b, fb, iflat_b0, iflat_b1);

          for (size_t i = 0; i < raw_data.shape(1); ++i)
          {
            for (size_t j = 0; j < raw_data.shape(2); ++j)
            {
              // bilinear interpolation in time and in angle
              float interpolated_flat = (1 - FF) * ((1 - fa) * flats(iflat_a0, i, j) + fa * flats(iflat_a1, i, j));
              interpolated_flat += FF * ((1 - fb) * flats(iflat_b0, i, j) + fb * flats(iflat_b1, i, j));
              raw_data(iradio, i, j) /= interpolated_flat;
            }
          }
        }
      }
    }
  }
  if (parent->has_double_flats)
  {
    for (int iradio = 0; iradio < (int)num_projections_to_read; iradio++)
    {
      int global_iradio = iradio + output_proj_start;

      // the four coefficients for bilinear interpolation   
      xt::xtensor<float, 1> ff({4});
      
      // when this functions returns, the local_double_flats is filled with good data from the double flats
      // but only for the concerned region, and ldf_last_reads has been used to know if local_double_flats
      // needs to be changed or can be kept as they are. ldf_last_reads will possibly be changed if a double flats reload has been necessary
      setup_interp_and_data_for_double_flats(local_double_flats, ldf_last_reads, global_iradio, ff, read_start_z, read_end_z );

      for (size_t i = 0; i < raw_data.shape(1); ++i)
      {
        for (size_t j = 0; j < raw_data.shape(2); ++j)
        {

          float interpolated_double = 0;
          for (int k = 0; k < 4; k++)
          { 
            if( ff(k) != 0.0 ){
              interpolated_double += ff(k) * local_double_flats(k, i, j);
            }     
          }
          if (parent->doubleflat_is_additive)
          {
            raw_data(iradio, i, j) -= interpolated_double;
          }
          else
          {
            raw_data(iradio, i, j) /= interpolated_double;
          }
        }
      }
    }
  }

  // Apply distortion correction if needed
  if (parent->has_distortion_correction)
    {
      for (int i = 0; i < (int)raw_data.shape()[0]; i++)
        {
	  auto raw_view = xt::view(raw_data, i, xt::all(), xt::all());
          auto signal_view = xt::view(readed_signal, i, xt::all(), xt::all());
	  
	  this->interpolate_sparse(   signal_view, raw_view);
	  
        }
    }
  else
    {
      // No distortion correction needed, use raw data directly
      readed_signal = raw_data;
    }
}


  void EtfScanReading::setup_interp_and_data_for_double_flats(
    xt::xtensor<float, 3> & local_double_flats,
    xt::xtensor<int,1> &ldf_last_reads,
    int i_proj_global, 
    xt::xtensor<float, 1> ff,
    int read_start_z, int read_end_z 
    )
  {
    // when this functions returns, the local_double_flats is filled with good data from the double flats
    // but only for the concerned region, and ldf_last_reads has been used to know if local_double_flats
    // needs to be changed or can be kept as they are. ldf_last_reads will possibly be changed if a double flats reload has been necessary

    double my_angle = abs(  parent->angles_deg_unwrapped.at(i_proj_global) ); // the "at" method always perform bounds check. Even with unset XTENSOR_ENABLE_ASSERT 
 
    int i_turn ;  // the integer part of the number of turns
    float f_turn =  my_angle / 360.0; // the precise number of turns
    float f_in_turn = modfi( f_turn, &i_turn);  // fractional part of the number of turns

    int i_sector = (int)(f_in_turn * parent->nsectors_per_turn); // for just one sector  i_sector would be 0
                                                                 // otherwise it is comprised within 0 and parent->nsectors_per_turn -1
    if( i_sector >= parent->nsectors_per_turn ) { i_sector =  parent->nsectors_per_turn - 1 ; }

    int n_angular_doubles = parent->double_flats_shape[0];

    int n_covered_turns =  ( n_angular_doubles / parent->nsectors_per_turn ); 

    int idx0, idx1, idx2=-1, idx3=-1;

    int i_period =  static_cast<int>(std::floor( (my_angle / 360.0) / parent->period_nturns ) ); 
    float f_period = static_cast<float>(i_period) /  parent->period_nturns ;
    float f_in_period = modfi( ( f_period ), &i_period);

    // between a period and another period averaging is done considering that a period's double flat is centered at the period's middle point. 
    if ( f_turn <=  (0.5f *parent->period_nturns)  || (f_turn >= (n_covered_turns - 0.5f *parent->period_nturns ) ) )
    {
      // before the middle point of the first period or after the middle point of the last period we pad with the first / last period
      // This is implemented setting f(2) and f(3) to zero

      idx0 = std::min( (i_period * parent->nsectors_per_turn + i_sector), n_angular_doubles ); // this min should remain ineffective
      idx1 = std::min( (i_period * parent->nsectors_per_turn + ((i_sector + 1) % parent->nsectors_per_turn)),  n_angular_doubles); // this min should remain ineffective

      ff(1) = modfi( f_in_turn * parent->nsectors_per_turn , nullptr);
      ff(0) = 1 - ff(1);
      ff(2) = 0;
      ff(3) = 0;
    }
    else
    {

      if( f_in_period > 0.5f ) {
        float f_in_period_from_the_middle = (f_in_period - 0.5f);
        // this means from the middle of i_period

        idx0 = i_period * parent->nsectors_per_turn + i_sector;
        idx1 = i_period * parent->nsectors_per_turn + ((i_sector + 1) % parent->nsectors_per_turn);

        idx2 = idx1 + parent->nsectors_per_turn;  // these are for the following period ( + sign )
        idx3 = idx2 + parent->nsectors_per_turn;

        ff(1) = modfi( f_in_turn * parent->nsectors_per_turn , nullptr);
        ff(0) = 1 - ff(0);
        ff(3) = ff(1);
        ff(2) = ff(0);

        ff(1) *=  (1 - f_in_period_from_the_middle) ;  
        ff(0) *=  (1 - f_in_period_from_the_middle) ; 
        ff(3) *=   f_in_period_from_the_middle ;    
        ff(2) *=   f_in_period_from_the_middle ;  
      } else {
        float f_in_period_from_the_middle = (f_in_period + 0.5f);
        // this means from the middle of i_period - 1 ( the previous one)

        idx0 = i_period * parent->nsectors_per_turn + i_sector;
        idx1 = i_period * parent->nsectors_per_turn + ((i_sector + 1) % parent->nsectors_per_turn);

        idx2 = idx1 - parent->nsectors_per_turn;  // these are for previous period following period ( - sign )
        idx3 = idx2 - parent->nsectors_per_turn;

        ff(1) = modfi( f_in_turn * parent->nsectors_per_turn , nullptr);
        ff(0) = 1 - ff(0);
        ff(3) = ff(1);
        ff(2) = ff(0);

        ff(1) *=  f_in_period_from_the_middle ;  
        ff(0) *=  f_in_period_from_the_middle ; 
        ff(3) *=  (1 - f_in_period_from_the_middle) ;   
        ff(2) *=  (1 - f_in_period_from_the_middle) ; 
      } 
    }
    xt::xtensor<int,1> df_indexes= {{ idx0, idx1, idx2, idx3 }} ;
    for( int i=0; i<4; i++ ) {
      if( ldf_last_reads(i) != df_indexes(i)  && (ff(i) != 0.0 ) ) {
        // it is not already in the local_double_flats array. So we have to read it.
        ldf_last_reads(i) = df_indexes(i);

        // where the data is stored
        std::string base_name = "double_flats.h5";
        std::string data_where = "double_flats";
        fs::path file_path = fs::path(parent->mypath) / base_name;


	auto view_to_slice = xt::view(local_double_flats, i, xt::all(), xt::all()) ;
	
        generic_read_float_data(  
	  "double_flats.h5" ,
          "double_flats",
          parent->mypath,
          df_indexes(i),
          read_start_z,
          1,
          read_end_z - read_start_z,
          view_to_slice,
          cmd_channel, res_channel, __LINE__, __FILE__
        ) ;

      }
    }
  }

void EtfScanReading::save_readed_signal_to_hdf5(int n)
{
  std::ostringstream filename;
  filename << "test_" << std::setw(5) << std::setfill('0') << n << ".h5";
  std::string file_name = filename.str();

  HighFive::File file(file_name, HighFive::File::Truncate);

  std::vector<size_t> shape(readed_signal.shape().begin(), readed_signal.shape().end());
  
  auto dataset = file.createDataSet<float>("data", HighFive::DataSpace(shape));


  dataset.write_raw((float*)readed_signal.data());
}

void EtfScanReading::read_chunk_weights()
{
  size_t nx = radios_shape[2];

  // weights
  readed_weight.resize({(size_t)(output_end_z - output_start_z), nx});
  readed_weight_Ifactor.resize({(size_t)(output_proj_end - output_proj_start)});

  if (parent->has_weight)
  {
    hsize_t num_rows_to_read = read_end_z - read_start_z;
    xt::xtensor<float, 2> raw_weight({(size_t)(read_end_z - read_start_z), nx});

    { // the map
      std::string base_name = "weight_map.h5";
      fs::path file_path = fs::path(parent->mypath) / base_name;

      h5::File file(file_path.string(), h5::File::ReadOnly);
      h5::DataSet dataset = file.getDataSet("data");
      std::vector<size_t> dims = dataset.getDimensions();

      std::vector<size_t> offset = {(size_t)output_start_z, 0};
      std::vector<size_t> count = {(size_t)num_rows_to_read, dims[1]};


      dataset.select(offset, count).read_raw<float>(raw_weight.data());

    }
    { // the factors for the current
      std::string base_name = "projections.h5";
      fs::path file_path = fs::path(parent->mypath) / base_name;

      h5::File file(file_path.string(), h5::File::ReadOnly);
      HighFive::DataSet dataset = file.getDataSet("framewise/control");

      size_t length = output_proj_end - output_proj_start;

      dataset.select({(size_t)output_proj_start}, {length}).read_raw<float>(readed_weight_Ifactor.data());
      readed_weight_Ifactor /= parent->nominal_current;
    }

    if (parent->has_distortion_correction)
    {
      std::cout << " interpolating weight " << shape_to_string(readed_weight.shape()) << " " << shape_to_string(raw_weight.shape()) << std::endl;
      this->interpolate_sparse(readed_weight, raw_weight);
    }
    else
    {
      readed_weight = raw_weight;
    }
  }
  else
  {
    readed_weight.fill(1.0);
    readed_weight_Ifactor.fill(1.0);
  }
}

// this function is called from the gridded_accumulator_wrap.cpp
std::unique_ptr<EtfScanReading> EtfScan::createForSlice(int z_start, int z_end, int proj_start, int proj_end)
{
  std::unique_ptr<EtfScanReading> resultObj = std::make_unique<EtfScanReading>();

  {
    int used_reader_idx = -1;

    while( used_reader_idx == -1 )  {

      {
	std::lock_guard<std::mutex> lock(reader_initialisation_mtx);
	// Search for the first zero in the vector reader_is_adopted


	for (int i = 0; i < (int)reader_is_adopted.size()  ; ++i)
	  {
	    
	    if (reader_is_adopted[i] == 0)
	      {
		used_reader_idx = static_cast<int>(i);
		break;
	      }
	  }
	
	
	
	// If no zero is found, throw an exception
	if (used_reader_idx == -1)
	  {
	    
	    
	    // std::stringstream ss;
	    // ss << "No available index found in the vector reader_is_adopted of size " << reader_is_adopted.size()
	    //    << ". FILE: " << __FILE__ << " LINE: " << __LINE__;
	    // throw std::runtime_error(ss.str());
	    
	    
	  } else {
	  reader_is_adopted[used_reader_idx] = 1;
	  resultObj->cmd_channel = read_cmd_pipes[used_reader_idx];
	  resultObj->res_channel = read_res_pipes[used_reader_idx];
	  resultObj->used_reader_idx = used_reader_idx;
	}
      }
      if (used_reader_idx == -1)
	{
      
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

    }
    
    
  }
  
  resultObj->parent = this;
  
  int ny = radios_shape[1];
  // int nx = radios_shape[2];

  if (z_end > ny)
  {
    std::cout << " Problem z_end > ny" << std::endl;
  }
  assert(z_start >= 0);
  assert(z_end <= ny);
  assert(z_end > z_start);

  resultObj->output_start_z = z_start;
  resultObj->output_end_z = z_end;

  resultObj->output_proj_start = proj_start;
  resultObj->output_proj_end = proj_end;

  resultObj->radios_shape = radios_shape;
  resultObj->allocate_output_buffers();
  // resultObj->readed_signal.resize({proj_end-proj_start,   });

  if (!has_distortion_correction)
  {
    resultObj->read_start_z = z_start;
    resultObj->read_end_z = z_end;
    resultObj->reading_shape = xt::xtensor<size_t, 1>{(size_t)(resultObj->read_end_z - resultObj->read_start_z), radios_shape[2]};
  }
  else
  {
    resultObj->set_interpolation_for_subregion(z_start, z_end, dc_start_line, dc_end_line, coords_source_x, coords_source_z);
  }

  if (has_dark)
  {
    resultObj->dark = xt::view(dark, xt::range(resultObj->read_start_z, resultObj->read_end_z), xt::all());
  }

  if (has_flats)
  {
    resultObj->flats = xt::view(flats, xt::all(), xt::range(resultObj->read_start_z, resultObj->read_end_z), xt::all());
  }

  return resultObj;
  // return std::move(resultObj) ;
}

void EtfScanReading::set_interpolation_for_subregion(int z_start,
                                                     int z_end,
                                                     std::vector<int> &original_dc_start_line,
                                                     std::vector<int> &original_dc_end_line,
                                                     xt::xarray<float> &original_coords_source_x,
                                                     xt::xarray<float> &original_coords_source_z)
{
  int nx = radios_shape[2];

  this->read_start_z = original_dc_start_line[z_start];
  this->read_end_z = original_dc_end_line[z_end - 1];

  this->reading_shape = xt::xtensor<size_t, 1>{(size_t)(this->read_end_z - this->read_start_z), radios_shape[2]};

  int current_start = 0;
  for (int i = 0; i < (int)(z_end - z_start); ++i)
  {
    for (int j = 0; j < nx; ++j)
    {
      float x = original_coords_source_x(z_start + i, j);
      float y = original_coords_source_z(z_start + i, j) - this->read_start_z;
      std::vector<int> indices;
      std::vector<float> coeffs;
      interpolate(x, y, indices, coeffs, this->reading_shape[0], nx);
      this->dc_sparse_starts.push_back(current_start);
      current_start += indices.size();
      this->dc_sparse_j.insert(this->dc_sparse_j.end(), indices.begin(), indices.end());
      this->dc_sparse_coeffs.insert(this->dc_sparse_coeffs.end(), coeffs.begin(), coeffs.end());
    }
  }
  dc_sparse_starts.push_back(current_start); // Aggiungi l'elemento finale
}

void EtfScan::reinterpret_distortion_corrections()
{
  int ny = radios_shape[1];
  int nx = radios_shape[2];

  int current_start = 0;

  dc_start_line.resize(ny);
  dc_end_line.resize(ny);

  for (int i = 0; i < ny; ++i)
  {
    dc_start_line[i] = 2 * ny;
    dc_end_line[i] = -1;

    for (int j = 0; j < nx; ++j)
    {
      float x = coords_source_x(i, j);
      float y = coords_source_z(i, j);
      std::vector<int> indices;
      std::vector<float> coeffs;
      interpolate(x, y, indices, coeffs, ny, nx);

      dc_sparse_starts.push_back(current_start);
      current_start += indices.size();
      dc_sparse_j.insert(dc_sparse_j.end(), indices.begin(), indices.end());
      dc_sparse_coeffs.insert(dc_sparse_coeffs.end(), coeffs.begin(), coeffs.end());

      // Aggiorna dc_start_line e dc_end_line
      int min_line = std::floor(y);
      int max_line = std::ceil(y);
      dc_start_line[i] = std::min(dc_start_line[i], min_line);
      dc_end_line[i] = std::max(dc_end_line[i], max_line + 1);
    }
  }
  dc_sparse_starts.push_back(current_start); // Aggiungi l'elemento finale

  for (int i = 0; i < ny; ++i)
  {
    if (dc_start_line[i] < 0)
      dc_start_line[i] = 0;
  }

  // coords_source_x = xt::xarray<float>{};
  // coords_source_z = xt::xarray<float>{};
}

void EtfScan::check_distortion_corrections(std::string external_distortion_file)
{

  std::string base_name = "distortion_correction.h5";
  fs::path file_path = fs::path(mypath) / base_name;
  
  // Check if external_distortion_file is provided
  if (!external_distortion_file.empty()) {
    fs::path external_file_path = external_distortion_file;
    
    // Check if the external file exists
    if (!fs::exists(external_file_path)) {
      std::stringstream ss;
      ss << "Error: File " << external_distortion_file << " does not exist.";
      throw std::runtime_error(ss.str());
    }
    
    // Set file_path to the external distortion file if it exists
    file_path = external_file_path;
  }



  
  if (!fs::exists(file_path))
  {
    has_distortion_correction = false;
  }
  else
  {
    std::vector<std::pair<std::string, xt::xarray<float> &>> datasets = {
        {"coords_source_x", coords_source_x},
        {"coords_source_z", coords_source_z}};
    for (auto &ds : datasets)
    {
      const std::string &dataset_name = ds.first;
      xt::xarray<float> &tensor = ds.second;

      h5::File file(file_path.string(), h5::File::ReadOnly);

      h5::DataSet dataset = file.getDataSet(dataset_name);
      auto dims = dataset.getDimensions();

      if (xt::adapt(dims) != xt::view(radios_shape, xt::range(1, xt::xnone)))
      {
        for (int k = 0; k < 10; k++)
        {
          std::cout << "WARNING, distortion correction shape (" << dims[0] << "," << dims[1] << ") does not match radios shape (" << radios_shape[1] << "," << radios_shape[1] << ")\n";
        }
        std::cout << "Now discarding distortion correction\n";
        has_distortion_correction = false;
        return;
      }

      this->check_dims(dims, dataset_name, file_path);
      tensor.resize(dims);
      dataset.read_raw<float>(((float *)tensor.data()));
    }

    auto max_value = static_cast<float>(coords_source_x.shape()[1] - 1.001);
    coords_source_x = xt::clip(coords_source_x, 0.0f, max_value);

    max_value = static_cast<float>(coords_source_z.shape()[0] - 1.001);
    coords_source_z = xt::clip(coords_source_z, 0.0f, max_value);

    has_distortion_correction = true;
  }
}

void EtfScan::check_projections()
{

  std::string base_name = "projections.h5";
  fs::path file_path = fs::path(mypath) / base_name;
  

  
  if (!fs::exists(file_path))
  {
    std::stringstream ss;
    ss << " Could not find projections file " << file_path.string();
    throw std::runtime_error(ss.str());
  }
  else
  {
    h5::File file(file_path.string(), h5::File::ReadOnly);

    { // Globals
      std::vector<std::pair<std::string, double &>> datasets = {
          {"globals/beam_energy", beam_energy},
          {"globals/distance", distance},
          {"globals/estimated_cor_from_motor", estimated_cor},
          {"globals/x_pixel_size", x_pixel_size},
          {"globals/y_pixel_size", y_pixel_size},
      };

      for (auto &ds : datasets)
      {
        // commentnst std::string& dataset_name = ds.first;
        double &scalar = ds.second;
        const std::string &dataset_name = ds.first;
        h5::DataSet dataset = file.getDataSet(dataset_name);
        dataset.read(scalar);
      }
      {
        h5::DataSet dataset = file.getDataSet("globals/bliss_original_files");
        dataset.read(bliss_original_files);
      }
    }

    { // Framewise
      std::vector<std::pair<std::string, xt::xarray<double> &>> datasets = {
          {"framewise/angles_deg", angles_deg},
          {"framewise/unwrapped_angles_deg", angles_deg_unwrapped},
          {"framewise/control", machine_current},
          {"framewise/x_translation", x_translation},
          {"framewise/y_translation", y_translation},
          {"framewise/z_translation", z_translation}};

      for (auto &ds : datasets)
      {
        const std::string &dataset_name = ds.first;
        xt::xarray<double> &tensor = ds.second;
        h5::DataSet dataset = file.getDataSet(dataset_name);
        auto dims = dataset.getDimensions();
        tensor.resize(dims);
        dataset.read_raw<double>(((double *)tensor.data()));
      }

      // angles in radians
      angles_rad = xt::deg2rad(angles_deg);
      angles_rad_unwrapped = xt::deg2rad(angles_deg_unwrapped);

      // translation in pixels
      x_translation /= x_pixel_size;
      y_translation /= x_pixel_size;
      z_translation /= x_pixel_size;
    }
    {
      // shape
      h5::DataSet dataset = file.getDataSet("data");
      radios_shape = xt::adapt(dataset.getDimensions());
      _MendBlissGaps(); 
    }
  }
}


void EtfScan::_MendBlissGaps() {
  // This code is just a temporary fix.
  // This code could possibly be removed once the bliss files will be written without undue gaps in z_translations.
  // The  gaps appear due to an incoherence, in the writing routines, in the z definitions between standard scans
  // and helical scans. A gap appear between a helical scan and a standard scans.
  // The standard scans are , if any, at the beginning and at the end.

  // Estimate a mean step value
  xt::xarray<double> diff_data = xt::diff(z_translation);
  double step = xt::mean(xt::abs(diff_data))();

  // Getting where there is no movement 
  xt::xarray<bool> is_flat = xt::abs(diff_data) < step / 200;

  if ( is_flat(0) ) { // flat scan at the beginning
    int idx = -1;

    for(int i=0; i< (int) is_flat.shape()[0]-1; i++ ) {
      if (  ! is_flat(i+1) )  {
	idx = i;
	break ; 
      }
    }
    if( idx != -1 ) {
      // idx+1 is the last one of a flat scan
      double gap = z_translation[idx+2] - z_translation[idx+1] ;

      std::cout << " Gap beginning " << gap << " at id " << idx <<  std::endl;

      int itest = idx + 2  ; 

      int count_flat = 0;

      for(int i= itest ; i < itest + 1000 ; i++ ) {  // the acceleration may be slow and there mey be null steps. Here we want to detect z_stages which have gaps that should not be mended
	if ( i< (int) is_flat.shape()[0] ) {
	  if ( is_flat(i) ) count_flat ++ ; 
	}
      }
      if( count_flat != 1000 ) { // it is not a z_stage
	if ( idx > 800 ) { //
	  xt::view(z_translation, xt::range(0, idx + 2)) += gap;
	  
	  // for(int k= idx-100; k< idx+100 ; k++) {
	  //   std::cout <<   z_translation(k)  << std::endl;
	  // }
	}
      }
    }
  }

  if ( is_flat(is_flat.size() - 1) ) { // flat scan at the end
    int idx = -1;

    for(int i= is_flat.size()-1  ; i>0; i-- ) {
      if (  ! is_flat(i-1) )  {
	idx = i;
	break ; 
      }
    }
    if( idx != -1 ) {

      double gap = z_translation[idx] - z_translation[idx-1] ; 
      std::cout << " END gap " << gap << " at id " << idx <<  std::endl;

      
      int itest = idx - 2  ;  //  small but safe step

      int count_flat = 0;

      for(int i= itest ; i > itest - 1000 ; i-- ) {  //  Here we want to detect z_stages which have gaps that should not be mended
	if ( i >= 0 ) {
	  if ( is_flat(i) ) count_flat ++ ; 
	}
      }
      if( count_flat != 1000 ) { // the previous one is not a z_stage
	if ( idx < (int) is_flat.size() - 800 ) { //
	  xt::view(z_translation, xt::range(idx, z_translation.size())) -= gap;
	}
      }
	  // for(int k= idx-100; k< idx+100 ; k++) {
	  //   std::cout <<   z_translation(k)  << std::endl;
	  // }
      
      
    }
  }
}

void EtfScan::check_diffusion_corrections()
{
  std::string base_name = "diffusion_correction.h5";

  fs::path file_path = fs::path(mypath) / base_name;

  if (!fs::exists(file_path))
  {
    has_diffusion_correction = false;
  }
  else
  {
    h5::File file(file_path.string(), h5::File::ReadOnly);

    {
      h5::DataSet dataset = file.getDataSet("current");
      auto dims = dataset.getDimensions();
      diffusion_currents.resize(dims);
      dataset.read_raw<float>(((float *)diffusion_currents.data()));
    }
    {
      h5::DataSet dataset = file.getDataSet("data");
      auto dims = dataset.getDimensions();
      diffusion_shape = xt::adapt(dataset.getDimensions());
    }
    {
      if (diffusion_shape(0) != diffusion_currents.shape()[0])
      {
        std::stringstream ss;
        ss << " The diffusion corrections have a length for currents which is " << diffusion_shape(0)
           << " and does not correspond to the length of the corrections which is " << diffusion_currents.shape()[0];
        throw std::runtime_error(ss.str());
      }
    }
    {
      h5::DataSet dataset = file.getDataSet("binnings");
      auto dims = dataset.getDimensions();
      std::vector<size_t> expected_dimensions = {2};
      if (dims != expected_dimensions)
      {
        throw std::runtime_error(" The dataset binnings does not have the two elements that it should have ");
      }

      int binnings_tmp[2];
      dataset.read_raw<int>(((int *)binnings_tmp));

      diffusion_bin_proj = binnings_tmp[0];
      diffusion_bin_x = binnings_tmp[1];
      ;
    }
    has_diffusion_correction = true;
  }
}

void interpolate(float x, float y, std::vector<int> &indices, std::vector<float> &coeffs, int m, int n)
{
  int x0 = std::floor(x);
  int x1 = x0 + 1;
  int y0 = std::floor(y);
  int y1 = y0 + 1;

  x0 = std::max(std::min(x0, n - 1), 0);
  x1 = std::max(std::min(x1, n - 1), 0);

  y0 = std::max(std::min(y0, m - 1), 0);
  y1 = std::max(std::min(y1, m - 1), 0);

  float dx = x - x0;
  float dy = y - y0;

  // Indici
  indices.push_back(y0 * n + x0);
  indices.push_back(y0 * n + x1);
  indices.push_back(y1 * n + x0);
  indices.push_back(y1 * n + x1);

  // Coefficienti di interpolazione
  coeffs.push_back((1 - dx) * (1 - dy));
  coeffs.push_back(dx * (1 - dy));
  coeffs.push_back((1 - dx) * dy);
  coeffs.push_back(dx * dy);
}

void EtfScanReading::apply_diffusion_corrections(xt::xtensor<float, 3> &raw_data, int output_proj_start, int output_proj_end, int read_start_z, int read_end_z)
{

  // subtract the diffused background

  // first thing to do: calculate an incapsulating volume in the diffusion digest that encompass our projections data
  // that we have just read. This calculation is done on the basis of the binnings

  int corr_read_proj_start = std::min(output_proj_start / parent->diffusion_bin_proj, static_cast<int>(parent->diffusion_shape[0]) - 1);
  int corr_read_proj_end = static_cast<int>(std::ceil(static_cast<double>(output_proj_end) / parent->diffusion_bin_proj));

  corr_read_proj_end = std::min(corr_read_proj_end, static_cast<int>(parent->diffusion_shape[0]));

  int corr_read_start_z = std::min(read_start_z / parent->diffusion_bin_x, static_cast<int>(parent->diffusion_shape[1]) - 1);
  int corr_read_end_z = static_cast<int>(std::ceil(static_cast<double>(read_end_z) / parent->diffusion_bin_x));

  corr_read_end_z = std::min(corr_read_end_z, static_cast<int>(parent->diffusion_shape[1]));

  // Now that we know the encompassing region, allocate the memory buffer
  xt::xtensor<float, 3> diffusion_correction_data;
  diffusion_correction_data.resize({(size_t)(corr_read_proj_end - corr_read_proj_start), (size_t)(corr_read_end_z - corr_read_start_z), parent->diffusion_shape[2]});

  // read the data through the pipe
  {
    // where the data is stored
    std::string base_name = "diffusion_correction.h5";
    std::string data_where = "data";
    fs::path file_path = fs::path(parent->mypath) / base_name;

    
    generic_read_float_data(  
      "diffusion_correction.h5" ,
      "data",
      parent->mypath,
      corr_read_proj_start,
      corr_read_start_z,
      corr_read_proj_end - corr_read_proj_start,
      corr_read_end_z - corr_read_start_z,
      diffusion_correction_data ,
      cmd_channel, res_channel, __LINE__, __FILE__
    ) ;
  }
  // rescale diffusion intensity current to the nominal current
  try {

    
  for (int i = 0; i < (int)diffusion_correction_data.shape()[0]; i++)
      {
	diffusion_correction_data(i) *=  parent->nominal_current / parent->diffusion_currents(  i +  corr_read_proj_start ); 
      }

    // apply the corrections

    xt::xtensor<float, 1> currents_segment = xt::view(parent->machine_current, xt::range(output_proj_start, output_proj_end));
    apply_interpolated_scintillator_diffusion_correction(
							 raw_data,
							 currents_segment,
							 parent->nominal_current,
							 diffusion_correction_data,
							 parent->diffusion_bin_proj,
							 parent->diffusion_bin_x,
							 output_proj_start,
							 read_start_z,
							 corr_read_proj_start,
							 corr_read_start_z
							 );
  } catch (const std::exception& e) {
    std::cerr << "Eccezione catturata: " << e.what() << std::endl;
  }
  
}

float clamped_interpolation(const xt::xtensor<float, 3> &data, float x, float y, float z)
{
  // Clamp indices to valid range
  int x0 = std::clamp(static_cast<int>(x), 0, static_cast<int>(data.shape()[0] - 1));
  int y0 = std::clamp(static_cast<int>(y), 0, static_cast<int>(data.shape()[1] - 1));
  int z0 = std::clamp(static_cast<int>(z), 0, static_cast<int>(data.shape()[2] - 1));

  // Compute clamped fractional part
  float dx = x - x0;
  float dy = y - y0;
  float dz = z - z0;

  // Ensure indices are within bounds
  int x1 = std::min(x0 + 1, static_cast<int>(data.shape()[0] - 1));
  int y1 = std::min(y0 + 1, static_cast<int>(data.shape()[1] - 1));
  int z1 = std::min(z0 + 1, static_cast<int>(data.shape()[2] - 1));

  // Linear interpolation
  float c00 = data(x0, y0, z0) * (1 - dz) + data(x0, y0, z1) * dz;
  float c01 = data(x0, y1, z0) * (1 - dz) + data(x0, y1, z1) * dz;
  float c10 = data(x1, y0, z0) * (1 - dz) + data(x1, y0, z1) * dz;
  float c11 = data(x1, y1, z0) * (1 - dz) + data(x1, y1, z1) * dz;

  float c0 = c00 * (1 - dy) + c01 * dy;
  float c1 = c10 * (1 - dy) + c11 * dy;

  return c0 * (1 - dx) + c1 * dx;
}

void apply_interpolated_scintillator_diffusion_correction(
    xt::xtensor<float, 3> &raw_data,
    xt::xtensor<float, 1> &currents_segment,
    float nominal_current,
    xt::xtensor<float, 3> &diffusion_correction_data,
    int diffusion_bin_proj,
    int diffusion_bin_x,
    int output_proj_start,
    int read_start_z,
    int corr_read_proj_start,
    int corr_read_start_z)
{
  /*
    This function subtracts interpolated values, obtained from diffusion_correction_data, from raw_data.
    Each point (i, j, k) in raw_data corresponds to a fractional position in diffusion_correction_data:

    In diffusion_correction_data, the corresponding fractional positions are:
    - Projection index: ( (i + output_proj_start )/ diffusion_bin_proj) - corr_read_proj_start
    - Pixel Z index: (j / diffusion_bin_x) - corr_read_start_z
    - Pixel X index: k / diffusion_bin_x

    The function iterates through all elements in raw_data, calculates their corresponding
    fractional positions in diffusion_correction_data, interpolates the values, and subtracts
    the interpolated values from raw_data.
  */

  // Iterate over the raw_data
  for (size_t i = 0; i < raw_data.shape()[0]; ++i)
  {
    for (size_t j = 0; j < raw_data.shape()[1]; ++j)
    {
      for (size_t k = 0; k < raw_data.shape()[2]; ++k)
      {
        // Calculate corresponding fractional indices in diffusion_correction_data
        float frac_proj_index = static_cast<float>(i + output_proj_start) / diffusion_bin_proj - corr_read_proj_start;
        float frac_z_index = static_cast<float>(j) / diffusion_bin_x - corr_read_start_z;
        float frac_x_index = static_cast<float>(k) / diffusion_bin_x;

        // Interpolate value from diffusion_correction_data
        float interpolated_value = clamped_interpolation(diffusion_correction_data, frac_proj_index, frac_z_index, frac_x_index);
        // Subtract the interpolated value from raw_data
        raw_data(i, j, k) -= interpolated_value * currents_segment[i] / nominal_current;
      }
    }
  }
}



void EtfScan::stop_readers()
{
  for(auto i: reading_children) {
    std:: cout << " stopping reader process"<<  reading_children[i] << std::endl;
    kill(reading_children[i], SIGTERM) ;
  }
  
}


void EtfScan::init_readers(int num_reading_sub_processes)
{

  if (reading_children.size())
  {
    if (num_reading_sub_processes != (int)reading_children.size())
    {
      std::stringstream ss;
      ss << "The number of required reading thread is " << num_reading_sub_processes
         << " but the readers have already been set up and they are in number of " << reading_children.size()
         << ". file:" << __FILE__ << " line:" << __LINE__;
      throw std::runtime_error(ss.str());
    }
    else
    {
      // nothing to do. Reader processes exist already.
      return;
    }
  }

  reading_children.resize(num_reading_sub_processes);
  read_cmd_pipes.resize(num_reading_sub_processes);
  read_res_pipes.resize(num_reading_sub_processes);

  reader_is_adopted = std::vector<int>(read_cmd_pipes.size(), 0);

  
  this->num_reading_sub_processes  = num_reading_sub_processes ; 

  
  // Set the parent process as the leader of the process group
  setpgid(0, 0);

  // Register the terminateChildren function to be called upon exit
  atexit([]()
         { killpg(getpgid(0), SIGTERM); });

  // Create pipes
  {

    std::vector<int> tmp_cmd_pipe(2 * num_reading_sub_processes);

    // "2*"  because pipes are always created in pairs. Int the pair the first one is reading the other for writing
    std::vector<int> tmp_res_pipe(2 * num_reading_sub_processes);

    for (int i = 0; i < num_reading_sub_processes; ++i)
    {

      if (pipe(&tmp_cmd_pipe[2 * i]) == -1 || pipe(&tmp_res_pipe[2 * i]) == -1)
      {
        perror("pipe");
        std::stringstream ss;
        ss << "Error creating pipes number " << i;
        throw std::runtime_error(ss.str());
      }

      pid_t pid = fork();
      if (pid == -1)
      {
        perror("fork");
        std::stringstream ss;
        ss << "Error forking process " << i << "th";
        throw std::runtime_error(ss.str());
      }
      else if (pid == 0)
      {
        // Child process.
        setpgid(0, getpgid(getppid())); // Set the child process group ID
        close(tmp_cmd_pipe[2 * i + 1]); // The child does not write to the command pipe
        close(tmp_res_pipe[2 * i + 0]); // The child does not read from the result pipe
        ReadingChildProcess(i, tmp_cmd_pipe[2 * i + 0], tmp_res_pipe[2 * i + 1]);
      }
      else
      {
        // Parent process
        reading_children[i] = pid;

        close(tmp_cmd_pipe[2 * i + 0]); // The parent does not read from the command pipe
        close(tmp_res_pipe[2 * i + 1]); // The parent does not write to the result pipe

        read_cmd_pipes[i] = tmp_cmd_pipe[2 * i + 1];
        read_res_pipes[i] = tmp_res_pipe[2 * i + 0];
      }
    }
  }
}

// this function is the main loop of the reader
void ReadingChildProcess(int id, int cmd_read_fd, int res_write_fd)
{
  std::vector<float> buffer; //  to store the reading results, will be resized to match the requestes
  try
  {
    while (true)
    {
      // wait for a command
      Command cmd;
      ssize_t bytesRead = read(cmd_read_fd, &cmd, sizeof(cmd));

      // process the commands
      if (bytesRead <= 0)
        break; // If no more commands, terminate
      // read the HDF5 slice
      read_hdf5_slice(cmd.filename, cmd.dataset_url, cmd.start1, cmd.start2, cmd.span1, cmd.span2, buffer);



      // Send the data back to the parent process
      ssize_t dataSize = buffer.size() * sizeof(float);
      ssize_t totalWritten = 0;

      while (totalWritten < dataSize) {
	ssize_t bytesWritten = write(res_write_fd, buffer.data() + totalWritten, dataSize - totalWritten);
	
	if (bytesWritten == -1) {
	  // Gestire errore (EINTR, EAGAIN, o altri errori)
	  std::string errorMsg = "Error during write: ";
	  errorMsg += strerror(errno); // Cattura il messaggio d'errore relativo a errno
	  throw std::runtime_error(errorMsg);
	}
	totalWritten += bytesWritten;
      }
      
      if (totalWritten != dataSize) {
	std::string errorMsg = "Error, not all the bytes have been written: ";
	errorMsg += std::to_string(totalWritten) + " over " + std::to_string(dataSize);
	throw std::runtime_error(errorMsg);
      }

      
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << "ReadingChild " << id << " encountered an error: " << e.what() << std::endl;
    close(cmd_read_fd);  // Close the read side of the command pipe
    close(res_write_fd); // Close the write side of the result pipe
    exit(1);             // Terminate the ReadingChild process with an error status
  }
  close(cmd_read_fd);  // Close the read side of the command pipe
  close(res_write_fd); // Close the write side of the result pipe
  exit(0);             // Terminate the ReadingChild process
}



void read_hdf5_slice(const char *filename, const char *dataset_url, hsize_t start1, hsize_t start2, hsize_t span1, hsize_t span2, std::vector<float> &buffer)
{
  try
  {

    // Open the HDF5 file and dataset
    HighFive::File file(filename, HighFive::File::ReadOnly);
    HighFive::DataSet dataset = file.getDataSet(dataset_url);

    // Get the dataspace of the dataset
    HighFive::DataSpace dataspace = dataset.getSpace();

    // Get the dimensions of the dataset
    std::vector<size_t> dims = dataspace.getDimensions();

    // Check if dimensions match expected 3D structure
    if (dims.size() != 3)
    {
      std::cerr << "Dataset is not 3D" << std::endl;
      return;
    }
    // Define hyperslab selection (start at specified indices for the first two dimensions, span full range for the third dimension)
    std::vector<size_t> offset = {start1, start2, 0};
    std::vector<size_t> count = {span1, span2, dims[2]};

    // Resize buffer if necessary
    buffer.resize(span1 * span2 * dims[2]);

    // Read data into buffer
    dataset.select(offset, count).read_raw<float>((float *)buffer.data());
  }
  catch (const HighFive::Exception &e)
  {
    std::cerr << "HighFive error: " << e.what() << std::endl;
    throw;
  }
}

