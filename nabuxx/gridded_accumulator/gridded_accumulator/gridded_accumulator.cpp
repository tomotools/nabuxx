#define XTENSOR_ENABLE_ASSERT 

#include <vector>
#include <tuple>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>

#include <xtensor/xio.hpp>
#include <xtensor/xview.hpp>

#include "pybind11/numpy.h"
#include "gridded_accumulator.h"
#include "utilities.h"
#include <etfscan.hpp>
#include "../etfscan/span_info.h"
#include <math.h>

#include <omp.h>
#include <future>
#include <mutex>

#include <sys/types.h>
#include<random>
#include<array>


using namespace nabuxx;


GriddedAccumulator::GriddedAccumulator(
    EtfScan *etfscan_arg,
    py::buffer_info &gridded_radios_info,
    py::buffer_info &gridded_weights_info,

    py::buffer_info &diagnostic_radios_info,
    py::buffer_info &diagnostic_weights_info,
    py::buffer_info &diagnostic_angles_info,

    py::buffer_info &diagnostic_searched_angles_rad_clipped_info,
    py::buffer_info &diagnostic_zpix_transl_info,
    int diag_zpro_run
				       )
{

  this->etfscan = etfscan_arg;


  this->nominal_current = 0.2;

  this->gridded_radios_info.copy_from(gridded_radios_info);
  this->gridded_weights_info.copy_from(gridded_weights_info);
  this->diagnostic_radios_info.copy_from(diagnostic_radios_info);
  this->diagnostic_weights_info.copy_from(diagnostic_weights_info);
  this->diagnostic_angles_info.copy_from(diagnostic_angles_info);

  this->diagnostic_searched_angles_rad_clipped_info.copy_from(diagnostic_searched_angles_rad_clipped_info);
  this->diagnostic_zpix_transl_info.copy_from(diagnostic_zpix_transl_info);
  this->diag_zpro_run = diag_zpro_run;


  // @stripes this->stripes_coefficients_list_info .copy_from( stripes_coefficients_list_info ) ;
  // @stripes this->stripes_profile_info .copy_from( stripes_profile_info ) ;

  size_t n_proj_gridded = gridded_radios_info.shape[0];

  if (diag_zpro_run)
  {
    // no multithreading when diagnostic are collected because we have always the case of few projections around a contributed diagnostic
    // and therefore all the thread would finish waiting for the same
    // target
    num_of_threads = 1;
  }
  else
  {
    num_of_threads = this->etfscan-> num_reading_sub_processes ;
  }

  // to avoid dead lock accessing pairs of contiguous  projections when interpolation-projecting the data on gridded
  this->num_of_threads = std::max(1, std::min(num_of_threads, ((int)n_proj_gridded) / 2));

  mutexes.resize(n_proj_gridded);

  int n_searched_diags = this->diagnostic_searched_angles_rad_clipped_info.shape[0];
  mutexes_diag.resize(n_searched_diags * 2);

  nabuxx::buffer_size((this->gridded_radios_info));

  {
    std::vector<std::tuple<py::buffer_info *, std::string, int>> todo = {
        {&(this->gridded_radios_info), "gridded_radio", 3},
        {&(this->gridded_weights_info), "gridded_weights_info", 3},
    };

    for (auto tp : todo)
    {
      if (std::get<0>(tp)->ndim != 0)
      {
        if (!check_c_contiguity(*std::get<0>(tp)))
        {
          std::stringstream ss;
          ss << " argument " << std::get<1>(tp) << "  should be contiguous in creator of GriddedAccumulator ";
          throw std::invalid_argument(ss.str());
        }
        if (std::get<0>(tp)->ndim != 0 && std::get<0>(tp)->ndim != std::get<2>(tp))
        {
          std::stringstream ss;
          ss << " argument " << std::get<1>(tp) << "  should have " << std::get<2>(tp) << " dimensions. in creator of GriddedAccumulator. It has " << std::get<0>(tp)->ndim;
          throw std::invalid_argument(ss.str());
        }
      }
    }
  }
};


std::vector<int> arange(int start, int end, int step) {
    std::vector<int> values;
    for (int i = start; i < end; i += step) {
        values.push_back(i);
    }
    return values;
}


void GriddedAccumulator::read_and_process(int n_granularity) {
  
  std::vector<ArgumentForProcess> argument_list;

  ChunkInfo & chunk_info = this->etfscan->span_info->chunk_info ; 

  int phase_margin_pix = this->etfscan->span_info->pag_margin_pix ; 
  
  int proj_num_start = chunk_info.angle_index_span(0) ;
  int proj_num_end   = chunk_info.angle_index_span(1) ;
                                                                                                                                                                                                          
  std::vector<int> pnum_start_list = arange(proj_num_start, proj_num_end, n_granularity);                                                                                                                  
  std::vector<int> pnum_end_list(pnum_start_list.begin() + 1, pnum_start_list.end()  );                                                                                                                    
  pnum_end_list.push_back(proj_num_end);                                                                                                                                                                    
                                                
  int my_chunk_first_pnum = proj_num_start ; 

  // what we want to reconstruct
  int subr_start_z = chunk_info.absolute_span_v(0)- phase_margin_pix  ;
  int subr_end_z = chunk_info.absolute_span_v(1)  + phase_margin_pix  ;

  for (size_t i = 0; i < pnum_start_list.size(); ++i) {

    int pnum_start = pnum_start_list[i];
    int pnum_end = pnum_end_list[i];
    
    int proj_start_in_chunk = pnum_start - my_chunk_first_pnum;
    int proj_end_in_chunk = pnum_end - my_chunk_first_pnum;
    
    auto my_integer_shifts_v = xt::view(chunk_info.integer_shift_v, xt::range(proj_start_in_chunk, proj_end_in_chunk));
    auto fract_complement_shifts_v = xt::view(chunk_info.fract_complement_to_integer_shift_v, xt::range(proj_start_in_chunk, proj_end_in_chunk));
    auto x_shifts_list = xt::view(chunk_info.x_pix_per_proj, xt::range(proj_start_in_chunk, proj_end_in_chunk));
    
 
    // where we need to catch the data. Possibly out of the detector : this is for an imaginary detector possibly having lines below zero and beyond its vertical span
    auto subr_start_z_list = subr_start_z - my_integer_shifts_v;
    auto subr_end_z_list = subr_end_z - my_integer_shifts_v + 1;
    
    
    // where we need to catch the data in a single minecraft read. Start and end for all the projections of this chunk grain ( subchunk )
    int my_subr_start_z = static_cast<int>(xt::amin(subr_start_z_list)());
    int my_subr_end_z = static_cast<int>(xt::amax(subr_end_z_list)()) ;
    
    
    // What we can read from the data (trimming out the out of detector regions)
    int datasrc_start_z = std::max(0,  my_subr_start_z  );
    int datasrc_end_z = std::min(static_cast<int>(this->etfscan->span_info->detector_shape_vh(0)), my_subr_end_z);
    
    // adding a new argument
    // for the chunk grain ( subchunk ) 
    argument_list.push_back(
			    ArgumentForProcess(
					       { proj_start_in_chunk, proj_end_in_chunk },  // relative ( to the chunk) projection inteval
					       std::array<int,2>{ subr_start_z, subr_end_z   }, // interval I would ideally read
					       std::array<int,2>{ datasrc_start_z, datasrc_end_z    }, // interval I can practically read
					       { pnum_start, pnum_end  }  // absolute projection interval
					       )
			    );
    
  }
  
  // Randomize the order of elements in argument_list.
  // It is may be completely unuseful but I put it here scaramanticly because
  // I would say that even in the worst case reading will not always be on the same regions.
  // But there are meybe better schemes. in any case reading is fast so far
  std::random_device rd;  // Random number generator
  std::mt19937 g(rd());   // Random engine seeded by random_device
  std::shuffle(argument_list.begin(), argument_list.end(), g);
  
  
#pragma omp parallel for num_threads(num_of_threads)
  for (size_t i = 0; i < argument_list.size(); ++i) {
    this->extract_preprocess_with_flats( argument_list[i]    );
  }
}



int GriddedAccumulator::mend_zero_weight_border_regions()
{
  // it may happen that accumulated array have problems at the border region on the left and on the right, this fixes the problem

  int count = 0;
  size_t n_proj_gridded = gridded_radios_info.shape[0];
  size_t ny = gridded_radios_info.shape[1];
  size_t nx = gridded_radios_info.shape[2];

  float *r = (float *)gridded_radios_info.ptr;
  float *w = (float *)gridded_weights_info.ptr;

  printf(" pesi %e %e\n", w[0], w[nx - 1]);

  for (size_t ipro = 0; ipro < n_proj_gridded; ipro++)
  {
    for (size_t iy = 0; iy < ny; iy++)
    {

      int ix = 0;
      for (ix = 0; ix < (int)nx; ix++)
      {
        if (w[(ipro * ny + iy) * nx + ix] > 1.0e-7)
        {
          break;
        }
      }
      if (ix > 0 && ix < (int)nx)
      {
        count += 1;
        float val_w = w[(ipro * ny + iy) * nx + ix];
        float val_r = r[(ipro * ny + iy) * nx + ix];

        int ix_nonzero = ix;
        for (ix = 0; ix < (int)ix_nonzero; ix++)
        {
          w[(ipro * ny + iy) * nx + ix] = val_w;
          r[(ipro * ny + iy) * nx + ix] = val_r;
        }
      }

      ix = nx - 1;
      for (ix = nx - 1; ix > -1; ix--)
      {
        if (w[(ipro * ny + iy) * nx + ix] > 1.0e-7)
        {
          break;
        }
      }
      if (ix > -1 && ix < (int)nx - 1)
      {
        count += 1;
        float val_w = w[(ipro * ny + iy) * nx + ix];
        float val_r = r[(ipro * ny + iy) * nx + ix];

        int ix_nonzero = ix;
        for (ix = nx - 1; ix > ix_nonzero; ix--)
        {
          w[(ipro * ny + iy) * nx + ix] = val_w;
          r[(ipro * ny + iy) * nx + ix] = val_r;
        }
      }
    }
  }

  return count;
}

void InterpInfo::setup_for_iproj(GriddedAccumulator &accu, int i_proj_rel,  ChunkInfo &chunk_info)
{

  {

    // region that we want to create. Moving target. It moves with the z translation ( it depends on iproj )
    // The -1 accounts for fractional step magin
    int s_start_z = subr_start_z_iproj;
    int s_end_z = subr_end_z_iproj - 1;

    // data that we have
    int d_start_z = dtasrc_start_z;
    int d_end_z = dtasrc_end_z - 1;

    /*
      """ determines the useful lines which can be transferred from d to s

      """
    */

    int t_h = s_end_z - s_start_z;
    int s_h = d_end_z - d_start_z;

    this->target_start = std::max(0, d_start_z - s_start_z);
    this->target_end = std::min(t_h, d_end_z - s_start_z);

    this->source_start = std::max(0, s_start_z - d_start_z);
    this->source_end = std::min(s_h, s_end_z - d_start_z);
  }

  {
    // for horizontal interpolation
    integer_x_shift_iproj = (int)(floorf(x_shift_iproj));
    fract_x_shift_iproj = x_shift_iproj - integer_x_shift_iproj;
  }

  {
    // interpolation between projections
    // It will be between ipro_gridded_0,1  with coeffient this->ang_intp_alpha on this->ipro_gridded_1
    double my_angle = chunk_info.chunk_angles_rad[chunk_relative_projection_start + i_proj_rel];
    this->my_angle = my_angle;
    my_angle = fmod(my_angle, 2 * M_PI);
    if (my_angle < 0)
      my_angle = my_angle + 2 * M_PI;

    float f_pos = n_proj_gridded * (my_angle / (2 * M_PI));

    
    this->ipro_gridded_0 = (int)floorf(f_pos);
    this->ipro_gridded_1 = (this->ipro_gridded_0 + 1) % n_proj_gridded;
    this->ang_intp_alpha = f_pos - this->ipro_gridded_0;
  }
};


void GriddedAccumulator::project_radio(GriddedAccumulator::ProjectRadioArgument my_arguments)
{

  py::buffer_info &target_info = my_arguments.target_info;
  // vertical height of the target
  size_t target_v = target_info.shape[1];
  
  // precalculated informations to perform the interpolation
  const InterpInfo &tpinf = my_arguments.interpolation_info;


  // This method works column by column. Working in this way allows to limit the size of temporary arrays.
  // The vertical interpolation is done first on the columns buffer, together with the data reduction.
  // Then taking this reduced column as source the targets are filled considering angular interpolation and horizontal interpolation

  std::vector<float> column_buffer_vect(tpinf.d_w_z);
  std::vector<float> column_buffer_weight_vect(tpinf.d_w_z);
  std::vector<float> column_buffer_gridded_vect(target_v);
  std::vector<float> column_buffer_gridded_weight_vect(target_v);

  // accessing a raw pointer is slightly faster than accessing a std::vector.
  // in case of debugging, reverting to the usage of the std::Vector objects should be easy ( for example through a redefinition of the following symbols )
  // std::vectors bound checking can be activated

  float * column_buffer                 = &column_buffer_vect                 [0];
  float * column_buffer_weight          = &column_buffer_weight_vect          [0];
  float * column_buffer_gridded         = &column_buffer_gridded_vect         [0];
  float * column_buffer_gridded_weight  = &column_buffer_gridded_weight_vect  [0];
   
  // the data sources, the radios and the weights 
  float *data = my_arguments.data;
  float *data_weight = my_arguments.data_weight;
  
  //the factors for the intensity
  float data_weight_Ifactor = my_arguments.data_weight_Ifactor;


  
  //  the informations about the python buffer on which we are going to write
  py::buffer_info &target_weights_info = my_arguments.target_weights_info;


  // the buffers for diagnostic, these also will be written
  py::buffer_info &diag_info = my_arguments.diag_info;
  py::buffer_info &diag_weights_info = my_arguments.diag_weights_info;
  

  // check that the region height foreseen for reading ( buffers have been allocated accoringly)
  // corresponds to the width of the processed region given in absolute coordinates
  assert((tpinf.dtasrc_end_z - tpinf.dtasrc_start_z) == tpinf.d_w_z);


  // The width of the target matches the width of the radios 
  size_t target_w = tpinf.d_w_x;

  float *target_ptr = (float *)target_info.ptr;


  for (int i_col = 0; i_col < (int)tpinf.d_w_x; i_col++){
    for (int i_z = 0; i_z < (int)tpinf.d_w_z; i_z++)
      {
	column_buffer[i_z] = data[i_z * tpinf.d_w_x + i_col];
      }
    
    if (data_weight )
      {
	for (int i_z = 0; i_z < tpinf.d_w_z; i_z++)
	  {
	    column_buffer_weight[i_z] = data_weight[ i_z  * target_w + i_col] * data_weight_Ifactor  ;
	  }
      }
    
    if (data_weight)
      {
	if (tpinf.fract_complement_shift_v)
	  {
	    // fractional verticalshift
	    const float &alpha = tpinf.fract_complement_shift_v;
	    for (int i_z = 0; i_z <= tpinf.d_w_z - 2; i_z++)
	      {
		column_buffer_weight[i_z] = ((1.0 - alpha) * column_buffer_weight[i_z] + alpha * column_buffer_weight[i_z + 1]);
	      }
	  }

	// writing on a column whose height matches the target
	for (int i_z = tpinf.target_start; i_z < tpinf.target_end; i_z++)
	  {
	    column_buffer_gridded_weight[i_z] = column_buffer_weight[tpinf.source_start + i_z - tpinf.target_start];
	  }
	float padding_low = 1.0e-7, padding_high = 1.0e-7;
	for (int i_z = 0; i_z < tpinf.target_start; i_z++)
	  {
	    column_buffer_gridded_weight[i_z] = padding_low;
	  }
	for (int i_z = tpinf.target_end; i_z < (int)target_v; i_z++)
	  {
	    column_buffer_gridded_weight[i_z] = padding_high;
	  }
      }
    
    {
      // fractional verticalshift
      if (tpinf.fract_complement_shift_v)
	{
	  const float &alpha = tpinf.fract_complement_shift_v;
	  for (int i_z = 0; i_z <= tpinf.d_w_z - 2; i_z++)
	    {
	      column_buffer[i_z] = ((1.0 - alpha) * column_buffer[i_z] + alpha * column_buffer[i_z + 1]);
	    }
	}
      for (int i_z = tpinf.target_start; i_z < tpinf.target_end; i_z++)
	{
	  column_buffer_gridded[i_z] = column_buffer[tpinf.source_start + i_z - tpinf.target_start];
	}
      // it is not good to have zero weight, would give nan as soon as the denominator is taken
      float padding_low = 1.0e-7, padding_high = 1.0e-7;
      padding_low = column_buffer[0];
      padding_high = column_buffer[tpinf.d_w_z - 2];
      for (int i_z = 0; i_z < tpinf.target_start; i_z++)
	{
	  column_buffer_gridded[i_z] = padding_low;
	}
      for (int i_z = tpinf.target_end; i_z < (int)target_v; i_z++)
	{
	  column_buffer_gridded[i_z] = padding_high;
	}
    }
    float non_zero = 1.0;
    
    if (data_weight)
      {
	// now, proceeding horizontally the columns are projected by interpolation on their targets

	// Padding value for horizontal direction
	non_zero = 1.0e-14;
	// when data weight is used, a division is done at the end by the weight.
	// This non_zero float number is used for padding the border zone in the weight accuulator and thus avoiding divisions by zero
	
	for (int i_z = 0; i_z < (int)target_v; i_z++)
	  {
	    // weight is used. The data are weigthed here
	    column_buffer_gridded[i_z] *= column_buffer_gridded_weight[i_z];
	  }
	
	// where we add contributions
	float *target_weight_ptr = (float *)target_weights_info.ptr;
	
	if (!this->diag_zpro_run)
	  {
	    // when diag_zpro_run is activated
	    // no radio nor flats are accumulated
	    
	    int icol0 = i_col + tpinf.integer_x_shift_iproj;
	    // the actual data column will contributed to icol0 and  icol0+1
	    
	    if (icol0 >= 0 && icol0 < (int)target_w)
	      {
		for (int i_z = 0; i_z < (int)target_v; i_z++)
		  {
		    
		    // runnng along the column, contributions are given here to icol0 and for the two target angle
		    // Bilinear interpolation is done for x translation and angular interpolation.
		    // The vertical interpolation is already one in the source column.
		    target_weight_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + icol0] += column_buffer_gridded_weight[i_z] * (1 - tpinf.ang_intp_alpha) * (1.0 - tpinf.fract_x_shift_iproj);
		    target_weight_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + icol0] += column_buffer_gridded_weight[i_z] * (tpinf.ang_intp_alpha) * (1.0 - tpinf.fract_x_shift_iproj);
		  }
		
		if (i_col == 0)
		  {
		    for (int k = 0; k < icol0; k++)
		      {
			// we are at extrema in the source. We cannot go further to the left. Here we pad on the left
			for (int i_z = 0; i_z < (int)target_v; i_z++)
			  {
			    target_weight_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + k] += column_buffer_gridded_weight[i_z] * (1 - tpinf.ang_intp_alpha) * non_zero;
			    target_weight_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + k] += column_buffer_gridded_weight[i_z] * (tpinf.ang_intp_alpha) * non_zero;
			  }
		      };
		  }
	      }
	    
	    int icol1 = icol0 + 1;
	    // Here we repeat what we have doe above, but for ico1 as target.
	    if (icol1 >= 0 && icol1 < (int)target_w)
	      {
		for (int i_z = 0; i_z < (int)target_v; i_z++)
		  {
		    target_weight_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + icol1] += column_buffer_gridded_weight[i_z] * (1 - tpinf.ang_intp_alpha) * (tpinf.fract_x_shift_iproj);
		    target_weight_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + icol1] += column_buffer_gridded_weight[i_z] * (tpinf.ang_intp_alpha) * (tpinf.fract_x_shift_iproj);
		  }
		if (i_col == ((int)target_w - 1))
		  {
		    for (int k = icol1 + 1; k < (int)target_w; k++)
		      {
			for (int i_z = 0; i_z < (int)target_v; i_z++)
			  {
			    target_weight_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + k] += column_buffer_gridded_weight[i_z] * (1 - tpinf.ang_intp_alpha) * non_zero;
			    target_weight_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + k] += column_buffer_gridded_weight[i_z] * (tpinf.ang_intp_alpha) * non_zero;
			  }
		      };
		  }
	      }
	  }
      }
    {
      // And the same thing that we have do for the weight we do for the weigthed data
      if (!this->diag_zpro_run)
	{
	  int icol0 = i_col + tpinf.integer_x_shift_iproj;
	  if (icol0 >= 0 && icol0 < (int)target_w)
	    {
	      for (int i_z = 0; i_z < (int)target_v; i_z++)
		{
		  target_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + icol0] += column_buffer_gridded[i_z] * (1 - tpinf.ang_intp_alpha) * (1.0 - tpinf.fract_x_shift_iproj);
		  target_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + icol0] += column_buffer_gridded[i_z] * (tpinf.ang_intp_alpha) * (1.0 - tpinf.fract_x_shift_iproj);
		}
	      if (i_col == 0)
		{
		  for (int k = 0; k < icol0; k++)
		    {
		      for (int i_z = 0; i_z < (int)target_v; i_z++)
			{
			  target_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + k] += column_buffer_gridded[i_z] * (1 - tpinf.ang_intp_alpha) * non_zero;
			  target_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + k] += column_buffer_gridded[i_z] * (tpinf.ang_intp_alpha) * non_zero;
			}
		    };
		}
	    }
	  
	  int icol1 = icol0 + 1;
	  if (icol1 >= 0 && icol1 < (int)target_w)
	    {
	      for (int i_z = 0; i_z < (int)target_v; i_z++)
		{
		  target_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + icol1] += column_buffer_gridded[i_z] * (1 - tpinf.ang_intp_alpha) * (tpinf.fract_x_shift_iproj);
		  target_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + icol1] += column_buffer_gridded[i_z] * (tpinf.ang_intp_alpha) * (tpinf.fract_x_shift_iproj);
		}
	    }
	  
	  if (i_col == ((int)target_w - 1))
	    {
	      for (int k = icol1 + 1; k < (int)target_w; k++)
		{
		  for (int i_z = 0; i_z < (int)target_v; i_z++)
		    {
		      target_ptr[(tpinf.ipro_gridded_0 * target_v + i_z) * target_w + k] += column_buffer_gridded[i_z] * (1 - tpinf.ang_intp_alpha) * non_zero;
		      target_ptr[(tpinf.ipro_gridded_1 * target_v + i_z) * target_w + k] += column_buffer_gridded[i_z] * (tpinf.ang_intp_alpha) * non_zero;
		    }
		};
	    }
	}
    }
    
    if (tpinf.diag_target_i > -1)
      {
	// DIAGNOSTICS COLLECTION
	// we have a diagnostic to contribute.
	// Things are easier here because we have only the target diag_target_i and we have already the factor tpinf.diag_factor
	
	float *diag_ptr = (float *)diag_info.ptr;
	float *diag_weights_ptr = (float *)diag_weights_info.ptr;
	
	float factor = tpinf.diag_factor;
	
	if (factor > 0)
	  {
	    int icol0 = i_col + tpinf.integer_x_shift_iproj;
	    if (icol0 >= 0 && icol0 < (int)target_w)
	      {
		for (int i_z = 0; i_z < (int)target_v; i_z++)
		  {
		    diag_ptr[(tpinf.diag_target_i * target_v + i_z) * target_w + icol0] += column_buffer_gridded[i_z] * factor * (1.0 - tpinf.fract_x_shift_iproj);
		    diag_weights_ptr[(tpinf.diag_target_i * target_v + i_z) * target_w + icol0] += column_buffer_gridded_weight[i_z] * factor * (1.0 - tpinf.fract_x_shift_iproj);
		  }
	      }
	  }
      }
  }
}



// void GriddedAccumulator::extract_preprocess_with_flats_old(
//     GriddedAccumulator::ExtractArguments args)
// {

//   // absolute value of the average step
//   double my_angle_step_rad = abs(args.chunk_info.chunk_angles_rad.periodic(-1) - args.chunk_info.chunk_angles_rad[0]) / (args.chunk_info.chunk_angles_rad.shape(0) - 1);

//   // average step
//   double my_angle_step_rad_signed = (args.chunk_info.chunk_angles_rad.periodic(-1) - args.chunk_info.chunk_angles_rad[0]) / (args.chunk_info.chunk_angles_rad.shape(0) - 1);

//   // @stripes weights_info.shape[0], weights_info.shape[1],weights_info.shape[2],

//   // These are the angles of the data, between 0 and 2*pi
//   std::vector<float> my_angles_02pi(args.chunk_relative_projection_end - args.chunk_relative_projection_start);
//   for (int k = args.chunk_relative_projection_start; k < args.chunk_relative_projection_end; k++)
//   {
//     float tmp = fmod(args.chunk_info.chunk_angles_rad[k], 2 * M_PI);
//     if (tmp < 0)
//       tmp += 2 * M_PI;
//     my_angles_02pi[k - args.chunk_relative_projection_start] = tmp;
//   }

//   // // this is a technical thing used for multithreading
//   // std::future<int> retvals[num_of_threads];

//   args.my_angles_02pi = my_angles_02pi;
//   args.my_angle_step_rad = my_angle_step_rad;
//   args.my_angle_step_rad_signed = my_angle_step_rad_signed;

//   args.my_thread_id = 0;
//   args.num_threads = 1;
//   extract_preprocess_with_flats_one_thread(args);

// }



int GriddedAccumulator::extract_preprocess_with_flats(
    ArgumentForProcess args)
{
  // nabuxx::GriddedAccumulator &this_obj,
  //   py::slice chunk_slice,
  //   py::object & chunk_info_obj,
  //   py::array_t<int> &subr_start_end,
  //   py::array_t<int> &dtasrc_start_end,
  //   py::slice & radios_range_slice
  
  nabuxx::ChunkInfo &chunk_info = this->etfscan->span_info->chunk_info;
  
  // Considering all the useful projections for a chunk of slices, and that we are processing a part of these projections, typically 100 illustrative value, at once,
  // what chunk_relative_projection_start tells is where, relatively to the beggining of the useful range, the bunch of 100 projections (illustrative value) is starting
  
  int chunk_relative_projection_start, chunk_relative_projection_end  ;
  
  chunk_relative_projection_start = args.subchunk_slice[0]    ;
  chunk_relative_projection_end   = args.subchunk_slice[1]    ;
  
  // these are the global indexes in the whole range of projection angle of all the scan ,
  // corresponfing to chunk_relative_projection_start and chunk_relative_projection_end
  
  int chunk_projection_start, chunk_projection_end;
  chunk_projection_start =   args.radios_angular_range_slicing[0]         ;
  chunk_projection_end   =   args.radios_angular_range_slicing[1]         ;
  
  // subr_start_z, subr_end_z is the ideal region that we are trying to read, possibly beyond the
  // borders of the data, and dtasrc_start_z, dtasrc_end_z are the trimmed border that will be
  // read
  int subr_start_z, subr_end_z, dtasrc_start_z, dtasrc_end_z;
  subr_start_z    = args.subregion_z[0]    ;
  subr_end_z      = args.subregion_z[1]     ;
  dtasrc_start_z  = args.datasrc_z[0]    ;
  dtasrc_end_z    = args.datasrc_z[1]     ;
  
  // read the already preprocessed chunk from the etf files ( already flat fielded, double flats fielded, dark, distortion, diffusion
  std::unique_ptr<EtfScanReading> etfscan_reader_ptr = this->etfscan->createForSlice( dtasrc_start_z ,  dtasrc_end_z,  chunk_projection_start,  chunk_projection_end );
  etfscan_reader_ptr->read_chunk();

  // using a python buffer structure because that is the way it was done for the gridded accuulator, they come from python anyway.
  // So we switch from xt::xarray to py::buffer_info to have uniformity in the gridded_accumulator code that we call
  
  std::vector<size_t> readed_signal_strides_in_bytes;
  for (const auto& stride : etfscan_reader_ptr->readed_signal.strides()) {
    // this is the only annoying aspect of the switch to buffer_info: the strides are expressed in bytes
    readed_signal_strides_in_bytes.push_back(stride * sizeof(float));
  }
  
  py::buffer_info data_raw_info(
				(float*) etfscan_reader_ptr->readed_signal.data(),     // Pointer to the data
				sizeof(float),        // Size of each element
				py::format_descriptor<float>::format(), // Format descriptor (depends on the data type)
				3,               // Number of dimensions
				etfscan_reader_ptr->readed_signal.shape(),           // Shape (dimensions of the array)
				readed_signal_strides_in_bytes          // Strides
				);


  
  std::vector<size_t> readed_weight_strides_in_bytes;
  for (const auto& stride : etfscan_reader_ptr->readed_weight.strides()) {
    readed_weight_strides_in_bytes.push_back(stride * sizeof(float));
  }
  py::buffer_info weight_raw_info(
				  (float*) etfscan_reader_ptr->readed_weight.data(),     // Pointer to the data
				  sizeof(float),        // Size of each element
				  py::format_descriptor<float>::format(), // Format descriptor (depends on the data type)
				  2,               // Number of dimensions
				  etfscan_reader_ptr->readed_weight.shape(),           // Shape (dimensions of the array)
				  readed_weight_strides_in_bytes          // Strides
				  );



  
  std::vector<size_t> readed_weight_Ifactor_strides_in_bytes;
  for (const auto& stride : etfscan_reader_ptr->readed_weight_Ifactor.strides()) {
    readed_weight_Ifactor_strides_in_bytes.push_back(stride * sizeof(float));
  }
  
  py::buffer_info weight_Ifactor_raw_info(
					  (float*) etfscan_reader_ptr->readed_weight_Ifactor.data(),     // Pointer to the data
					  sizeof(float),        // Size of each element
					  py::format_descriptor<float>::format(), // Format descriptor (depends on the data type)
					  1,               // Number of dimensions
					  etfscan_reader_ptr->readed_weight_Ifactor.shape(),           // Shape (dimensions of the array)
					  readed_weight_Ifactor_strides_in_bytes          // Strides
					  );
  
  
  
  // // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~éé
  //   struct  __attribute__((visibility("default")))    ExtractArguments {						
  //     int chunk_relative_projection_start;
  //     int chunk_relative_projection_end;
  //     int subr_start_z;
  //     int  subr_end_z;
  //     int dtasrc_start_z;
  //     int dtasrc_end_z;
  //     ChunkInfo &chunk_info;
  //     py::buffer_info &data_raw_info;
  //     py::buffer_info &weight_raw_info;
  //     py::buffer_info &weight_Ifactor_raw_info;      
  //     int my_thread_id;
  //     int num_threads;
  //     float my_angle_step_rad;
  //     float my_angle_step_rad_signed;
  //     std::vector<float>  my_angles_02pi; 
  //   };
  //   // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  
    GriddedAccumulator::ExtractArguments extract_args(
						      {
							.chunk_relative_projection_start             = chunk_relative_projection_start            ,
							  .chunk_relative_projection_end             = chunk_relative_projection_end              ,
							  .subr_start_z            = subr_start_z               ,
							  .subr_end_z              = subr_end_z                 ,
							  .dtasrc_start_z          = dtasrc_start_z               ,
							  .dtasrc_end_z            = dtasrc_end_z                   ,
							  .chunk_info              = this->etfscan->span_info->chunk_info   ,
							  .data_raw_info           = data_raw_info                      ,
							  .weight_raw_info           =  weight_raw_info                     ,
							  .weight_Ifactor_raw_info           =   weight_Ifactor_raw_info  
							  }
						      ) ; 

  // Completing the args with some more info
  
  // absolute value of the average step
  double my_angle_step_rad = abs(extract_args.chunk_info.chunk_angles_rad.periodic(-1) - extract_args.chunk_info.chunk_angles_rad[0]) / (extract_args.chunk_info.chunk_angles_rad.shape(0) - 1);

  // average step
  double my_angle_step_rad_signed = (extract_args.chunk_info.chunk_angles_rad.periodic(-1) - extract_args.chunk_info.chunk_angles_rad[0]) / (extract_args.chunk_info.chunk_angles_rad.shape(0) - 1);


  // These are the angles of the data, between 0 and 2*pi
  std::vector<float> my_angles_02pi(extract_args.chunk_relative_projection_end - extract_args.chunk_relative_projection_start);
  for (int k = extract_args.chunk_relative_projection_start; k < extract_args.chunk_relative_projection_end; k++)
  {
    float tmp = fmod(extract_args.chunk_info.chunk_angles_rad[k], 2 * M_PI);
    if (tmp < 0)
      tmp += 2 * M_PI;
    my_angles_02pi[k - extract_args.chunk_relative_projection_start] = tmp;
  }

  extract_args.my_angles_02pi = my_angles_02pi;
  extract_args.my_angle_step_rad = my_angle_step_rad;
  extract_args.my_angle_step_rad_signed = my_angle_step_rad_signed;
  extract_args.my_thread_id = 0;
  extract_args.num_threads = 1;


  // finally call the real extraction
  extract_preprocess_with_flats_one_thread(extract_args);
  return 1;
}

int GriddedAccumulator::extract_preprocess_with_flats_one_thread(
    GriddedAccumulator::ExtractArguments args)
{

  GriddedAccumulator::ExtractArguments *my_args = &args;

  // the number of angles at which we collect the diagnostics
  int n_searched_diags = this->diagnostic_searched_angles_rad_clipped_info.shape[0];

  // In total the subchunk that is given to this instance of nabuxx contains
  // the following number of projections to be collected
  size_t n_proj_this_interval = my_args->chunk_relative_projection_end -  my_args->chunk_relative_projection_start;

  // The present thread will work in the interval below.
  // Such interval is a subinterval of an projection interval, typically 100 projections, which is part of a data chunk which is the total available data
  // to reconstruct a chunk of slices.
  int n_proj_start;
  int n_proj_end;
  {

    int np_per_thread = (int)ceil(n_proj_this_interval * 1.0 / my_args->num_threads);
    n_proj_start = my_args->my_thread_id * np_per_thread;
    n_proj_end = std::min((int)(n_proj_start + np_per_thread), ((int)n_proj_this_interval));
  }

  // this is is big collected regridded data size that will be filled, after several call to nabuxx
  // each time passing  a subchunk of projections ( typically 100 projections at once )
  size_t n_proj_gridded = gridded_radios_info.shape[0];

  // here the (sub-image) shape of a projection contained in the data
  size_t d_w_z = my_args->data_raw_info.shape[1];
  size_t d_w_x = my_args->data_raw_info.shape[2];

  // this is the jump over the raw data  that moves from one projection to the followin one
  size_t d_stride_iproj = my_args->data_raw_info.strides[0] / my_args->data_raw_info.itemsize;

  // the data are found here
  float *data = (float *)(my_args->data_raw_info.ptr);


  // this is a loop on the projections that the present thread will process
  for (int i_proj_rel = n_proj_start; i_proj_rel < n_proj_end; i_proj_rel++)
  {

    // the vertical shift
    int my_integer_shift_v = my_args->chunk_info.integer_shift_v(my_args->chunk_relative_projection_start + i_proj_rel);
    float fract_complement_shift_v = my_args->chunk_info.fract_complement_to_integer_shift_v(my_args->chunk_relative_projection_start + i_proj_rel);

    // the horizontal shift
    float x_shift_iproj = my_args->chunk_info.x_pix_per_proj(my_args->chunk_relative_projection_start + i_proj_rel);

    // ideally, making abstraction of the borders, I would like
    // to have access to the range here below, in z, from
    // the data
    int subr_start_z_iproj = my_args->subr_start_z - my_integer_shift_v;
    int subr_end_z_iproj = my_args->subr_end_z - my_integer_shift_v + 1;

    // optionally there may be a diagnostic target that will be contributed by the projection.
    // by default -1 means no contributed diagnostic.
    // If we are collecting 8 diagnostic when diag_target_i takes a value in [0,7 + 8[
    // there is a contribution.
    // The first 8 are the searche angles, the other 8 are replica ( +360)
    int diag_target_i = -1;
    float diag_factor = 0;

    {
      float *searched_diag_angles = (float *)this->diagnostic_searched_angles_rad_clipped_info.ptr;

      // the angle of the data
      float my_angle = my_args->my_angles_02pi[i_proj_rel];

      // search to seee if we fall on a diagnostic
      for (int i = 0; i < n_searched_diags; i++)
      {
        float tmp_a = searched_diag_angles[i];
        if ((tmp_a - my_args->my_angle_step_rad < my_angle) && (my_angle < tmp_a + my_args->my_angle_step_rad))
        {
          diag_target_i = i;
          diag_factor = 1.0 - (fabsf(tmp_a - my_angle)) / (my_args->my_angle_step_rad);
          break;
        }
      }

      if (diag_target_i >= 0)
      {
        // all the machinery contained in this if block
        // has the unique goal of setting up the information
        // that will be used to target the proper diagnostic.
        // For example: at which angular replica do we contribute?
        // There are several turns which contribute to the same angle
        // modulus 360 ( z correlation ) or modulus 180 ( cor correlation)

        const std::lock_guard<std::mutex> lock0(mutexes_diag[diag_target_i]);
        // there may be several threads which contribute to the same diagnostic.
        // With the lock we are safe.

        float target_a = searched_diag_angles[diag_target_i];
        double source_abs = fabsf(my_args->chunk_info.chunk_angles_rad(my_args->chunk_relative_projection_start + i_proj_rel));
        // How do we assign to diagnostic replicated angles  ?
        // For diag_target_i to keep its value it must be the occurrence within chunk
        // with the smallest absolute value of the scan angle
        // For diag_target_i to be promoted to (diag_target_i + n_searched_diags )
        // it must be the occurrence with the second smallest
        // absolute value of the scan angle.
        // This works when the subchunk of projection never covers an angular range which contains two replica
        //
        int count_of_smaller = 0;

        for (int k = 0; k < my_args->chunk_relative_projection_start + i_proj_rel; k++)
        {

          double tmp_abs = fabs(my_args->chunk_info.chunk_angles_rad(k));
          double tmp_clipped = fmod(my_args->chunk_info.chunk_angles_rad(k), 2 * M_PI);
          if (tmp_clipped < 0)
            tmp_clipped += 2 * M_PI;

          if ((target_a - my_args->my_angle_step_rad < tmp_clipped) && (tmp_clipped < target_a + my_args->my_angle_step_rad))
          {
            if (tmp_abs < source_abs - my_args->my_angle_step_rad * 2.0)
            {
              if (fabsf(my_args->chunk_info.z_pix_per_proj[my_args->chunk_relative_projection_start + i_proj_rel] -
                        my_args->chunk_info.z_pix_per_proj[k]) > 2.0)
              { // # to avoid, in z_stages with >360 range for one single stage, to fill the second items which should instead be filled by another stage.
                int n_of_2pi = roundf((source_abs - tmp_abs) / (2 * M_PI));
                count_of_smaller = std::max(count_of_smaller, n_of_2pi);
              }
            }
          }
        }
        if (count_of_smaller > 1)
        {
          // we have already two equivalent angles, this is a bigger one.
          // We discard it.
          diag_target_i = -1;
        }
        else
        {
          // 0 or 1 smaller equivalent angles, this one is to be kept.

          float fractional_missing_step = searched_diag_angles[diag_target_i] - my_args->my_angles_02pi[i_proj_rel];
          float step_fraction = fractional_missing_step / my_args->my_angle_step_rad_signed;

          float my_z = my_args->chunk_info.z_pix_per_proj[my_args->chunk_relative_projection_start + i_proj_rel];
          double my_angle = my_args->chunk_info.chunk_angles_rad[my_args->chunk_relative_projection_start + i_proj_rel] + (fractional_missing_step);
          float dz = 0;
          {
            float dzl = 0, dzh = 0;
            int kk = my_args->chunk_relative_projection_start + i_proj_rel;
            if (kk)
            {
              dzl = my_z - my_args->chunk_info.z_pix_per_proj[kk - 1];
            }
            if (kk < my_args->chunk_relative_projection_end - 1)
            {
              dzh = my_args->chunk_info.z_pix_per_proj[kk + 1] - my_z;
            }
            if (fabsf(dzl) < fabsf(dzh))
            {
              dz = dzl;
            }
            else
            {
              dz = dzh;
            }
          }
          my_z += dz * step_fraction;

          int n_searched_angles = this->diagnostic_searched_angles_rad_clipped_info.shape[0];

          diag_target_i += n_searched_angles * count_of_smaller;

          float old_angle = ((float *)this->diagnostic_angles_info.ptr)[diag_target_i];
          float old_z = ((float *)this->diagnostic_zpix_transl_info.ptr)[diag_target_i];

          if ((old_angle == old_angle) // this excludes nan ( not initialised)
              && (fabsf(my_z - old_z) > 2.0))
          {
            // to avoid possible big jumps with z-stages ( these are pixel units)
            diag_target_i = -1;
          }
          else
          {
            // all is good
            ((float *)this->diagnostic_angles_info.ptr)[diag_target_i] = my_angle;
            ((float *)this->diagnostic_zpix_transl_info.ptr)[diag_target_i] = my_z;
          }
        }
      }
    }
    InterpInfo interp_info({.d_w_z = (long int)d_w_z,
                            .d_w_x = (long int)d_w_x,
                            .dtasrc_start_z = my_args->dtasrc_start_z,
                            .dtasrc_end_z = my_args->dtasrc_end_z,
                            .subr_start_z = my_args->subr_start_z,
                            .subr_end_z = my_args->subr_end_z,
                            .chunk_relative_projection_start = my_args->chunk_relative_projection_start,
                            .chunk_relative_projection_end = my_args->chunk_relative_projection_end,

                            .my_integer_shift_v = my_integer_shift_v,
                            .fract_complement_shift_v = fract_complement_shift_v,
                            .x_shift_iproj = x_shift_iproj,
                            .subr_start_z_iproj = subr_start_z_iproj,
                            .subr_end_z_iproj = subr_end_z_iproj,

                            .n_proj_gridded = (long int)n_proj_gridded,
                            .extension_padding = true,
                            .diag_target_i = diag_target_i,
                            .diag_factor = diag_factor});

    interp_info.setup_for_iproj(*this, i_proj_rel,  my_args->chunk_info);

    {
      // acquire locks on to be modified target projections
      // This could potentially generate a dead lock when the number of gridded projection
      // is less than the double of threads. To avoid this, the number of threads
      // is limited in this unprobable case, in the constructor of the class

      const std::lock_guard<std::mutex> lock0(mutexes[interp_info.ipro_gridded_0]);
      const std::lock_guard<std::mutex> lock1(mutexes[interp_info.ipro_gridded_1]);

      {
        interp_info.extension_padding = true;

        if (!(this->diag_zpro_run && interp_info.diag_target_i == -1)) {
	  
	  float *data_weight = NULL;
	  if (my_args->weight_raw_info.ndim)
            {
              data_weight = (float *) my_args->weight_raw_info.ptr;
            }
	  float data_weight_Ifactor = 1;
	  if (my_args->weight_Ifactor_raw_info.ndim)
            {
              data_weight_Ifactor =  ((float*)my_args->weight_Ifactor_raw_info.ptr)[i_proj_rel];
            }
	  
	  this->project_radio({
			       .data = data + i_proj_rel * d_stride_iproj,
			       .data_weight = data_weight,
			       .data_weight_Ifactor = data_weight_Ifactor,
			       .interpolation_info = interp_info,
			       .target_info = gridded_radios_info,
			       .target_weights_info = gridded_weights_info,
			       .diag_info = diagnostic_radios_info,
			       .diag_weights_info = diagnostic_weights_info,
			       .diag_angles_info = diagnostic_angles_info		       
            });
	}
      }
    }
  }
  return 1;
}

