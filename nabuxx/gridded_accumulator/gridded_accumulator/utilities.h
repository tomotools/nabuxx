#ifndef NABUXX_UTILITIES_H
#define NABUXX_UTILITIES_H

#include "pybind11/numpy.h"

namespace py = pybind11;


namespace nabuxx {


  enum memlayouts  {
    any = 0x0,
    c_contiguous = 0x1,
    f_contiguous = 0x2
  };

  memlayouts operator | (memlayouts a, memlayouts b);
  
  bool check_c_contiguity(py::buffer_info &info, int till_dimension = -1) ;
  
  bool check_f_contiguity(py::buffer_info &info) ;

  bool check_f_or_c_contiguity(   py::buffer_info &info  ) ;
  
  size_t  buffer_size( py::buffer_info &info);


  class __attribute__((visibility("default"))) buffer_info : public py::buffer_info {
  public:
    // void *ptr;
    void copy_from(py::buffer_info &rhs)  {
        ptr = rhs.ptr;
        itemsize = rhs.itemsize;
        size = rhs.size;
        ndim = rhs.ndim;
        shape = rhs.shape;
        strides = rhs.strides;
    }
    void copy_from(buffer_info &rhs)  {
        ptr = rhs.ptr;
        itemsize = rhs.itemsize;
        size = rhs.size;
        ndim = rhs.ndim;
        shape = rhs.shape;
        strides = rhs.strides;
    }

    buffer_info( buffer_info &other    ) {
      copy_from(other);
    }
    buffer_info(     ) {
    }
    void operator =( buffer_info &other) {
      copy_from(other);
    }
    
    ~buffer_info() {
       
    }
    
  }  ;

  py::buffer_info checked_buffer_request( py::buffer &buffer,
					  char type ,
					  std::string method_name,
					  std::string variable_name,
					  nabuxx::memlayouts layout = nabuxx::memlayouts::any ,
					  int ndim=-1,
					  int till_dimension = -1) ;

 

  
}
#endif
