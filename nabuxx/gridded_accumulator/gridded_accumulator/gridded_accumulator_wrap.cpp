// #define XTENSOR_ENABLE_ASSERT 

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>
#include"gridded_accumulator.h"
#include<etfscan.hpp>

using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;


template <typename T>
std::vector<long int> convert_shape(const T& shape) {
  std::vector<long int> vec(shape.begin(), shape.end());
  return vec;
}


		     // py::buffer &dark,		  
		     // py::buffer &flat_indexes,		  
		     // py::buffer &flats,
		      
		     // py::buffer &weight,
		     // py::buffer &double_flat,
		      
		     // py::array  &radios_srcurrent,
		     // py::array  &flats_srcurrent,
		      
		     // // py::array_t<float>  &angular_doubles,
		     // py::array  &angular_doubles,g
		     // int angular_doubles_period_nturns,
		     // int angular_doubles_nsectors_per_turn


PYBIND11_MODULE(gridded_accumulator,m)
{
  xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for gridded accumulator";
  py::class_<nabuxx::GriddedAccumulator>(m, "GriddedAccumulator")
    .def(py::init([](
		     EtfScan *etfscan,
		     py::buffer &gridded_radios, 
		     py::buffer &gridded_weights,
		     
		     py::buffer &diagnostic_radios, 
		     py::buffer &diagnostic_weights,
		     py::buffer &diagnostic_angles,
		      
		     py::buffer &diagnostic_searched_angles_rad_clipped,
		     py::buffer &diagnostic_zpix_transl,
		     int diag_zpro_run
		      
		     ) {


		    py::buffer_info gridded_radios_info  =  nabuxx::checked_buffer_request(gridded_radios , 'f' , "init of GriddedAccumulator" , "gridded_radios",  nabuxx::memlayouts::c_contiguous, 3 );

		    py::buffer_info gridded_weights_info =  nabuxx::checked_buffer_request( gridded_weights ,  'f' ,  "init of GriddedAccumulator" , "weights",  nabuxx::memlayouts::c_contiguous, 3 )  ;
		    py::buffer_info diagnostic_radios_info = nabuxx::checked_buffer_request(diagnostic_radios,'f',"init of GriddedAccumulator","diagnostic_radios",  nabuxx::memlayouts::c_contiguous,3);
		    
		    py::buffer_info diagnostic_weights_info= nabuxx::checked_buffer_request(diagnostic_weights,'f',"init of GriddedAccumulator" , "diagnostic_weights",  nabuxx::memlayouts::c_contiguous, 3 )  ;


		    
		    py::buffer_info diagnostic_angles_info =  nabuxx::checked_buffer_request( diagnostic_angles,'f',"init of GriddedAccumulator" , "diagnostic_angles",  nabuxx::memlayouts::c_contiguous, 1 )  ;


		    py::buffer_info diagnostic_searched_angles_rad_clipped_info =  nabuxx::checked_buffer_request( diagnostic_searched_angles_rad_clipped,'f',"init of GriddedAccumulator" , "diagnostic_searched_angles_rad_clipped_info",  nabuxx::memlayouts::c_contiguous, 1 )  ;
		    
		    py::buffer_info diagnostic_zpix_transl_info =  nabuxx::checked_buffer_request( diagnostic_zpix_transl,'f',"init of GriddedAccumulator" , "diagnostic_zpix_transl_info",  nabuxx::memlayouts::c_contiguous, 1 )  ;


		    

		    return new nabuxx::GriddedAccumulator(
							  etfscan,
							  gridded_radios_info  ,
							  gridded_weights_info ,
							  diagnostic_radios_info  ,
							  diagnostic_weights_info ,
							  diagnostic_angles_info ,
							  diagnostic_searched_angles_rad_clipped_info,
							  diagnostic_zpix_transl_info,
							  diag_zpro_run				  
							  ) ; 
		  }),
	 py::arg("etfscan"), 
	 py::arg("gridded_radios"),
	 py::arg("gridded_weights") ,
	 py::arg("diagnostic_radios")=py::array_t<float>({0,2,2}),
	 py::arg("diagnostic_weights") =py::array_t<float>({0,2,2}),
	 py::arg("diagnostic_angles") =py::array_t<float>({0}),
	 py::arg("diagnostic_searched_angles_rad_clipped") =py::array_t<float>({0}),
	 py::arg("diagnostic_zpix_transl")=py::array_t<float>({0}),
	 py::arg("diag_zpro_run")=0
	 )
    .def_readwrite("num_of_threads", &nabuxx::GriddedAccumulator::num_of_threads)
    .def("mend_zero_weight_border_regions", &nabuxx::GriddedAccumulator::mend_zero_weight_border_regions)
    
    .def("read_and_process", [](nabuxx::GriddedAccumulator& this_obj, int n_granularity) {
	py::gil_scoped_release release;  // release the gile
	this_obj.read_and_process(n_granularity);  // Run the function ( see file gridded_accumulator.cpp
      })
    
    .def("set_vertical_range",  [](nabuxx::GriddedAccumulator &this_obj, py::array_t<int> span_v) {

				
				  int span_v_absolute_start,  span_v_absolute_end;
				
				  py::buffer_info buffer_info  = span_v.request();
				  if( buffer_info.ndim!=1  || buffer_info.shape[0]!=2) {
				    throw std::invalid_argument("the span_v argument has not the right shape. It must be a 1D array with two int entries");
				  }
				
				  span_v_absolute_start = *((int*)buffer_info.ptr) ;
				  span_v_absolute_end   = *((int*) ((char*)buffer_info.ptr + buffer_info.strides[0] ) ) ;
				  
				  
				  {
				    py::gil_scoped_release release;
				    this_obj.etfscan->span_info->set_chunk_info( span_v_absolute_start, span_v_absolute_end);
				  }
				  
				  
				  
				  py::object SimpleNamespace = py::module_::import("types").attr("SimpleNamespace");
				  py::object ns = SimpleNamespace(
								  "angle_index_span"_a= xt::pyarray<int>(this_obj.etfscan->span_info->chunk_info.angle_index_span),
								  "span_v"_a= xt::pyarray<int>(this_obj.etfscan->span_info->chunk_info.absolute_span_v),
								  "integer_shift_v"_a= xt::pyarray<int>(this_obj.etfscan->span_info->chunk_info.integer_shift_v),
								  "fract_complement_to_integer_shift_v"_a= xt::pyarray<float>(this_obj.etfscan->span_info->chunk_info.fract_complement_to_integer_shift_v),
								  "z_pix_per_proj"_a= xt::pyarray<float>(this_obj.etfscan->span_info->chunk_info.z_pix_per_proj),
								  "x_pix_per_proj"_a= xt::pyarray<float>(this_obj.etfscan->span_info->chunk_info.x_pix_per_proj),
								  "angles_rad"_a= xt::pyarray<float>(this_obj.etfscan->span_info->chunk_info.chunk_angles_rad),
								  "internally_projection_angles_deg"_a= xt::pyarray<double>(this_obj.etfscan->span_info->internally_projection_angles_deg)
								  );
				  printf(" LIMITI  OK %d %d %ld\n",  span_v_absolute_start,  span_v_absolute_end,  buffer_info.strides[0] ) ;
				  
				  return ns;
      
				  
				}, py::arg("span_v"));
  
}

