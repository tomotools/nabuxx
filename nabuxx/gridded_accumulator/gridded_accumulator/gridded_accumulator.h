#ifndef SPAN_ACCUMULATOR_H
#define SPAN_ACCUMULATOR_H

#include "pybind11/numpy.h"
#include"../../span_strategy/span_strategy/span_info.h"
#include"utilities.h"
#include<mutex>
#include<vector>
#include<string>
#include<etfscan.hpp>
// @stripes #include <xtensor/xtensor.hpp>


#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <cstring>
#include <highfive/H5File.hpp>



namespace h5 = HighFive;
namespace py = pybind11;




namespace nabuxx {



  struct ArgumentForProcess {

    // Considering all the useful projections for a chunk of slices, and that we are processing a part of these projections, typically 100 illustrative value, at once,
    // what chunk_relative_projection_start tells is where, relatively to the beggining of the useful range, the bunch of 100 projections (illustrative value) is starting
    std::vector<int> subchunk_slice;
    
    std::array<int, 2> subregion_z;
    std::array<int, 2> datasrc_z;

    
    // these are the global indexes in the whole range of projection angle of all the scan ,
    // corresponfing to chunk_relative_projection_start and chunk_relative_projection_end
    std::vector<int> radios_angular_range_slicing;

    // Constructor
    ArgumentForProcess(
		       std::vector<int> subchunk_slice,
		       std::array<int, 2> subregion_z,
		       std::array<int, 2> datasrc_z,
		       std::vector<int> radios_angular_range_slicing)
      : subchunk_slice(subchunk_slice),
	subregion_z(subregion_z),
	datasrc_z(datasrc_z),
	radios_angular_range_slicing(radios_angular_range_slicing) {}
  };
  
  
  struct mutex_wrapper : std::mutex
  {
    mutex_wrapper() = default;
    mutex_wrapper(mutex_wrapper const&) noexcept : std::mutex() {}
    bool operator==(mutex_wrapper const&other) noexcept { return this==&other; }
  };
  /* This mutexes_wrapper above is used to make it possible to have std::vector<mutex_wrapper>. Such templated class needs the copy contructor for resize().

     The mutex_wrapper structure provides a wrapper around std::mutex with some added functionality. Let's break down the specific interest and benefits of using std::vector<mutex_wrapper> instead of a simple std::vector<std::mutex>.  In particula we need this one:

     Copy Constructor:
     std::mutex is neither copyable nor movable. By default, attempting to copy a std::mutex object will result in a compilation error. The mutex_wrapper provides a copy constructor:

     Other functionalities follow...
  */
  

  class InterpInfo;

  class GriddedAccumulator{
  public:

    EtfScan *etfscan;
    nabuxx::buffer_info gridded_radios_info;
    nabuxx::buffer_info gridded_weights_info;
    
    nabuxx::buffer_info diagnostic_radios_info  ;
    nabuxx::buffer_info diagnostic_weights_info ;
    nabuxx::buffer_info diagnostic_angles_info  ;
    
    nabuxx::buffer_info diagnostic_searched_angles_rad_clipped_info  ;
    nabuxx::buffer_info diagnostic_zpix_transl_info  ;
    int diag_zpro_run;
    
    int num_of_threads;
    std::vector<mutex_wrapper> mutexes;
    std::vector<mutex_wrapper> mutexes_diag;

    float nominal_current;

    GriddedAccumulator(
		       EtfScan * etfscan,
		       py::buffer_info &gridded_radios_info,
		       py::buffer_info &gridded_weights_info,
		       
		       py::buffer_info &diagnostic_radios_info,
		       py::buffer_info &diagnostic_weights_info,
		       py::buffer_info &diagnostic_angles_info,
		       
		       py::buffer_info &diagnostic_searched_angles_rad_clipped_info,
		       py::buffer_info &diagnostic_zpix_transl_info,
		       int diag_zpro_run
		       );


    void read_and_process(int reading_granularity);
    
    void init_readers(int num_reading_sub_processes) ;

    int mend_zero_weight_border_regions();
    void reset() {
      memset( gridded_radios_info.ptr , 0,    nabuxx::buffer_size(gridded_radios_info) * gridded_radios_info.itemsize   ) ; 
      memset( gridded_weights_info.ptr , 0,    nabuxx::buffer_size(gridded_weights_info) * gridded_weights_info.itemsize   ) ; 

    }


    struct  __attribute__((visibility("default")))    ExtractArguments {						
      int chunk_relative_projection_start;
      int chunk_relative_projection_end;
      int subr_start_z;
      int  subr_end_z;
      int dtasrc_start_z;
      int dtasrc_end_z;
      ChunkInfo &chunk_info;
      py::buffer_info &data_raw_info;
      py::buffer_info &weight_raw_info;
      py::buffer_info &weight_Ifactor_raw_info;      
      int my_thread_id;
      int num_threads;
      float my_angle_step_rad;
      float my_angle_step_rad_signed;
      std::vector<float>  my_angles_02pi; 
    };
     
    
    /* void extract_preprocess_with_flats_old( */
    /* 				       ExtractArguments args */
    /* 				       ) ; */

    int extract_preprocess_with_flats(
				       ArgumentForProcess args
				       ) ;



    
    int extract_preprocess_with_flats_one_thread(
						 ExtractArguments args
						 ) ;


    struct __attribute__((visibility("default"))) ProjectRadioArgument   {
      int i_col              ;
      float *data        ;
      float *data_weight        ;
      float data_weight_Ifactor        ;
      const InterpInfo &interpolation_info;
      py::buffer_info &target_info ;
      py::buffer_info &target_weights_info ;
      py::buffer_info &diag_info          ;
      py::buffer_info &diag_weights_info  ;
      py::buffer_info &diag_angles_info   ;
     
      bool do_flats      ;
    };
    void project_radio( ProjectRadioArgument  args  );
  };
  
  
  class InterpInfo {
  public:

    void setup_for_iproj(GriddedAccumulator & accu, int i_proj,  ChunkInfo &ChunkInfo ) ;
    
    long int d_w_z; // vertical size of the read data
    long int d_w_x; // horizontal

    int dtasrc_start_z; // where data starts vertically  in the detector
    int dtasrc_end_z;   // where....  ends ...

    int subr_start_z;
    int subr_end_z;

    int chunk_relative_projection_start;
    int chunk_relative_projection_end;

    int my_integer_shift_v  ;
    float fract_complement_shift_v ;

    float   x_shift_iproj;
    int   integer_x_shift_iproj;
    float   fract_x_shift_iproj;
    int subr_start_z_iproj ;
    int subr_end_z_iproj   ;
    long int n_proj_gridded;
    bool extension_padding ;

    int diag_target_i;
    float diag_factor;
    
    int i_col;
    
    int ff_idx1;
    int ff_idx2;
    float ff_c1;
    float ff_c2;

    float ff_current_factor_c1;
    float ff_current_factor_c2;

    // for angular and turn interpolation
    int ff_idx3;
    int ff_idx4;
    float ff_c3;
    float ff_c4;
    float ff_current_factor_c3;
    float ff_current_factor_c4;

    // for angular_doubles
    int ad_idx1;
    int ad_idx2;
    float ad_c1;
    float ad_c2;
    
    int ad_idx3;
    int ad_idx4;
    float ad_c3;
    float ad_c4;
    
    float radio_current_factor;
    float dark_spurious_factor_in_flat ; 
     
    int  target_start;
    int  target_end  ;
    int  source_start  ;
    int  source_end    ;

    int ipro_gridded_0;
    int ipro_gridded_1;
    float ang_intp_alpha;
    double my_angle;

  };
  
  
  

}




#endif
