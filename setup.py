# coding: utf-8

from setuptools import setup, find_packages
import os
version="2024.1"

from  find_pybind11_extensions import find_extensions, CMakeBuild

def setup_package():
    setup(
        name="nabuxx",
        author="Alessandro Mirone",
        version=version,
        author_email="mirone@esrf.fr",
        maintainer="Alessandro Mirone",
        maintainer_email="mirone@esrf.fr",
        packages=find_packages(),
        package_data={
        },
        include_package_data=True,
        install_requires=[
            "numpy > 1.9.0",
        ],
        setup_requires=[
            "wheel",
        ],
        
        ext_modules=find_extensions("nabuxx"),
        cmdclass={"build_ext": CMakeBuild},
        


        extras_require={
        },
        description="Nabuxx - selected accelerated modules",
        entry_points={
            "console_scripts": [
            ],
        },
        zip_safe=True,
    )


if __name__ == "__main__":
    setup_package()
