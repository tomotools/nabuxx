# nabuxx

Provides several accelerated functions for tomography. One among them is the gridded accumulator for helical tomography.

We provide in this readme the installation instruction and an application example made of few python lines, to convert a chunk helical dataset into a standard sinogram.

## Installation

The installation procedure here works on the esrf cluster and it relies on some header-only libraries like xtensor and pybind11.
See CMakelists.txt for installation on other systems.

```bash
python3 -m venv ~/venv_nabuxx
source ~/venv_nabuxx/bin/activate
git clone https://gitlab.esrf.fr/tomotools/nabuxx
cd  nabuxx
pip install .
```

## Testing the gridded accumulator

First, prepare a nexus file:

```bash
pip install nxtomomill
nxtomomill h52nx /data/visitor/md1389/bm18/20240711/RAW_DATA/HEL_HA500_4.260um_GLE1087_lung_ROI-1/HEL_HA500_4.260um_GLE1087_lung_ROI-1_0001/HEL_HA500_4.260um_GLE1087_lung_ROI-1_0001.h5  HEL_HA500_4.260um_GLE1087_lung_ROI-1_0001.nx
```

Now we reformat the nexus file into a better format which is adapted to high-performance computing. This is done with the script *nx2etf.py* that you find in the nabuxx sources, in the test directory.

```bash
# The script comes from night_rail and depends on scipy and namedlist
pip install scipy
pip install namedlist
python nx2etf.py --nexus_file HEL_HA500_4.260um_GLE1087_lung_ROI-1_0001_0000.nx
```

Now we have reformatted the data into the ETF format. We can test the accumulator.
Launch python, and create a dataset_info object, note the argument for concurrent reading, it mpodulates the number of threads, and the associated subprocesses,

```python
from nabuxx.etfscan import EtfScan
dataset_info = EtfScan("HEL_HA500_4.260um_GLE1087_lung_ROI-1_0001.etf", num_reading_sub_processes=32)

dataset_info.get_doable_span()

print(dataset_info.get_informative_string())
```

Now create two accumulation arrays, they will cover 360 degrees. One for the accumulated signal, the other for the accumulated weight.

```python
import numpy as np
my_angle_step = abs(np.diff(dataset_info.projection_angles_deg).mean())

n_gridded_angles = int(round(360.0 / my_angle_step))

gridded_radios = np.zeros((n_gridded_angles,) + (100, dataset_info.radio_shape[1]), "f")
gridded_weights = np.zeros_like(gridded_radios)
```

Now create the gridded accumulator feeding it with dataset_info and the accumulation arrays. 

```python

from nabuxx.gridded_accumulator import GriddedAccumulator
gridded_accumulator = GriddedAccumulator( dataset_info, gridded_radios, gridded_weights)
```

Inform the region to be collected, here for slice 1450 up to 1549. The numbering is in the detector reference (see informative string):

```python
gridded_accumulator.set_vertical_range((1450, 1550))
```

Perform the reading and processing, here with a granularity of 100 projections (each thread reads 100 projections at once):

```python
gridded_accumulator.read_and_process(100)
```

The following data are ready to be paganinised, logged, and reconstructed:

```python
gridded_radios /= gridded_weights

import h5py
h5py.File("sino.h5", "w")["data"] = gridded_radios
```

The ETFScan object can also be used in stand-alone mode.

You can retrieve a single frame or a stack of non-consecutive frames:
Retrieving a Single Frame:

```python

# Retrieve frame at index 10
frame = dataset_info.get_frame(10)
print(frame)
```


Retrieving Non-Consecutive Frames:

```python

# Retrieve frames 2, 10, and 25
frame_indices = [2, 10, 25]
frame_stack = dataset_info.get_frames_stack(frame_indices)

print(frame_stack)

```

# Detailed description

# Gridded Accumulator - ETFScan Module

This repository contains the GriddedAccumulator system, providing several accelerated functions for tomography, including gridded accumulation for helical tomography. The system is optimized for parallel processing and offers flexibility in data handling and reconstruction.

## Features

- **ETF File Scanning**: Reads and processes multiple types of tomographic data stored in ETF format, including dark fields, flat fields etcetera (with or without angular dependencies), double flat fields (with or without angular dependencies), and projections. Plus weighted averaging of redundancies. 
- **Parallel Data Processing**: Uses multi-threading to parallelize data reading and processing for improved performance.
- **Span Information Management**: Automatically calculates reconstructable boundaries and repositioning shifts through the SpanInfo class.
- **Comprehensive Pre-Processing**: Includes advanced correction techniques like distortion correction, dark field removal, flat field normalization, and diffusion removal, dark flat fields  and double-flat-files with  angular dependencies, weighted averaging of redundancies. 
- **Modular Integration**: The `EtfScan` object can be passed to a `GriddedAccumulator` object to manage data accumulation for tomographic reconstruction.
- **Frame and Stack Retrieval**: Offers methods to retrieve both single frames and non-consecutive stacks of frames, enhancing flexibility in data access.


## Pre-Processing Operations

During the reading and processing, the following operations are automatically applied to the data:
- **Dark Field Removal**: Reduces noise by subtracting the dark field.
- **Flat Field Normalization**: Corrects non-uniform illumination by dividing by the flat field. Possibly with angular dependency.
- **Double Flat Field Removal**: Handles the case of double flat fields in the data. Possibly with angular dependency
- **Redundencies weighted averaging**: In helical mode and/or for 360degre scans the redundancy is weighted by the statistical weight which can be lowered for defective regions.
- **Distortion Correction**: Fixes geometric distortions introduced during scanning.
- **Diffusion Removal**: Removes scintillator diffusion artifacts, improving data clarity.

---

### Class Responsibilities

#### 1. `EtfScan`
- **Role**: Manages the ETF scan data and acts as a parent container for both `SpanInfo` and `EtfScanReading` objects.
- **Key Responsibilities**:
   - When an `EtfScan` object is created, it automatically creates an internal object of the class `SpanInfo`, for which it becomes a container. The `SpanInfo` class calculates the reconstructable zone and manages the repositioning shifts required during tomographic reconstruction. The SpanInfo class complements the informations contained in EtfScan with what will be needed by the gridded accumulator for helical scans.
   - **Parallel Reading**: When a data reading is requested, multiple threads are launched. Each thread creates an instance of the `EtfScanReading` class. These `EtfScanReading` objects are responsible for reading and processing data chunks, and each of them is linked to the parent `EtfScan` object.
   - Provides methods to retrieve individual frames (`get_frame`) or stacks of frames (`get_frames_stack`).
   - Allows access to various metadata and scan information, such as projection angles, pixel size, and other scan parameters.

#### 2. `SpanInfo`
- **Role**: Defines and manages the geometrical parameters of the scan.
- **Key Responsibilities**:
   - Calculates the boundaries of the reconstructable zone.
   - Manages repositioning shifts required to reconstruct specific slices during tomographic reconstruction.
   - Provides important metadata such as pixel size, z-offsets, and sunshine limits (reconstruction boundaries).

#### 3. `EtfScanReading`
- **Role**: Handles the reading and processing of data chunks.
- **Key Responsibilities**:
   - Created within each thread that is launched during a parallel reading operation. It is responsible for reading chunks of data from the ETF file and applying necessary corrections.
   - Applies pre-processing steps such as dark field removal, flat field normalization, distortion correction, and diffusion removal.
   - Returns processed data, which is then accumulated by the `GriddedAccumulator`.

#### 4. `GriddedAccumulator`
- **Role**: Accumulates the processed data for tomographic reconstruction.
- **Key Responsibilities**:
   - Receives data from multiple threads created by `EtfScan` and acted by `EtfScanReading`, to process and accumulate projection data.
   - Manages the parallel accumulation of frames into a final sinogram for further analysis.
   - Allows customization of angular ranges, vertical slice ranges, and the granularity of processing.


### Wrapping with Python

The core functionality of `EtfScan` and `GriddedAccumulator` is exposed to Python through `pybind11`. This allows developers to:

- Initialize and configure `EtfScan` objects in Python.
- Control data accumulation and processing through `GriddedAccumulator` from a Python environment.


## Summary of Key Classes

- **`EtfScan`**: Manages ETF scan data and contains span and file information. It handles the initialization of the ETF scan and controls the `SpanInfo` object. Manages multi-threaded data reading
- **`SpanInfo`**: Calculates the reconstruction boundaries and repositioning shifts.
- **`EtfScanReading`**: Handles data reading and pre-processing, applying all necessary corrections. It is active within one single thread
- **`GriddedAccumulator`**: Manages multi-threaded data accumulation, working with `EtfScan` to extract and process data chunks.

## Conclusion

This module is designed to streamline the processing and accumulation of tomographic scan data stored in the ETF format. By leveraging multi-threading and pre-processing, the system ensures efficient and accurate data reconstruction. The Python bindings make it easy to integrate into larger pipelines for image reconstruction and analysis.



